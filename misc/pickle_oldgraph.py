#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov  7 12:24:57 2018

Force compatibility across different versions of TensorFlow via Pythons pickle

@author: imohacsi-adm
"""

import tensorflow as tf
import pickle
from utilities import kernel_transfer



FLAGS = tf.app.flags.FLAGS

tf.app.flags.DEFINE_string("savefile_path","../picklefiles","""Folder for saving model checkpoints""" );
tf.app.flags.DEFINE_string("loadfile_path","../savefiles/hupa_xception_v2_x0.5_r1e5_foc0.85","""Folder for reading previous checkpoints""" );
tf.app.flags.DEFINE_string("loadfile_name","hupa_xception_v2_x0.5_r1e5_foc0.85-70701","""Name of the loaded checkpoint""" );

oldvar = kernel_transfer.warminit_oldvar_dict( FLAGS.loadfile_path, FLAGS.loadfile_name, False );



pfilename = FLAGS.savefile_path+"/"+FLAGS.loadfile_name+".pickle";
print("Saving to: ", pfilename )
pfile = open( pfilename, "wb" );
pickle.dump( oldvar, pfile );
pfile.close();