import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import os

from fileio import fileio_hupa as fileio
from fileio import imagestream_hupa as imagestream
import layers


#datapath = "/home/imohacsi-adm/Downloads/HuPA/train"
#csvpath  = "/home/imohacsi-adm/Downloads/HuPA/train.csv"
datapath = "/run/media/imohacsi/DataSSD/Kaggle/HumanProteinAtlas/train"
csvpath  = "/run/media/imohacsi/DataSSD/Kaggle/HumanProteinAtlas/train.csv"


FLAGS = tf.app.flags.FLAGS
tf.app.flags.DEFINE_integer("num_threads",2,"""Number of threads for preprocessing""" );

tf.app.flags.DEFINE_integer("batch_size",48,"""Number of images in a batch""" );

tf.app.flags.DEFINE_integer("image_crop_x",64,"""Number of pixels bv   to be cropped from the image in X""" );
tf.app.flags.DEFINE_integer("image_crop_y",64,"""Number of pixels to be cropped from the image in Y""" );
tf.app.flags.DEFINE_integer("image_zoom",64,"""Number of pixels on the shorter edge of the resized image""" );


"""Running the actual main function"""
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"  
os.environ["CUDA_VISIBLE_DEVICES"]=""; # % (0); 

print("Loading data...")

filelist_train,num_class = fileio.filelist_hupa( csvpath, datapath ).load_filelist();
train_preproc = imagestream.imagestream_hupa(1000000,16,False,True,True,True).get_keypoints();
files_tr,labels_tr,_,iterator_tr = train_preproc;

print("Building graph...")

rnd_plh = tf.placeholder( tf.float32, shape=[None,None,None,1], name='features' );

with tf.device('/cpu:0'):            
    handle = tf.placeholder(tf.string, shape=[], name='input_handle');
    iterator =  tf.data.Iterator.from_string_handle( handle, (tf.float32,tf.float32), (tf.TensorShape([None,None,None,3]), tf.TensorShape([None,28])) );
x_raw, y_raw = iterator.get_next();
x_raw = tf.image.resize_images(x_raw, [FLAGS.image_crop_x,FLAGS.image_crop_y]);


G_rnd  = layers.Generator( rnd_plh );
D_real = layers.Discriminator( x_raw );
D_gen  = layers.Discriminator( G_rnd );

g_loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits = D_gen, labels = tf.ones_like(D_gen)) );
d_loss_real = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits = D_real, labels = tf.ones_like(D_real)))
d_loss_fake = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits = D_gen, labels = tf.zeros_like(D_gen)))
d_loss = d_loss_real + d_loss_fake


gen_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "generator");
dis_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "discriminator");


train_g = tf.train.AdamOptimizer(0.0001 ).minimize(g_loss, var_list=gen_vars)
train_d = tf.train.AdamOptimizer(0.00001).minimize(d_loss, var_list=dis_vars)

print("Initializing session...")
init = tf.global_variables_initializer();
sess = tf.Session();
sess.run( init );


handle_tr = sess.run( iterator_tr.string_handle() );
tr_dirs = { files_tr[0]: filelist_train['blue'], files_tr[1]: filelist_train['green'], files_tr[2]: filelist_train['yellow'], 
           files_tr[3]: filelist_train['red'],  labels_tr: np.float32(filelist_train.iloc[:,5:33]) }
sess.run( iterator_tr.initializer, feed_dict=tr_dirs );



print("Beginning training...")
dLoss = [ 1.0 ];
gLoss = [ 1.0 ];
for it in range(100000):
    rnd_batch = np.random.uniform(-1, 1, size=[FLAGS.batch_size, FLAGS.image_crop_x//4,FLAGS.image_crop_y//4,1] );
    
    _,_dLoss = sess.run([train_d,d_loss], feed_dict={ handle: handle_tr, rnd_plh: rnd_batch } );
    _,_gLoss = sess.run([train_g,g_loss], feed_dict={ rnd_plh: rnd_batch } );
#    rnd_batch = np.random.uniform(-1, 1, size=[FLAGS.batch_size, FLAGS.image_crop_x//4,FLAGS.image_crop_y//4,1] );
#    _,gLoss = sess.run([train_g,g_loss], feed_dict={ rnd_plh: rnd_batch } );

    dLoss.append( 0.2*_dLoss+0.8*dLoss[-1] );
    gLoss.append( 0.2*_gLoss+0.8*gLoss[-1] );
    
    if it%20==0 and it>20:
        print("%d\tDiscriminator: %g\tGenerator: %g" % (it,dLoss[-1],gLoss[-1]) );
        rnd_batch = np.random.uniform(-1, 1, size=[1, FLAGS.image_crop_x//4,FLAGS.image_crop_y//4,1] );
        img_real,img_gen = sess.run( [x_raw,G_rnd], feed_dict={ handle: handle_tr, rnd_plh: rnd_batch})
        img_real= np.squeeze( img_real[0,:,:,:] );
        img_gen = np.squeeze( img_gen );
        
        plt.figure(1)
        plt.subplot(1,2,1)
        plt.imshow( 0.5*(img_real+1) );
        plt.show(block=False );
        plt.subplot(1,2,2)
        plt.imshow( 0.5*(img_gen+1) );
        plt.show(block=False );
        plt.pause(0.1);

        plt.figure(0)
        plt.plot( dLoss )
        plt.plot( gLoss )
        plt.show(block=False );
        plt.pause(0.1);






