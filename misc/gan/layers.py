#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 30 12:39:26 2018

@author: imohacsi-adm
"""

import tensorflow as tf

FLAGS = tf.app.flags.FLAGS


"""Global pooling with recurrent network"""
def depool(x_in, shape_out, pool_out=None ):
    x_size = x_in.get_shape().as_list();
    if pool_out is None:
        pool_out = x_size[-1];
    
    x = tf.image.resize_images( x_in, size=[shape_out[1], shape_out[2]] )
    #x_o=tf.layers.conv2d( x, pool_out, kernel_size=[3,3], padding="SAME", activation=tf.nn.elu );
    return x;

def residual_bottleneck( x_in, filters, filters_out=None ):
    x_size = x_in.get_shape().as_list();

    if filters_out is None:
        filters_out = x_size[-1];
        
    if filters_out != x_size[-1]:
        x_shrt =  tf.layers.conv2d( x_in, filters_out, [1,1],padding='SAME', activation=tf.nn.leaky_relu );
    else:
        x_shrt = x_in;
    
    c1 = tf.layers.conv2d( x_in, filters, [3,3],padding='SAME', activation=tf.nn.leaky_relu );
    c2 = tf.layers.conv2d( c1, filters_out, [3,3],padding='SAME', activation=tf.nn.leaky_relu );

    return x_shrt + c2

def Generator( x_in ):
    with tf.variable_scope("generator", reuse=False ): 
        g0 = x_in;
        g1 = tf.layers.conv2d( g0, 64, [5,5],padding='SAME', activation=tf.nn.elu );
        g1 = residual_bottleneck( g1, 64//2 );
        g1 = residual_bottleneck( g1, 64, 32 );
        g2 = depool( g1, [None,FLAGS.image_crop_x//2,FLAGS.image_crop_y//2,None] )
        g2 = residual_bottleneck( g2, 32//2 );
        g2 = residual_bottleneck( g2, 32, 16 );
        g3 = depool( g2, [None,FLAGS.image_crop_x,FLAGS.image_crop_y,None] )
        g3 = residual_bottleneck( g3, 16//2 );
        g3 = residual_bottleneck( g3, 16 );
        
        g4 = tf.layers.conv2d( g3, 3,  [5,5],padding='SAME', activation=tf.nn.softsign );
    return g4;

def Discriminator( x_in ):
    with tf.variable_scope("discriminator", reuse=tf.AUTO_REUSE): 
        c0 = x_in;
        c1 = tf.layers.conv2d( c0, 16, [3,3], padding='VALID', activation=tf.nn.elu );
        c1 = tf.layers.conv2d( c1, 16, [3,3], padding='VALID', activation=tf.nn.elu );

        c1 = residual_bottleneck( c1, 16//2 );
        c1 = residual_bottleneck( c1, 16, 32 );        
        c2 = tf.nn.max_pool( c1, ksize=[1,3,3,1],strides=[1,2,2,1],padding='VALID' )
        c2 = residual_bottleneck( c2, 32//2 );
        c2 = residual_bottleneck( c2, 32, 64 );        
        c3 = tf.nn.max_pool( c2, ksize=[1,3,3,1],strides=[1,2,2,1],padding='VALID' )
        c3 = residual_bottleneck( c3, 64//2 );
        c3 = residual_bottleneck( c3, 64, 64 );        
           
        gp = tf.reduce_mean( c3, axis=[1,2] );
        
        d1 = tf.layers.dense( gp, 64, activation=tf.nn.elu );
        d2 = tf.layers.dense( d1, 1, activation=None );
    return d2;