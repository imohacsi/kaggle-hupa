# -*- coding: utf-8 -*-


import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import cv2



def load_df_image( entry ):
    image_r = cv2.imread( entry['red'], 0 );
    if image_r is None:
        image_r = cv2.imread( entry['red'][:-3]+"jpg", 0 );
    image_g = cv2.imread( entry['green'], 0 );
    if image_g is None:
        image_g = cv2.imread( entry['green'][:-3]+"jpg", 0 );
    image_b = cv2.imread( entry['blue'], 0 );
    if image_b is None:
        image_b = cv2.imread( entry['blue'][:-3]+"jpg", 0 );
    image_rgb = np.stack([image_r,image_g,image_b], axis=-1 );
    return image_rgb;


def plot_set_duplicates( duplicate_df, filelist_df ):
    dtest_unique = duplicate_df['dhash'].unique();
    for huniq in dtest_unique:
        img_sel = duplicate_df[ duplicate_df['dhash']==huniq ].reset_index(drop=True);
        num_sel = len(img_sel)
        plt.figure(1)
        plt.clf()
        for ii,entry in img_sel.iterrows():
            fl_entry = filelist_df[ filelist_df['Id'] == entry['Id'] ];
            img = load_df_image( fl_entry.iloc[0] ); 
            plt.subplot(1,num_sel,ii+1)
            plt.imshow(img)
        plt.show(block=False)
        plt.pause(2)
    
    
def plot_pair_duplicates( duplicate_df, filelist_1, filelist_2 ):
    for idx,entry in duplicate_df.iterrows():
        
        fl_entry1 = filelist_1[ filelist_1['Id'] == entry['Id_x'] ];
        fl_entry2 = filelist_2[ filelist_2['Id'] == entry['Id_y'] ];

        img_1 = load_df_image( fl_entry1.iloc[0] ); 
        img_2 = load_df_image( fl_entry2.iloc[0] ); 
        
        plt.figure(1)
        plt.clf()
        plt.subplot(1,2,1)
        plt.imshow(img_1)
        plt.subplot(1,2,2)
        plt.imshow(img_2)
        plt.show(block=False)
        plt.pause(5)
    
    
