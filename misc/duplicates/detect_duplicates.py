#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 15 11:31:51 2018

@author: imohacsi-adm
"""
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from PIL import Image
import imagehash
import cv2
import sys
sys.path.append("..")


from fileio import fileio_hupa as fileio
import duplicate_helpers as helpers


hash_size = 4;
#
#testpath = "/home/imohacsi-adm/Downloads/HuPA/test"
#trainpath = "/home/imohacsi-adm/Downloads/HuPA/train"
#traincsv = "/home/imohacsi-adm/Downloads/HuPA/train.csv"
testpath = "/run/media/imohacsi/DataSSD/Kaggle/HumanProteinAtlas/test"
trainpath= "/run/media/imohacsi/DataSSD/Kaggle/HumanProteinAtlas/train"
traincsv = "/run/media/imohacsi/DataSSD/Kaggle/HumanProteinAtlas/expanded_training_set.csv"


""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""




"""Loading train and test image lists"""
filelist_test    = fileio.get_test_filelist( testpath );
filelist_test = filelist_test.reset_index(drop=True);
filelist_train,_ = fileio.filelist_hupa( traincsv, trainpath ).load_filelist();
filelist_train = filelist_train.reset_index(drop=True);
print("Number of training examples: ", len(filelist_train) );
print("Number of testing examples: ", len(filelist_test) );
del _;

"""First hashing test dataset"""
test_db = [];
for idx,entry in filelist_test.iterrows():
    image_r = cv2.imread( entry['red'], 0 );
    image_g = cv2.imread( entry['green'], 0 );
    image_b = cv2.imread( entry['blue'], 0 );
    image_rgb = np.stack([image_r,image_g,image_b], axis=-1 );
    
    pil_im = Image.fromarray( image_rgb );
    img_hash = str( imagehash.dhash( pil_im, hash_size=hash_size ) );
    test_db.append( [ entry['Id'], img_hash ] );
    
#    #Add all possible flippings if someone tried to be naughty...
#    for flip in [-1,0,1]:
#        flipped_img = cv2.flip( image_rgb.copy(), flip );
#        pil_im = Image.fromarray( flipped_img );
#        img_hash = str( imagehash.dhash( pil_im, hash_size=hash_size ) );
#        test_db.append( [ entry['Id'], img_hash ] );    
        
    if idx%500==0:
        print("Calculated %d of %d (test)..." %(idx,len(filelist_test)) );
del image_r,image_g,image_b,image_rgb,flip, flipped_img,pil_im,img_hash,idx,entry
    
    
"""Then hashing train dataset"""
train_db = [];
for idx,entry in filelist_train.iterrows():
    image_r = cv2.imread( entry['red'], 0 );
    if image_r is None:
        image_r = cv2.imread( entry['red'][:-3]+"jpg", 0 );
    image_g = cv2.imread( entry['green'], 0 );
    if image_g is None:
        image_g = cv2.imread( entry['green'][:-3]+"jpg", 0 );
    image_b = cv2.imread( entry['blue'], 0 );
    if image_b is None:
        image_b = cv2.imread( entry['blue'][:-3]+"jpg", 0 );

    image_rgb = np.stack([image_r,image_g,image_b], axis=-1 );
    image_rgb = cv2.resize( image_rgb, (512,512) );
    
    
    pil_im = Image.fromarray( image_rgb );
    img_hash = str( imagehash.dhash( pil_im, hash_size=hash_size ) );
    train_db.append( [ entry['Id'], img_hash ] );
    
    if idx%500==0:
        print("Calculated %d of %d (train)..." %(idx,len(filelist_train)) );
del image_r,image_g,image_b,image_rgb,pil_im,img_hash,idx,entry;
    
    
"""Creating test and train dataframes"""    
test_df = pd.DataFrame( test_db, columns=['Id','dhash'] );
train_df = pd.DataFrame( train_db, columns=['Id','dhash'] );
del test_db,train_db;


"""Duplicates within the individual sets"""
mask_te = test_df['dhash'].duplicated(keep=False);
test_duplicates = test_df[mask_te].copy();
mask_tr = train_df['dhash'].duplicated(keep=False);
train_duplicates = train_df[mask_tr].copy();
del mask_tr,mask_te;

"""Plotting a movie of duplicates"""
#helpers.plot_set_duplicates( test_duplicates, filelist_test );



"""Check for data leak between train and test"""
test_in =  test_df['dhash'].isin(train_df['dhash'] )
train_in =  train_df['dhash'].isin(test_df['dhash'] )

trte_duplicates_1 = test_df[test_in];
trte_duplicates_2 = train_df[train_in];
trte_duplicates = pd.merge(trte_duplicates_1,trte_duplicates_2, on='dhash').copy()
del test_in,train_in,trte_duplicates_1,trte_duplicates_2

"""Plot duplicated between datasets"""
helpers.plot_pair_duplicates( trte_duplicates, filelist_test,filelist_train );






