#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  8 14:45:01 2018

@author: imohacsi-adm
"""

import tensorflow as tf
import numpy as np
import os

os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"  
os.environ["CUDA_VISIBLE_DEVICES"]=""; # % (0); 


savefile_path = "../savefiles/hupa_xception24_x0.5_MILv0_probAVG_focAUTO2"
savefile_name = "hupa_xception24_x0.5_MILv0_probAVG_focAUTO2-20201"


metafile_name = '%s/%s.meta' % ( savefile_path, savefile_name )        
loader = tf.train.import_meta_graph( metafile_name, clear_devices=True );   
tensor_names = [t.name for op in tf.get_default_graph().get_operations() for t in op.values()];
variable_names = [ v.name for v in tf.trainable_variables() ]

#print( tensor_names )
for v in variable_names:
   print(v)



"""Set up the GPU computation environment"""
gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.8, allow_growth=True, allocator_type="BFC");
config = tf.ConfigProto(gpu_options=gpu_options,allow_soft_placement=False);

sess = tf.train.MonitoredTrainingSession(master="", is_chief=True, hooks=None,
                                       save_checkpoint_secs=600, checkpoint_dir=None,
                                       save_summaries_steps=500, config=config );   



datafile_name = '%s/%s' % ( savefile_path, savefile_name );    
print("Restoring variables from: ", datafile_name)

loader.restore( sess, datafile_name );    



graph = tf.get_default_graph();

class_weights_t = graph.get_tensor_by_name("model/finals/loss_focal/class_weights:0");  
class_weights = sess.run( class_weights_t );

print( class_weights );

class_acc_t = graph.get_tensor_by_name("model/finals/running_accuracy/classwise_accuracy:0");  
class_acc = sess.run( class_acc_t );
print( class_acc );


#
#centroid_center = graph.get_tensor_by_name("model/finals/loss_ddxentr/predictions/dd_center:0");  
#centroid_sigmas = graph.get_tensor_by_name("model/finals/loss_ddxentr/predictions/dd_sigmas:0");  
#val_center = sess.run( centroid_center );
#val_sigmas = sess.run( centroid_sigmas )
#print( "Average center L2: ", np.mean( val_center**2 ) );
#print( "Total center L2: ", np.sum( val_center**2 ) );
#print( "Average sigma L2: ", np.mean( val_sigmas**2 ) );
#print( "Total sigma L2: ", np.sum( val_sigmas**2 ) );




















