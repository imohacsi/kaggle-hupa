#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 19 22:59:10 2018

@author: imohacsi
"""

import os
import numpy as np



directory = "./test"


filelist = os.listdir( directory );

first_letters = np.unique( [ item[0] for item in filelist ] );


for letter in first_letters:
    dirname = "%s/%s" % (directory, letter);
    if not os.path.exists( dirname ):
        os.mkdir( dirname );
        
        
for file in filelist:
    source = "%s/%s" % (directory, file)
    dest = "%s/%s/%s" % (directory,file[0],file);
#    print( dest )
    if os.path.isfile( source ):
        os.rename(source, dest);