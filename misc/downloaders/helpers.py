#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 11 12:24:16 2018

@author: imohacsi
"""

import urllib
from bs4 import BeautifulSoup
import hashlib, os



cls_dict = {
        "actin filaments" : 12,
        "aggresome": 24, 
        "cell junctions": 22,
        "centrosome": 19, 
        "cytokinetic bridge": 16,
        "cytoplasmic bodies": 26, 
        "cytosol": 25,
        "endoplasmic reticulum": 6,
        "endosomes": 9, 
        "focal adhesion sites": 13,
        "golgi apparatus": 7,
        "intermediate filaments": 11,
        "lipid droplets": 20, 
        "lysosomes": 10,
        "microtubule ends": 15, 
        "microtubule organizing center": 18,
        "microtubules": 14,
        "mitochondria": 23, 
        "mitotic spindle": 17,
        "nuclear bodies": 5, 
        "nuclear membrane": 1,
        "nuclear speckles": 4,
        "nucleoli": 2,
        "nucleoli fibrillar center": 3,
        "nucleoplasm": 0,
        "peroxisomes": 8,
        "plasma membrane": 21,
        "rods & rings": 27,
        #"Cleavage furrow": -1, 
        #"Midbody": -1, 
        #"Midbody ring": -1,
        #Vesicles"	GO:0043231
        #Nucleus"	GO:0005634
        };
    


def download_if_nexist( r_url, l_name ):
    if not os.path.isfile( l_name ):
        try:
            urllib.request.urlretrieve( r_url, l_name );
        except:
            print("WARNING: Failed to retrieve image: ", r_url);
            

def download_xml( xml_path, entry_id ):
    try:
        xml_url = "http://www.proteinatlas.org/" + entry_id + ".xml";
        xml_file = xml_path + "/" + entry_id + ".xml";
        urllib.request.urlretrieve( xml_url, xml_file );
    except:
        pass;
        
        
def download_entry( xml_path, img_path, entry_id ):
    try:
        xml_file = xml_path + "/" + entry_id + ".xml";
        fp = open(xml_file,'r');
        soup = BeautifulSoup( fp, features='xml' );
        
        cells = soup.findAll('subAssay')
        for cell in cells:
            lines = cell.findAll('data');
            for line in lines:
                asimgs = line.findAll('assayImage');
                for asimg in asimgs:
                    imgs = line.findAll('image');
                    for img in imgs:
                        rgb_url = img.find('imageUrl'); 
                        rgb_url = rgb_url.string 
                        
                        #Generate hash
                        imghash = hashlib.sha1( rgb_url.encode() ).hexdigest();
                        
                        #Download the individual channels
                        r_url = rgb_url[:-19] + "_red.jpg";
                        r_name= img_path + "/" + imghash[0] + "/" + imghash + "_red.jpg";
                        download_if_nexist( r_url, r_name );
                        g_url = rgb_url[:-19] + "_green.jpg";
                        g_name= img_path + "/" + imghash[0] + "/" + imghash + "_green.jpg";
                        download_if_nexist( g_url, g_name );
                        b_url = rgb_url[:-19] + "_blue.jpg";
                        b_name= img_path + "/" + imghash[0] + "/" + imghash + "_blue.jpg";
                        download_if_nexist( b_url, b_name );
                        y_url = rgb_url[:-19] + "_yellow.jpg";
                        y_name= img_path + "/" + imghash[0] + "/" + imghash + "_yellow.jpg";
                        download_if_nexist( y_url, y_name );
                        #Finally downloading a color image as well
                        c_url = rgb_url;
                        c_name= img_path + "/" + imghash[0] + "/" + imghash + "_red_green_blue.jpg";
                        download_if_nexist( c_url, c_name );   
                                
        fp.close()
        return  1
    except:
        return 0;
    
    

def generate_labels( xml_path, entry_id ):
    xml_file = xml_path + "/" + entry_id + ".xml";
    fp = open(xml_file,'r');
    soup = BeautifulSoup( fp, features='xml' );

    found_img = [];

    cells = soup.findAll('subAssay')
    for cell in cells:
        lines = cell.findAll('data');
        for line in lines:
            locs = line.findAll('location')
            labels = [ ll.string.lower() for ll in locs ];
            tags = [ cls_dict[ll] for ll in labels if ll in cls_dict.keys() ];
            
            
            asimgs = line.findAll('assayImage');
            for asimg in asimgs:
                imgs = line.findAll('image');
                for img in imgs:
                    #Try to loose only one entry
                    try:
                        rgb_url = img.find('imageUrl'); 
                        rgb_url = rgb_url.string #prettify().split()[1];      
                        imghash = hashlib.sha1( rgb_url.encode() ).hexdigest();
                            
                        #Adding to the list of images    
                        found_img.append([imghash]+[tags])
                    except:
                        print("Failed to oad labels from: ", rgb_url )
                        pass;                       
    fp.close()
#    print( found_img )
    return found_img;






