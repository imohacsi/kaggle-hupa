import pandas as pd
import numpy as np
from multiprocessing import Pool

import helpers

tsvfile = "subcellular_location.tsv";
xmlpath = "./xml_files";
img_path= "./img_files";

df = pd.DataFrame.from_csv(tsvfile, sep='\t')
df =  df.reset_index();

cls_dict = helpers.cls_dict;
        

database = [];
for idx, entry in df.iterrows():
    name = entry['Gene'];
    database.append( [name] );
    if idx%1000==0:
        print("%d of %d" % (idx,len(df)) );
#    if idx==220:
#        break;

db = pd.DataFrame(database, columns=['Id']);
l_db = list(db['Id'])

#"""First download the XML files"""
#for idx,lid in enumerate(l_db):
#    ans = helpers.download_xml(xmlpath, lid);
#    if idx%200==0:
#        print( "%d of %d (xml)" % (idx, len(l_db)) )
#        
        
#"""Then download the images"""
#for idx,lid in enumerate(l_db):
#    helpers.download_entry( xmlpath, img_path, lid);
#    if idx%50==0:
#        print( "%d of %d (img)" % (idx, len(l_db)) )

"""Finally, collect the labels"""
final_db = [];
for idx,lid in enumerate(l_db):
    answ = helpers.generate_labels( xmlpath, lid);
    final_db = final_db+answ;
    if idx%50==0:
        print( "%d of %d (lab)" % (idx, len(l_db)) )


final_entr = []
for entry in final_db:
    labs = ""
    for ll in entry[1]:
        labs = labs + "%d "% ll;    
    labs = labs[:-1];
    final_entr.append( [entry[0]]+[labs] )


final_df = pd.DataFrame( final_entr, columns=['Id','Target'] );
final_df = final_df.to_csv('./additional_images.csv', index=False)










