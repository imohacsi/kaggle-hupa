# -*- coding: utf-8 -*-
import tensorflow as tf
import numpy as np

#from imgaug import augmenters as iaa 

FLAGS = tf.app.flags.FLAGS


#seq_inplace = iaa.Sequential([
#        #iaa.AdditiveGaussianNoise(loc=0,scale=(0.0,6.0), per_channel=0.5),
#        iaa.SomeOf( (0,2),
#            iaa.OneOf([
#                iaa.GaussianBlur((0.0,1.5)),
#                iaa.AverageBlur(k=(1,3)),
#                iaa.MedianBlur(k=(1,3)) ]),
#            iaa.OneOf([
#                iaa.Sharpen(alpha=(0, 0.7), lightness=(0.75, 1.25)),
#                iaa.Emboss(alpha=(0, 0.7), strength=(0, 1.0)) ]),                   
#            iaa.Sometimes( 0.3, iaa.Dropout( (0.01,0.05), per_channel=0.5 ) ) )                  
#        ], random_order=True);  
#
#def wrap_inplace( x_in ):
#    return seq_inplace.augment_image( x_in );
#
#seq_morpho = iaa.Sequential([
##        iaa.CoarseDropout((0.0, 0.05), size_percent=(0.05, 0.25))
##        iaa.Sometimes(iaa.Crop(percent=(0, 0.3))),
#        iaa.Affine(
#            scale={"x": (0.8, 1.2), "y": (0.8, 1.2)},
#            translate_percent={"x": (-0.1, 0.1), "y": (-0.1, 0.1)},
#            rotate=(-15, 15),
#            shear=(-8, 8),
#            order=0,
#            cval=(0) )
#            #mode=iaa.ALL 
##        iaa.Sometimes(0.5,iaa.PiecewiseAffine(scale=(0.01, 0.1)))
#        ], random_order=True);    
#           
#def wrap_morpho( x_in ):
#    seq_morpho_det = seq_morpho.to_deterministic();
#    return seq_morpho_det.augment_image( x_in );



class imagestream_hupa():
    def __init__(self, data_shuffle, data_prefetch, is_test=False,
                 do_augment=False, do_loopit=True, do_whiten=False, do_prefetch=True, iter_type='initializable' ):
        self.image_size = tf.constant( [FLAGS.image_crop_x,FLAGS.image_crop_y], dtype=tf.int32 );
        self.image_zoom = tf.constant( FLAGS.image_zoom, dtype=tf.int32 );
        self.num_threads= FLAGS.num_threads;
        self.data_shuffle = data_shuffle;
        self.data_prefetch= data_prefetch;
        self.is_test = is_test;
        self.do_augment = do_augment;
        self.do_loopit = do_loopit;
        self.do_whiten = do_whiten;
        self.do_prefetch = do_prefetch;
        self.iter_type = iter_type;
        
        self.create_location_map( FLAGS.image_crop_x, FLAGS.image_crop_y );
        #Creating the actual image streaming pipeline
        self.set_up_pipeline_hupa( 28 )

    def create_location_map( self, Nx, Ny ):
        x_vec = np.arange( Nx )*1.0/float(Nx);
        y_vec = np.arange( Ny )*1.0/float(Ny);
        
        xy_map = np.zeros( [Nx,Ny,2] );
        for xx in range(Ny):
            xy_map[xx,:,0] = x_vec;
        for yy in range(Nx):
            xy_map[:,yy,1] = y_vec;
            
        self.location_map = tf.Variable( xy_map, trainable=False, dtype=tf.float32 );        

               
    def set_up_pipeline_hupa( self, num_classes ):
    
        self.plholder_fnameB = tf.placeholder( tf.string, [None] );
        self.plholder_fnameG = tf.placeholder( tf.string, [None] );
        self.plholder_fnameY = tf.placeholder( tf.string, [None] );
        self.plholder_fnameR = tf.placeholder( tf.string, [None] );
        self.plholder_labels = tf.placeholder( tf.float32, [None,num_classes] );
        self.plholder_fnames = [self.plholder_fnameB,self.plholder_fnameG,self.plholder_fnameY,self.plholder_fnameR];
        
        
        self.data_set = tf.data.Dataset.from_tensor_slices( ( self.plholder_fnameB,self.plholder_fnameG,self.plholder_fnameY,self.plholder_fnameR, self.plholder_labels) );
        """Loop and repeat the filenames (not the images)"""
        if self.do_loopit:
            self.data_set = self.data_set.shuffle( buffer_size=self.data_shuffle );
            self.data_set = self.data_set.repeat( );       
        self.data_set = self.data_set.map( lambda f1,f2,f3,f4,lb: _tf_read_image_hupa(f1,f2,f3,f4,lb,self.image_size,self.image_zoom,self.do_augment), num_parallel_calls=FLAGS.num_threads );
        self.data_set = self.data_set.map( lambda im,lb: _tf_proc_image_hupa(im,lb,self.do_augment,self.do_whiten), num_parallel_calls=FLAGS.num_threads );
        self.data_set = self.data_set.batch( FLAGS.batch_size );        
       
        if self.do_prefetch:
            self.data_set = self.data_set.prefetch( self.data_prefetch );
    
        if self.iter_type=='initializable':
            self.dataset_iterator = self.data_set.make_initializable_iterator();
        elif self.iter_type=='reinitializable':
            self.dataset_iterator = self.data_set.make_reinitializable_iterator();
        else:
            assert False,'ERROR: unknown iterator type for dataset.';        
    
    def get_keypoints( self ):
        return self.plholder_fnames,self.plholder_labels,self.data_set,self.dataset_iterator;
    
    
    
def _tf_read_image_hupa( f1,f2,f3,f4, label, num_pixels, num_zoom, do_aug ):
    bytecode1 = tf.image.decode_jpeg( tf.read_file(f1), channels=1 );
    bytecode2 = tf.image.decode_jpeg( tf.read_file(f2), channels=1 );
    bytecode3 = tf.image.decode_jpeg( tf.read_file(f3), channels=1 );
#    bytecode4 = tf.image.decode_jpeg( tf.read_file(f4), channels=1 );
    image_decoded = tf.concat([bytecode1,bytecode2,bytecode3],axis=-1)
    
    """Ensuring that no image dimension will be smaller than num_zoom """
    oshape = tf.shape( image_decoded );
    is_landscape = tf.greater_equal( oshape[0], oshape[1] ); #Means x>=y
    nshort = num_zoom;
    new_h, new_w = tf.cond( is_landscape,
            lambda: [ tf.to_int32( (oshape[0]*nshort) / oshape[1] ), nshort ],                
            lambda: [ nshort, tf.to_int32( (oshape[1]*nshort) / oshape[0] ) ] );
                           
    """Finally resize the image to the final scale"""
    img = tf.image.resize_images( image_decoded, [new_h,new_w]);    
    """Randomly crop a ROI from the image"""
    if do_aug:
        img = tf.to_float( tf.random_crop( img, [num_pixels[0],num_pixels[1],3] ) );   
    else:
        img = tf.image.resize_image_with_crop_or_pad(img,num_pixels[0],num_pixels[1] );
        
    return img,label


def _tf_proc_image_hupa( img, label, do_aug, do_whiten, loc_map=None ):
    
    """Doing the augmentation"""
#    if do_aug:
#        oishape = tf.shape( img );    
#        img = tf.py_func( wrap_inplace, [img], tf.float32 );
#        img = tf.py_func( wrap_morpho,  [img],tf.float32 );
#        img = tf.reshape( img, oishape );
    
    """Randomly rotate original image by 90 deg"""
    coin = tf.less(tf.random_uniform((), 0., 1.), 0.5)
    img = tf.cond( coin, lambda: tf.image.rot90(img), lambda: img );
            
    """Randomly crop a ROI from the image"""
    if do_aug:
        img = tf.image.random_flip_up_down( img );
        img = tf.image.random_flip_left_right( img );
        img = img + tf.random_normal( tf.shape(img), 0.0,3.0, tf.float32 );  

    img = tf.scalar_mul( (1.0/128.0), img )-1.0;

    return img,label








