# -*- coding: utf-8 -*-
"""
Created on Thu Feb  1 16:50:35 2018

@author: mohacsii
"""
import matplotlib.pyplot as plt
import tensorflow as tf
import numpy as np
import pandas as pd
import time, os

from utilities import eval_ops
from fileio import fileio_hupa
#try:
#    from fileio import imagestream_hupa_imaug as imagestream_hupa
#except:
from fileio import imagestream_hupa_tf as imagestream_hupa
from graph.modlib.cls import model_template as model


FLAGS = tf.app.flags.FLAGS

#Basic learning environment variables
tf.app.flags.DEFINE_string("encoding_path","../encodings","""Folder for reading previous checkpoints""" );
tf.app.flags.DEFINE_string("encoding_name","hupa_xception24_x0.5_MILv0_featAVG_c1kd1k_foc0.9G2_Qpool-207k_desy_tf_forced","""Name of the loaded checkpoint""" );
tf.app.flags.DEFINE_string("loadfile_path","../savefiles/hupa_xception24_x0.5_MILv0_featAVG_c1kd1k_foc0.9G2_Qpool","""Folder for reading previous checkpoints""" );
tf.app.flags.DEFINE_string("loadfile_name","hupa_xception24_x0.5_MILv0_featAVG_c1kd1k_foc0.9G2_Qpool-207051","""Name of the loaded checkpoint""" );

tf.app.flags.DEFINE_integer("batch_size",64,"""Number of images in a batch""" );
tf.app.flags.DEFINE_integer("num_threads",4,"""Number of threads for preprocessing""" );
tf.app.flags.DEFINE_integer("image_crop_x",192,"""Number of pixels bv   to be cropped from the image in X""" );
tf.app.flags.DEFINE_integer("image_crop_y",192,"""Number of pixels to be cropped from the image in Y""" );
tf.app.flags.DEFINE_integer("image_zoom",256,"""Number of pixels on the shorter edge of the resized image""" );
tf.app.flags.DEFINE_integer("num_test_eval",128,"""Number of evaluations for final testing""" );

tf.app.flags.DEFINE_integer("image_shuffle",1000,"""Number of filenames to be shuffled""" );
tf.app.flags.DEFINE_integer("image_prefetch",2,"""Number of batches to be prefetched""" );

datapath = "/gpfs/petra3/scratch/mohacsii/HuPA/test"
traincsv  = "/gpfs/petra3/scratch/mohacsii/HuPA/train.csv"
#datapath = "/run/media/imohacsi/DataSSD/Kaggle/HumanProteinAtlas/test"
#traincsv = "/run/media/imohacsi/DataSSD/Kaggle/HumanProteinAtlas/train.csv"
#datapath = "/home/imohacsi-adm/Downloads/HuPA/test"
#traincsv = "/home/imohacsi-adm/Downloads/HuPA/train.csv"



"""Running the actual main function"""
#if __name__ == "__main__":
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"  
os.environ["CUDA_VISIBLE_DEVICES"]="0"; # % (0); 

"""Image data preprocessor pipeline"""
with tf.variable_scope("datastream",reuse=None):
    test_preproc = imagestream_hupa.imagestream_hupa(FLAGS.image_shuffle,FLAGS.image_prefetch,
                                                   True,False,True,False,is_test=True).get_keypoints();
    files_te,labels_te,test_set ,iterator_te = test_preproc;

"""Loading test images"""
filelist_test = fileio_hupa.get_test_filelist( datapath );
print("Number of testing examples: ", len(filelist_test) );

"""Loading pre-trained model"""
#tf.reset_default_graph();
cgraph = model.model_template(28);

metafile_name = '%s/%s.meta' % ( FLAGS.loadfile_path, FLAGS.loadfile_name)        
loader = tf.train.import_meta_graph( metafile_name, clear_devices=False );  
    

"""Loading named entities from the graph"""
cgraph.load_graph_checkpoints();
tensor_names = [t.name for op in tf.get_default_graph().get_operations() for t in op.values()];


print("Initializing variables!")


init = tf.global_variables_initializer();
"""Set up the GPU computation environment"""
gpu_options = tf.GPUOptions(allow_growth=True, allocator_type="BFC");
gpuconfig = tf.ConfigProto(gpu_options=gpu_options,allow_soft_placement=False);
#sess=tf.InteractiveSession( config=gpuconfig);
sess = tf.train.MonitoredTrainingSession(master="", is_chief=True, hooks=None,
                                       save_checkpoint_secs=600, checkpoint_dir=None,
                                       save_summaries_steps=500, config=gpuconfig );   
sess.run(init)
handle_te = sess.run( iterator_te.string_handle() )


#Restoring model parameters
datafile_name = '%s/%s' % ( FLAGS.loadfile_path, FLAGS.loadfile_name );
try:
    loader.restore( sess, datafile_name )
except Exception as e:
    print(e);
    pass;
    

predictions = eval_ops.evaluate_test( sess, filelist_test, files_te, labels_te, iterator_te, handle_te, cgraph, FLAGS.num_test_eval );


""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""
"""   Statistical re-evaluation  """
""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""

df_pred =  pd.DataFrame( predictions )

filelist_train = fileio_hupa.filelist_hupa( traincsv,"../").df;
train_stat = fileio_hupa.filelist_hupa.get_cls_stat( filelist_train );
lb_stat = np.array([0.36239782,0.043841336,0.075268817,0.059322034, 0.075268817, 0.075268817,  0.043841336,
           0.075268817, 0, 0, 0, 0.043841336, 0.043841336, 0.014198783, 0.043841336, 0, 0.028806584,
           0.014198783, 0.028806584, 0.059322034, 0, 0.126126126, 0.028806584, 0.075268817, 0, 
           0.222493888, 0.028806584, 0 ]);    

comb_stat = np.where( np.array(lb_stat)>0.0, lb_stat, train_stat)




"""
Test results:
Train: 0.404
Lboard: 0.363
Mixed: 0.408
Zeros: 0.392

"""


filename = FLAGS.encoding_path + '/' + FLAGS.encoding_name + '_train.csv';
auto_th = eval_ops.get_auto_th( train_stat, df_pred );
df_subm = eval_ops.predict_classes( df_pred, auto_th );
df_writ = eval_ops.write_submission( df_subm, filename );
#eval_ops.plot_statistics( df_writ , lb_stat, train_stat );


filename = FLAGS.encoding_path + '/' + FLAGS.encoding_name + '_lboard.csv';
auto_th = eval_ops.get_auto_th( lb_stat, df_pred );
df_subm = eval_ops.predict_classes( df_pred, auto_th );
df_writ = eval_ops.write_submission( df_subm, filename );
#eval_ops.plot_statistics( df_writ , lb_stat, train_stat );

filename = FLAGS.encoding_path + '/' + FLAGS.encoding_name + '_mixed.csv';
auto_th = eval_ops.get_auto_th( comb_stat, df_pred );
df_subm = eval_ops.predict_classes( df_pred, auto_th );
df_writ = eval_ops.write_submission( df_subm, filename );
#eval_ops.plot_statistics( df_writ , lb_stat, train_stat );

filename = FLAGS.encoding_path + '/' + FLAGS.encoding_name + '_zeros.csv';
auto_th = np.zeros([28])
df_subm = eval_ops.predict_classes( df_pred, auto_th );
df_writ = eval_ops.write_submission( df_subm, filename );
#eval_ops.plot_statistics( df_writ , lb_stat, train_stat );
