#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  8 14:45:01 2018

@author: imohacsi-adm
"""

import tensorflow as tf
import pandas as pd
import numpy as np
import scipy.stats as st
import matplotlib.pyplot as plt
import sys,os
sys.path.append("..")

from fileio import fileio_hupa


os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"  
os.environ["CUDA_VISIBLE_DEVICES"]=""; # % (0); 

datapath = "/run/media/imohacsi/DataSSD/Kaggle/HumanProteinAtlas/test"
traincsv = "/run/media/imohacsi/DataSSD/Kaggle/HumanProteinAtlas/train.csv"

savefile_path = "../../savefiles/hupa_xception24_x0.5_MILv0_probAVG_focAUTO2"
savefile_name = "hupa_xception24_x0.5_MILv0_probAVG_focAUTO2-207051"
#savefile_path = "../../savefiles/hupa_xception24_x0.5_MILv0_probAVG_focAUTO2G1"
#savefile_name = "hupa_xception24_x0.5_MILv0_probAVG_focAUTO2G1-50501"


"""Obtaiing training set class statistics"""
filelist_train = fileio_hupa.filelist_hupa( traincsv,"../").df;
train_stat = fileio_hupa.filelist_hupa.get_cls_stat( filelist_train );


"""Load graph"""
metafile_name = '%s/%s.meta' % ( savefile_path, savefile_name )        
loader = tf.train.import_meta_graph( metafile_name, clear_devices=True );   
tensor_names = [t.name for op in tf.get_default_graph().get_operations() for t in op.values()];
variable_names = [ v.name for v in tf.trainable_variables() ]

#print( tensor_names )
#for v in variable_names:
#   print(v)

"""Set up the GPU computation environment"""
gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.8, allow_growth=True, allocator_type="BFC");
config = tf.ConfigProto(gpu_options=gpu_options,allow_soft_placement=False);

sess = tf.train.MonitoredTrainingSession(master="", is_chief=True, hooks=None,
                                       save_checkpoint_secs=600, checkpoint_dir=None,
                                       save_summaries_steps=500, config=config );   


"""Restore variables from save file"""
datafile_name = '%s/%s' % ( savefile_path, savefile_name );    
loader.restore( sess, datafile_name );    



graph = tf.get_default_graph();

class_weights_t = graph.get_tensor_by_name("model/finals/loss_focal/class_weights:0");  
class_acc_t = graph.get_tensor_by_name("model/finals/running_accuracy/classwise_accuracy:0");  
class_weights = np.squeeze( sess.run( class_weights_t ) );
class_acc = sess.run( class_acc_t );

print( class_weights );
print( class_acc );


class_stats = pd.DataFrame( train_stat.copy() );
class_stats['class_weights'] = class_weights;
class_stats['class_acc'] = class_acc;



plt.scatter( class_stats.iloc[:,0], class_stats.iloc[:,2] )
plt.grid()
plt.show(block=False)
plt.pause(0.1);









