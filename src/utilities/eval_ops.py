#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  8 01:16:20 2018

@author: imohacsi
"""



import pandas as pd
import numpy as np
import scipy.stats as st
import matplotlib.pyplot as plt
import time, sys,os
sys.path.append("..")
from fileio import fileio_hupa


""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""
"""   Statistical re-evaluation  """
""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""


#pf =  pd.DataFrame( predictions )
#
#filelist_train = fileio_hupa.filelist_hupa( traincsv,"../").df;
#train_stat = fileio_hupa.filelist_hupa.get_cls_stat( filelist_train );
#lb_stat = [0.36239782,0.043841336,0.075268817,0.059322034, 0.075268817, 0.075268817,  0.043841336,
#           0.075268817, 0, 0, 0, 0.043841336, 0.043841336, 0.014198783, 0.043841336, 0, 0.028806584,
#           0.014198783, 0.028806584, 0.059322034, 0, 0.126126126, 0.028806584, 0.075268817, 0, 
#           0.222493888, 0.028806584, 0 ];    
#
##comb_stat = np.where( np.array(lb_stat)>0.0, lb_stat, train_stat)
##comb_stat = lb_stat
#comb_stat = train_stat

def get_auto_th( cls_stat, df_pred ):
    print("Calculating the automatic thresholds based on class statistics")

    auto_th = []        
    for cc in range(28):
        ccol = df_pred[cc+1].sort_values().copy();
        ccol = ccol.reset_index(drop=True);
        c_th = ccol.iloc[ max( 0, min( int( (1.0-cls_stat[cc])*len(ccol)+1 ), len(ccol)-1 ) ) ];
        auto_th.append( c_th );
               
    auto_th = np.array( auto_th );           
    return auto_th;



""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""
"""   Statistical re-evaluation  """
""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""

def predict_classes( df_pred, auto_th ):
    print("Thresholding the dataset...")
    df =  df_pred.copy();
    submissions = [];
    for idx,entry in df.iterrows():
#        predicted = [];
        prediction_prob = np.array( entry.iloc[1:] );
        th = auto_th;
        while True:
            predicted = list(np.where( prediction_prob > th )[0] );
            th = th-0.1;
            if len(predicted)>0:
                break;     
        submissions.append( [entry[0], predicted] );
    df_sub =  pd.DataFrame( submissions, columns=['Id','labels'] )

    return df_sub;
        

""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""
"""      Make the submission     """
""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""


def write( x ):
    try:
        ans="%d" % (x[0]);
        for w in x[1:]:
            ans += " %d" % (w);
        return ans;
    except:
        return "";

def write_submission( df_sub, filename ):
    print("Generating submission text")
    df =  df_sub.copy();
    df['Predicted']  = df['labels'].apply( lambda x: write(x) )
    df = df.drop(['labels'], axis=1);
    df = df.sort_values(['Id'], ascending=True);

    print(filename);
    df.to_csv( filename,index=False );
    return df;

""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""
"""        Final plotting        """
""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""
def plot_statistics( df , lb_stat, train_stat ):
    print("Plotting statistics")
    
    plabels = df['Predicted'].apply(lambda x: fileio_hupa.encode_multilabel(x,28) );
    plabels = pd.DataFrame( np.array( list(plabels) ) );
    pred_stat = plabels.sum()/len(plabels);
    
    plt.plot(lb_stat)
    plt.plot(train_stat)
    plt.plot( pred_stat)
    plt.legend(["LeaderBoard","TrainSet","Predictions"]);
    plt.show(block=False)
    plt.pause(0.01)





def evaluate_test( sess, filelist_test, files_plh, labels_plh, iterator_te, handle_te, cgraph, num_test_eval, labels_real=[], detailed=False, add_labels=0 ):
    predictions = []; 
    #Declare dummy placeholders
    labels = [];
    logits_mean = [];
    logits_std  = [];
    logits_skew = [];


    dummy_labels = np.array([np.zeros([28])]*num_test_eval )
    filelist_test = filelist_test.reset_index();
    for idx,entry in filelist_test.iterrows():
        tstart = time.time();

        entry_l = pd.DataFrame([entry]*num_test_eval);
        tr_dirs = { files_plh[0]: entry_l['blue'], files_plh[1]: entry_l['green'], 
                   files_plh[2]: entry_l['yellow'], files_plh[3]: entry_l['red'],
                     labels_plh: dummy_labels }
        sess.run( iterator_te.initializer, feed_dict=tr_dirs );

        logits_map = sess.run( cgraph.res_logit, feed_dict={ cgraph.input_handle: handle_te, cgraph.is_training: False } ); 
        logits_mean = list( np.mean( logits_map, axis=0 ) );
        if detailed:
            logits_std  = list(np.std(  logits_map, axis=0 ));
            logits_skew = list(st.skew( logits_map, axis=0 ));
        if add_labels<0:
            labels = list( np.zeros([28]) );
        elif add_labels>0:
            labels = list(entry[6:]);

        predictions.append( [entry['Id']] + labels +logits_mean + logits_std + logits_skew  ); 

        elapsed = time.time()-tstart;
        if idx%200==0:
            print("Calculated %d of %d in %f sec..." % (idx,len(filelist_test),elapsed) );    
    return predictions;















