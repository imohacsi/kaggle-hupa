# -*- coding: utf-8 -*-
"""
Created on Wed Aug 22 14:49:02 2018

@author: mohacsii
"""
import tensorflow as tf
import numpy as np
import sys,os
sys.path.append("..")

from fileio import fileio_cls
from fileio import fileio_hupa
from fileio import fileio_seg
from fileio import imagestream_cls
from fileio import pproc_format

try:
    from fileio import imagestream_seg_imaug as imagestream_seg
except:
    print("\nWARNING: using legacy augmenter!\n")
    from fileio import imagestream_seg_tf as imagestream_seg
try:
    from fileio import imagestream_hupa_imaug as imagestream_hupa
except:
    print("\nWARNING: using legacy augmenter!\n")
    from fileio import imagestream_hupa_tf as imagestream_hupa

from graph import build_graph
from graph.modlib.cls import model_template as model_cls





from diag import diag

FLAGS = tf.app.flags.FLAGS

def build_file_lists( datapath, num_class, style, csvfile=None, rseed=123456 ):
    """Read training file names"""
    class_freq = None;
    if FLAGS.model_type in ["cls","classification"]: 
        filelist_raw,num_class = fileio_cls.get_train_filelist( datapath, style ).load_filelist();
    elif FLAGS.model_type in ["hupa","hyspec","hyperspectral"]: 
        filelist_raw,num_class = fileio_hupa.filelist_hupa( csvfile, datapath ).load_filelist();
    elif FLAGS.model_type in ["seg","segmentation"]: 
        filelist_raw = fileio_seg.get_train_filelist( datapath, style );   
    filelist_train = filelist_raw.sample( frac=FLAGS.test_split, random_state=rseed );
    filelist_valid = filelist_raw.drop( filelist_train.index ); 
    if FLAGS.model_type in ["hupa","hyspec","hyperspectral"]: 
        filelist_train = fileio_hupa.class_rebalance( filelist_train, 0.002 )
        filelist_valid = fileio_hupa.class_rebalance( filelist_valid, 0.025 )
        class_freq =  fileio_hupa.filelist_hupa.get_cls_stat( filelist_train );
        
    print("Training set length %d examples\nEpoch length: %f iterations" % (len(filelist_train),len(filelist_train)/FLAGS.batch_size) );

    return filelist_train,filelist_valid,num_class,class_freq

def build_graph_ops( device, is_chief, num_class, d_in=5, oldvar=None, class_freq=None ):
    """Assigns ops to the local worker by default."""
    with tf.device(device):
        if FLAGS.do_load_checkpoint==1:
            if FLAGS.model_type in ["cls","classification"]: 
                cgraph = model_cls.model_template(-1);
                loader = cgraph.load_old_graph( FLAGS.loadfile_path, FLAGS.loadfile_name ) 
            elif FLAGS.model_type in ["hupa"]: 
                cgraph = model_cls.model_template(-1);
                loader = cgraph.load_old_graph( FLAGS.loadfile_path, FLAGS.loadfile_name ) 
            cgraph.load_graph_checkpoints();
        else:   
            loader=None;
            """Set up computation graph"""
            cgraph = build_graph.get_model_from_library(FLAGS.model_name, num_class, mtype=FLAGS.model_type, class_freq=class_freq );
            cgraph.set_model_width( FLAGS.model_width );
            cgraph.create_new_model( d_in=d_in );
            cgraph.load_graph_checkpoints();

        """Set up diagnostics"""
        diag.print_num_trainpar( FLAGS.summary_path );
        if FLAGS.model_type in ["cls","classification"]: 
            train_writer,valid_writer,merged_summary = diag.prepare_summary_writer_cls( cgraph, FLAGS.summary_path, FLAGS.diag_image_summary );
        elif FLAGS.model_type in ["hupa","hyspec","hyperspectral"]: 
            train_writer,valid_writer,merged_summary = diag.prepare_summary_writer_cls( cgraph, FLAGS.summary_path, FLAGS.diag_image_summary );
        elif FLAGS.model_type in ["seg","segmentation"]: 
            train_writer,valid_writer,merged_summary = diag.prepare_summary_writer_seg( cgraph, FLAGS.summary_path, FLAGS.data_style );

        #Get r create saver
        graph = tf.get_default_graph();   
        try:
            graph_saver = graph.get_tensor_by_name("saver/graph_saver:0");
        except:
            with tf.name_scope("saver"):
                graph_saver = tf.train.Saver(max_to_keep=5, keep_checkpoint_every_n_hours=1.5, name='graph_saver');  
             
        global_step = tf.train.get_or_create_global_step();
    
    if is_chief and FLAGS.do_use_warminit and (oldvar is not None):
        graph = tf.get_default_graph();
#        variables_train = tf.trainable_variables()
        old_variables   = oldvar;
        assignment_list =[];
        for vv in old_variables:
            try:
                assignment_list.append( vv + [ tf.assign( graph.get_tensor_by_name(vv[0]), vv[1] )  ] );
            except Exception as e:
                print(e);
    else:
        assignment_list=[];
        
    return cgraph, graph_saver, global_step, loader, assignment_list, train_writer,valid_writer,merged_summary

    """Set up data preprocessing"""
def build_data_preproc():
    
    with tf.variable_scope("datastream",reuse=None):  
        if FLAGS.model_type in ["cls","classification"]: 
            with tf.variable_scope("train_set",reuse=None):  
                train_preproc = imagestream_cls.imagestream_cls(FLAGS.image_shuffle,FLAGS.image_prefetch,False,True,True,True).get_keypoints();
            with tf.variable_scope("valid_set",reuse=None):  
                valid_preproc = imagestream_cls.imagestream_cls(FLAGS.image_shuffle,FLAGS.image_prefetch,False,True,True,True).get_keypoints();
        elif FLAGS.model_type in ["hupa","hyspec","hyperspectral"]: 
            with tf.variable_scope("train_set",reuse=None):  
                train_preproc = imagestream_hupa.imagestream_hupa(FLAGS.image_shuffle,FLAGS.image_prefetch,True,True,True,is_test=False).get_keypoints();
            with tf.variable_scope("valid_set",reuse=None):  
                valid_preproc = imagestream_hupa.imagestream_hupa(FLAGS.image_shuffle,FLAGS.image_prefetch,True,True,True,is_test=True ).get_keypoints();
        elif FLAGS.model_type in ["seg","segmentation"]: 
            train_format  = pproc_format.pproc_format( True,True,True,True,True,True,True,True,True );
            valid_format  = pproc_format.pproc_format( True,True,True,True,True,True,True,True,False );
    #        train_format  = pproc_format.pproc_format( False,True,False,True,True,True,True,True,False );
    #        valid_format  = pproc_format.pproc_format( False,True,False,True,True,True,True,True,False );
            with tf.variable_scope("train_set",reuse=None):  
                train_preproc = imagestream_seg.imagestream_seg(FLAGS.image_shuffle,FLAGS.image_prefetch,
                                                            train_format,False,True,True).get_keypoints();
            with tf.variable_scope("valid_set",reuse=None):  
                valid_preproc = imagestream_seg.imagestream_seg(FLAGS.image_shuffle,FLAGS.image_prefetch,
                                                            valid_format,False,True,True).get_keypoints();
    return train_preproc,valid_preproc
    

def build_os_env():
    """Running the actual main function"""
    os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"  
    os.environ["CUDA_VISIBLE_DEVICES"]="%d" % (FLAGS.gpu_index); 
        
    """"Setting up the output libraries"""
    FLAGS.savefile_path = FLAGS.savefile_path + '/' + FLAGS.run_name;
    try:
        os.mkdir( FLAGS.savefile_path );
    except:
        pass
    FLAGS.summary_path = FLAGS.summary_path + '/' + FLAGS.run_name;
    try:
        os.mkdir( FLAGS.summary_path );
    except:
        pass;

def init_loaded_parameters(sess, assignment_list,is_chief=True, loader=None ):
    if is_chief and FLAGS.do_load_checkpoint and (loader is not None):
        datafile_name = '%s/%s' % ( FLAGS.loadfile_path, FLAGS.loadfile_name );    
        print("Restoring variables from: ", datafile_name)
        
        loader.restore( sess, datafile_name );    
    if FLAGS.do_use_warminit:
        print("Starting warm init...")
        for entr in assignment_list:
            try:
                sess.run( entr[2] );
            except Exception as e:
                print(e)
        print("Done with warminit")



def init_data_handles(sess,iterator_tr,iterator_vl,files_tr,files_vl,filelist_train,filelist_valid,labels_tr,labels_vl ):
    
    handle_tr = sess.run( iterator_tr.string_handle() );
    handle_vl = sess.run( iterator_vl.string_handle() );
    if FLAGS.model_type in ["cls","classification"]: 
        sess.run( iterator_tr.initializer, feed_dict={ files_tr: filelist_train['filename'], labels_tr: filelist_train['label'] } );
        sess.run( iterator_vl.initializer, feed_dict={ files_vl: filelist_valid['filename'], labels_vl: filelist_valid['label'] } );
    elif FLAGS.model_type in ["hupa","hyspec","hyperspectral"]: 
        tr_dirs = { files_tr[0]: filelist_train['blue'], files_tr[1]: filelist_train['green'], files_tr[2]: filelist_train['yellow'], 
                   files_tr[3]: filelist_train['red'],  labels_tr: np.float32(filelist_train.iloc[:,5:33]) }
        sess.run( iterator_tr.initializer, feed_dict=tr_dirs );
        vl_dirs = { files_vl[0]: filelist_valid['blue'], files_vl[1]: filelist_valid['green'], files_vl[2]: filelist_valid['yellow'], 
                   files_vl[3]: filelist_valid['red'],  labels_vl: filelist_valid.iloc[:,5:33] }
        sess.run( iterator_vl.initializer, feed_dict=vl_dirs );
    elif FLAGS.model_type in ["seg","segmentation"]: 
        sess.run( iterator_tr.initializer, feed_dict={ files_tr: filelist_train['filename'], labels_tr: filelist_train['maskname'] } );
        sess.run( iterator_vl.initializer, feed_dict={ files_vl: filelist_valid['filename'], labels_vl: filelist_valid['maskname'] } );
    return handle_tr,handle_vl





def periodic_maintenance(sess, itr, elapsed, cgraph, graph_saver, global_step, merged_summary,train_writer,valid_writer,handle_tr, handle_vl):
    
    if FLAGS.model_type in ['cls','classification']:
        if (itr%FLAGS.write_summary)==1 and itr>FLAGS.write_summary:
            try:
                _, summary_val, lval_t, aval_t = sess.run( [cgraph.train_op,merged_summary,cgraph.res_loss,cgraph.res_accur], feed_dict={ cgraph.input_handle: handle_tr, cgraph.is_training: True } );
                train_writer.add_summary(  summary_val, itr)
                summary_val, lval_v, aval_v = sess.run( [merged_summary,cgraph.res_loss,cgraph.res_accur], feed_dict={ cgraph.input_handle: handle_vl, cgraph.is_training: False } );
                valid_writer.add_summary(summary_val, itr)
                print( "Iteration %d\tLoss:\t%.3g/%.3g\tAccuracy:\t%.3g/%.3g\t%.3g sec" %(itr, lval_t,lval_v,aval_t,aval_v,elapsed ) );
            except Exception as e:
                print(e)      
    elif FLAGS.model_type in ["hupa","hyspec","hyperspectral"]: 
        if (itr%FLAGS.write_summary)==1 and itr>FLAGS.write_summary:
            try:
                _, summary_val, lval_t, aval_t = sess.run( [cgraph.train_op,merged_summary,cgraph.res_loss,cgraph.res_accur], feed_dict={ cgraph.input_handle: handle_tr, cgraph.is_training: True, cgraph.p_keep: 0.5 } );
                train_writer.add_summary(  summary_val, itr)
                summary_val, lval_v, aval_v = sess.run( [merged_summary,cgraph.res_loss,cgraph.res_accur], feed_dict={ cgraph.input_handle: handle_vl, cgraph.is_training: False } );
                valid_writer.add_summary(summary_val, itr)
                print( "Iteration %d\tLoss:\t%.3g/%.3g\tAccuracy:\t%.3g/%.3g\t%.3g sec" %(itr, lval_t,lval_v,aval_t,aval_v,elapsed ) );
            except Exception as e:
                print(e)   
    elif FLAGS.model_type in ['seg','segmentation']:
        if (itr%FLAGS.write_summary)==1 and itr>FLAGS.write_summary:
            try:
                _, summary_val, lval_t, aval_t = sess.run( [cgraph.train_op,merged_summary,cgraph.res_loss,cgraph.batch_miou ], feed_dict={ cgraph.input_handle: handle_tr, cgraph.is_training: True } );
                train_writer.add_summary(  summary_val, itr)
                summary_val, lval_v, aval_v = sess.run( [merged_summary,cgraph.res_loss, cgraph.batch_miou ], feed_dict={ cgraph.input_handle: handle_vl, cgraph.is_training: False } );
                valid_writer.add_summary(summary_val, itr)
                print( "Iteration %d\tLoss:\t%0.4g/%0.4g\tbIoU:\t%0.4g/%0.4g\t%0.4g sec" %(itr, lval_t,lval_v,aval_t,aval_v,elapsed ) );
            except Exception as e:
                print(e)
                       
    if (itr%FLAGS.save_frequency)==1 and itr>FLAGS.save_frequency:
        savename = FLAGS.savefile_path + '/' + FLAGS.run_name;
        graph_saver.save(sess._tf_sess(),savename, global_step=global_step);                
                        
            




