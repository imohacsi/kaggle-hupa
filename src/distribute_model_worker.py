import tensorflow as tf
from graph import build_graph
from utilities import kernel_transfer
from utilities import multinodes
from utilities import init_ops
import sys,time,pickle

"""Select path for training and validation data"""
#datapath = '/gpfs/petra3/scratch/mohacsii/tiny-imagenet-200/train';
#datapath = 'C:\\Users\\mohacsii\\ColdStorage\\Data\\tiny-imagenet-200\\train';
#datapath = "/run/media/imohacsi/DataSSD/Kaggle/ILSVRC/Data/CLS-LOC/train";

#datapath = "/home/imohacsi-adm/Downloads/HuPA/train"
#csvpath = "/home/imohacsi-adm/Downloads/HuPA/train.csv"
#datapath = "/run/media/imohacsi/DataSSD/Kaggle/HumanProteinAtlas/train"
#csvpath  = "/run/media/imohacsi/DataSSD/Kaggle/HumanProteinAtlas/train.csv"
#csvpath  = "/run/media/imohacsi/DataSSD/Kaggle/HumanProteinAtlas/expanded_training_set.csv"
datapath = "/gpfs/petra3/scratch/mohacsii/HuPA/train"
csvpath  = "/gpfs/petra3/scratch/mohacsii/HuPA/train.csv"

num_class = 28;

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

FLAGS = tf.app.flags.FLAGS
tf.app.flags.DEFINE_string("worker_hosts","localhost:5000","""List of worker nodes""" );
tf.app.flags.DEFINE_string("ps_hosts","localhost:4999","""List of parameter servers""" );
#tf.app.flags.DEFINE_string("job_name","worker","""Role of the current node""" );
tf.app.flags.DEFINE_string("job_name","","""Role of the current node""" );
tf.app.flags.DEFINE_integer("task_index",0,"""Number of the current node""" );
tf.app.flags.DEFINE_integer("gpu_index",0,"""Number of the current GPU""" );

#Basic learning environment variables
tf.app.flags.DEFINE_string("run_name","hupa_xception24_x0.5_featAVG_c1kd1k_foc0.7G2_Tpool","""Prefix for save data""" );
tf.app.flags.DEFINE_string("summary_path","../summaries","""Prefix for save data""" );
tf.app.flags.DEFINE_string("savefile_path","../savefiles","""Folder for saving model checkpoints""" );
tf.app.flags.DEFINE_string("picklefile_path","../picklefiles","""Folder for previous checkpoints in pickle format""" );
tf.app.flags.DEFINE_string("loadfile_path","../savefiles/hupa_xception24_x0.5_MILv0_featAVG_c1kd1k_foc0.9G2","""Folder for reading previous checkpoints""" );
tf.app.flags.DEFINE_string("loadfile_name","hupa_xception24_x0.5_MILv0_featAVG_c1kd1k_foc0.9G2-161601","""Name of the loaded checkpoint""" );
tf.app.flags.DEFINE_integer("do_use_warminit",0,"""Toggle to load a subset of variables from file""" );
tf.app.flags.DEFINE_integer("do_load_checkpoint",0,"""Toggle to resume previous checkpoint""" );

#Model style and data parameters
tf.app.flags.DEFINE_string( "model_type","hupa","""Model type: classification or segmentation""" );
tf.app.flags.DEFINE_string( "data_style","imagenet","""Model type: classification or segmentation""" );
tf.app.flags.DEFINE_integer( "dim_in",4,"""Model input dimensions""" );
tf.app.flags.DEFINE_integer( "learn_cmap",1,"""Learn a colormap transformation""" );
tf.app.flags.DEFINE_integer( "learn_cmap_dims",4,"""Learned colormap dimensionality""" );
tf.app.flags.DEFINE_float(  "test_split",0.90,"""Fraction of data used for validation""" );

#Basic model parameters
tf.app.flags.DEFINE_string( "model_name","xception24","""Name of the model from the library""" );
tf.app.flags.DEFINE_float(  "model_width",0.5,"""Relative width of the model""" );
tf.app.flags.DEFINE_integer("batch_size",32,"""Number of images in a batch""" );
tf.app.flags.DEFINE_integer("last_step",410000,"""Number of total steps during training""" );
tf.app.flags.DEFINE_integer("loss_auto_class_balance" ,0,"""Try to balance rare classes""" );
tf.app.flags.DEFINE_float(  "loss_reg_l2" ,1e-5,"""L2 regularizer weight""" );
tf.app.flags.DEFINE_float(  "loss_reg_entropy" ,0.0,"""Xentropy regularizer weight""" );
tf.app.flags.DEFINE_float(  "loss_focal_gamma" ,2.0,"""Focal loss nonlinearility""" );
tf.app.flags.DEFINE_float(  "loss_focal_alpha" ,0.7,"""Focal loss positive weight""" );

#Learning parameters
tf.app.flags.DEFINE_integer("num_threads",4,"""Number of threads for preprocessing""" );
tf.app.flags.DEFINE_integer("image_pyread",0,"""Use Python to read images""" );
tf.app.flags.DEFINE_integer("image_shuffle",1000000,"""Number of filenames to be shuffled""" );
tf.app.flags.DEFINE_integer("image_prefetch",8,"""Number of batches to be prefetched""" );

tf.app.flags.DEFINE_integer("image_crop_x",192,"""Number of pixels bv   to be cropped from the image in X""" );
tf.app.flags.DEFINE_integer("image_crop_y",192,"""Number of pixels to be cropped from the image in Y""" );
tf.app.flags.DEFINE_integer("image_zoom",320,"""Number of pixels on the shorter edge of the resized image""" );
tf.app.flags.DEFINE_float( "lrate_init" ,0.001,"""Initial learning rate""" );
tf.app.flags.DEFINE_string("lrate_decay_type","expcos","""Learning rate decay method""" );

tf.app.flags.DEFINE_integer( "lrate_decay_per" ,8000,"""Learning rate decay length""" );
tf.app.flags.DEFINE_integer("lrate_ramp_up",3000,"""Learning rate ramp up period""" );
tf.app.flags.DEFINE_string("optimizer_type","adam","""Optimizer type""" );
#Debug options
tf.app.flags.DEFINE_integer( "write_summary" ,100,"""Write summary every N iteration""" );
tf.app.flags.DEFINE_integer( "save_frequency" ,5000,"""Save model every N iteration""" );
tf.app.flags.DEFINE_integer( "diag_image_summary" ,0,"""Include example images into the summary""" );



""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"""Running the actual main function"""
init_ops.build_os_env();

""""Loading variables from old initialization"""
if FLAGS.do_use_warminit:
    try:
        oldvar = kernel_transfer.warminit_oldvar_dict( FLAGS.loadfile_path, FLAGS.loadfile_name, False );
    except:
        pfilename = FLAGS.picklefile_path+"/"+FLAGS.loadfile_name+".pickle";
        print(pfilename)
        pfile = open( pfilename, "rb" );
        oldvar = pickle.load( pfile );
        pfile.close();
else:
    oldvar=None;

"""Read training file names"""
filelist_train,filelist_valid,num_class,class_freq = init_ops.build_file_lists( datapath,num_class,FLAGS.data_style, csvfile=csvpath );

"""Establish nodes"""
device, target = multinodes.get_device_and_target();
is_chief = (FLAGS.task_index == 0);

"""Assigns ops to the local worker by default."""
with tf.device(device):
    """Set up data preprocessing"""
    train_preproc,valid_preproc = init_ops.build_data_preproc();
    files_tr,labels_tr,_,iterator_tr = train_preproc;
    files_vl,labels_vl,_,iterator_vl = valid_preproc;

    """Build up graph"""
    graph_nodes = init_ops.build_graph_ops( device, is_chief, num_class, d_in=FLAGS.dim_in, oldvar=oldvar, class_freq=class_freq );
    cgraph, graph_saver, global_step, loader, assignment_list, train_writer,valid_writer,merged_summary = graph_nodes;

    """Retrieve key training ops"""
    if FLAGS.model_type in ["seg","segmentation"]: 
        train_op,loss,_,_,input_handle,is_training = cgraph.get_basic_nodes();
    elif FLAGS.model_type in ["hupa"]: 
        train_op,loss,_,input_handle,is_training,p_keep = cgraph.get_basic_nodes();        
    elif FLAGS.model_type in ["cls","classification"]: 
        train_op,loss,_,input_handle,is_training,p_keep = cgraph.get_basic_nodes();        
    else:
        sys.exit("ERROR: Unknown model type: " + FLAGS.model_type );

gnxt_op = iterator_tr.get_next();
tensor_names = [t.name for op in tf.get_default_graph().get_operations() for t in op.values()];
var_names = [var.name for var in tf.trainable_variables()];

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"""Set up training hooks"""
hooks=[tf.train.StopAtStepHook(last_step=FLAGS.last_step )];  # The StopAtStepHook handles stopping after running given steps.
"""Set up the GPU computation environment"""
gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.8, allow_growth=True, allocator_type="BFC");
config = tf.ConfigProto(gpu_options=gpu_options,allow_soft_placement=False);

#The MonitoredTrainingSession takes care of session initialization, 
#   restoring from a checkpoint, saving to a checkpoint, and closing.      
sess = tf.train.MonitoredTrainingSession(master=target, is_chief=is_chief, hooks=hooks,
                                       save_checkpoint_secs=600, checkpoint_dir=None,
                                       save_summaries_steps=500, config=config );   

"""Loading parameters from previous models (warminit)""" 
if is_chief:
    init_ops.init_loaded_parameters(sess,assignment_list, loader=loader);

"""Initializing datastream"""    
handle_tr,handle_vl = init_ops.init_data_handles(sess,iterator_tr,iterator_vl,files_tr,files_vl,
                                                 filelist_train,filelist_valid,labels_tr,labels_vl );

"""Run assynchronous training"""
print("Beginning the training...")
itr=0;
elapsed = 1.0;
while itr<FLAGS.last_step:
    itr+=1;            
    tstart = time.time();
    try:
        sess.run([train_op], feed_dict={ input_handle: handle_tr, is_training: True, p_keep: 0.5 } );
    except Exception as e:
        print("There was a crash in training op...\n")
        print(e)
        sess.run([train_op], feed_dict={ input_handle: handle_tr, is_training: True, p_keep:0.5 } );
    elapsed = 0.3*(time.time() - tstart)+0.7*elapsed;
    
    if is_chief:
        init_ops.periodic_maintenance(sess,itr,elapsed,cgraph,graph_saver,global_step,merged_summary,
                                      train_writer,valid_writer,handle_tr, handle_vl);
        
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

