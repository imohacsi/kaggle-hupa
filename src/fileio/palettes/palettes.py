#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 15 14:00:23 2018

@author: imohacsi
"""

from . import palette_tgssalt
from . import palette_camvid
import sys

def decode_cmap( image, style ):
    style = style.decode('ascii');
    if style in ['salt','tgs_salt', 'tgssalt']:
        labels_decoded = palette_tgssalt.decode_cmap( image );
    elif style=='camvid':
        labels_decoded = palette_camvid.decode_cmap( image );
    else:
        errmsg = 'ERROR: Unknown style in palettes: %s' % (style);
        sys.exit( errmsg )
    return labels_decoded;

def encode_cmap( class_idx, style ):
    if style in ['salt','tgs_salt', 'tgssalt']:
        color_image = palette_tgssalt.encode_cmap( class_idx );
    elif style=='camvid':
        color_image = palette_camvid.encode_cmap( class_idx );
    else:
        sys.exit( ('ERROR: Unknown style in palettes: %s' % (style)) )
    return color_image