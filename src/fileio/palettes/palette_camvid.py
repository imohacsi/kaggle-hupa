# -*- coding: utf-8 -*-
"""
Created on Thu Sep 13 12:38:38 2018

@author: mohacsii
"""

import numpy as np
import tensorflow as tf
import cv2

palette_camvid_in = np.array( [[0,0,0],[64,128,64],[192,0,128],[0,128,192],[0,128,64],
                            [128,0,0],[64,0,128],[64,0,192],[192,128,64],
                            [192,192,128],[64,64,128],[128,0,192],[192,0,64],
                            [128,128,64],[192,0,192],[128,64,64],[64,192,128],
                            [64,64,0],[128,64,128],[128,128,192],[0,0,192],
                            [192,128,128],[128,128,128],[64,128,192],[0,0,64],
                            [0,64,64],[192,64,128],[128,128,0],[192,128,192],
                            [64,0,64],[192,192,0],[64,192,0]], np.uint8 );

palette_camvid_out = palette_camvid_in;

#Decoding colormap
def decode_cmap(image):
#    fname = '../../../CamVid/masks/0001TP_006900_L.png'    
#   image = cv2.imread(fname )
    onehot_maps = [ np.logical_and.reduce( np.equal(image,color),axis=-1) for color in palette_camvid_in ]; 
    class_idx = np.argmax( np.array( onehot_maps ), axis=0 );
    return class_idx; 


#Encoding to colormap
def encode_cmap( class_idx ):
    
    palette = tf.constant( palette_camvid_out, dtype=tf.uint8 );
    oshape = tf.shape( class_idx );
    
    if len(class_idx.get_shape().as_list() )==4:
        class_idx = tf.argmax(class_idx, axis=-1);
#    print("\nEncoded indices: ",class_idx)
        
    class_idx = tf.cast(class_idx, tf.int32);
    class_idx = tf.reshape( class_idx, [-1])
    
    color_image = tf.gather(params=palette,indices=class_idx )
    color_image = tf.reshape(color_image, [oshape[0],oshape[1],oshape[2],3])
#    print("\nColor_image:",color_image)
#    color_image = tf.cast( tf.expand_dims(class_idx,axis=-1), tf.uint8 );
    return color_image;

