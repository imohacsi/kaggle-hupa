# -*- coding: utf-8 -*-
"""
Created on Thu Sep 13 12:38:38 2018

@author: mohacsii
"""

import numpy as np
import tensorflow as tf


palette_tgssalt_in = np.array( [[0],[255]], np.uint8 );
palette_tgssalt_out= np.array( [[0,0,0],[255,255,255]], np.uint8 );

#Decoding colormap
def decode_cmap(image):
    onehot_maps = []
    oshape = tf.shape( image );
    image = tf.cast( image/256, tf.uint8 );
    for colour in palette_tgssalt_in:
      class_map = tf.cast( tf.reduce_all(tf.equal(image, colour), axis=-1), tf.uint8);
      onehot_maps.append(class_map)
      
    onehot_maps= tf.stack( onehot_maps, axis=-1 );
    class_idx = tf.argmax( onehot_maps, axis=-1 ); 
    class_idx = tf.reshape( class_idx,[oshape[0],oshape[1],1]);
    
    return class_idx;

#Encoding to colormap
def encode_cmap( class_idx ):
    palette = tf.constant( palette_tgssalt_out, dtype=tf.uint8 );
    oshape = tf.shape( class_idx );
    
    if len(class_idx.get_shape().as_list() )==4:
        class_idx = tf.argmax(class_idx, axis=-1);
#    print("\nEncoded indices: ",class_idx)
        
    class_idx = tf.cast(class_idx, tf.int32);
    class_idx = tf.reshape( class_idx, [-1])
    
    color_image =  tf.gather(params=palette,indices=class_idx )
    color_image = tf.reshape(color_image, [oshape[0],oshape[1],oshape[2],3])
#    print("\nColor_image:",color_image)

    return color_image;

