# -*- coding: utf-8 -*-
import tensorflow as tf
import numpy as np
import os,cv2

FLAGS = tf.app.flags.FLAGS


class imagestream_hupa():
    def __init__(self, data_shuffle, data_prefetch,
                 do_augment=False, do_loopit=True, do_whiten=False, do_prefetch=True, iter_type='initializable', is_test=False ):
        self.image_size = tf.constant( [FLAGS.image_crop_x,FLAGS.image_crop_y], dtype=tf.int32 );
        self.image_zoom = tf.constant( FLAGS.image_zoom, dtype=tf.int32 );
        self.num_threads= FLAGS.num_threads;
        self.data_shuffle = data_shuffle;
        self.data_prefetch= data_prefetch;
        self.is_test = is_test;
        self.do_augment = do_augment;
        self.do_loopit = do_loopit;
        self.do_whiten = do_whiten;
        self.do_prefetch = do_prefetch;
        self.iter_type = iter_type;
        
        self.create_location_map( FLAGS.image_crop_x, FLAGS.image_crop_y );
        #Creating the actual image streaming pipeline
        self.set_up_pipeline_hupa( 28 )

    def create_location_map( self, Nx, Ny ):
        x_vec = np.arange( Nx )*1.0/float(Nx);
        y_vec = np.arange( Ny )*1.0/float(Ny);
        
        xy_map = np.zeros( [Nx,Ny,2] );
        for xx in range(Ny):
            xy_map[xx,:,0] = x_vec;
        for yy in range(Nx):
            xy_map[:,yy,1] = y_vec;
            
        self.location_map = tf.Variable( xy_map, trainable=False, dtype=tf.float32 );        

               
    def set_up_pipeline_hupa( self, num_classes ):
    
        plholder_fnameR = tf.placeholder( tf.string, [None] );
        plholder_fnameG = tf.placeholder( tf.string, [None] );
        plholder_fnameB = tf.placeholder( tf.string, [None] );    
        plholder_fnameY = tf.placeholder( tf.string, [None] );    
        self.plholder_labels = tf.placeholder( tf.float32, [None,num_classes] );
        self.plholder_fnames = [plholder_fnameR,plholder_fnameG,plholder_fnameB,plholder_fnameY];
        
        
        self.data_set = tf.data.Dataset.from_tensor_slices( ( plholder_fnameR,plholder_fnameG,plholder_fnameB,plholder_fnameY, self.plholder_labels) );
        """Loop and repeat the filenames (not the images)"""
        if self.do_loopit:
            self.data_set = self.data_set.shuffle( buffer_size=self.data_shuffle );
            self.data_set = self.data_set.repeat( );       
        self.data_set = self.data_set.map( lambda f1,f2,f3,f4,lb: _tf_read_image_hupa(f1,f2,f3,f4,lb,self.image_size,self.image_zoom,self.do_augment), num_parallel_calls=FLAGS.num_threads );
        if not self.is_test:
            self.data_set = self.data_set.map( lambda im,lb: _tf_proc_image_hupa(im,lb,self.do_augment,self.do_whiten), num_parallel_calls=FLAGS.num_threads );
        else:
            self.data_set = self.data_set.map( lambda im,lb: _tf_soft_image_hupa(im,lb,self.do_augment,self.do_whiten), num_parallel_calls=FLAGS.num_threads );
        
        self.data_set = self.data_set.batch( FLAGS.batch_size );        
       
        if self.do_prefetch:
            self.data_set = self.data_set.prefetch( self.data_prefetch );
    
        if self.iter_type=='initializable':
            self.dataset_iterator = self.data_set.make_initializable_iterator();
        elif self.iter_type=='reinitializable':
            self.dataset_iterator = self.data_set.make_reinitializable_iterator();
        else:
            assert False,'ERROR: unknown iterator type for dataset.';        
    
    def get_keypoints( self ):
        return self.plholder_fnames,self.plholder_labels,self.data_set,self.dataset_iterator;
    
    

    
def read_if_exists( filename ):
#    if os.path.exists( filename.decode("utf-8")[:-3]+"png" ):
    img = cv2.imread( filename.decode("utf-8")[:-3]+"png", 0 );
    if img is None:
        img = cv2.imread( filename.decode("utf-8")[:-3]+"jpg", 0 );
        if img is None:
            img = np.zeros( [512,512], dtype=np.uint8 );        
        else:
            img = cv2.resize( img, (512,512) );
    return img;
            
            
def opencv_read( nR, nG, nB, nY ):
    
    #If it's part of the png dataset
    if os.path.exists( nG.decode("utf-8")[:-3]+"png" ):
        img_g = read_if_exists( nG )
        img_r = read_if_exists( nR )
        img_b = read_if_exists( nB )
        img_y = read_if_exists( nY )
        img_rgby = np.uint8( np.stack([img_r, img_g, img_b, img_y ], axis=-1 ) );
    #Else there is an RGB composite
    else:
        nRGB = nG.decode("utf-8")[:-9]+"red_green_blue.jpg";
        img_rgb = cv2.imread( nRGB, 1 );
        img_rgb = cv2.resize( img_rgb, (512,512) );
        img_y = np.reshape( read_if_exists( nY ), [512,512,1] )
        img_rgby = np.concatenate( (img_rgb,img_y), axis=-1 );
    return img_rgby;

    
def _tf_read_image_hupa( f1,f2,f3,f4, label, num_pixels, num_zoom, do_aug ):
    
    if FLAGS.image_pyread: 
        print("Using python functions for image input")
        image_decoded = tf.py_func( opencv_read, [f1,f2,f3,f4], tf.uint8 );
        image_decoded = tf.reshape( image_decoded, [512,512,4] );
    else:
        print("Using tensorflow for image input")
        bytecodeR = tf.image.decode_jpeg( tf.read_file(f1), channels=1 );
        bytecodeG = tf.image.decode_jpeg( tf.read_file(f2), channels=1 );
        bytecodeB = tf.image.decode_jpeg( tf.read_file(f3), channels=1 );
        bytecodeY = tf.image.decode_jpeg( tf.read_file(f4), channels=1 );
        image_decoded = tf.concat([bytecodeR,bytecodeG,bytecodeB,bytecodeY],axis=-1)


    
    """Ensuring that no image dimension will be smaller than num_zoom """
    oshape = tf.shape( image_decoded );
    is_landscape = tf.greater_equal( oshape[0], oshape[1] ); #Means x>=y
    nshort = num_zoom;
    new_h, new_w = tf.cond( is_landscape,
            lambda: [ tf.to_int32( (oshape[0]*nshort) / oshape[1] ), nshort ],                
            lambda: [ nshort, tf.to_int32( (oshape[1]*nshort) / oshape[0] ) ] );
                           
    """Finally resize the image to the final scale"""
    img = tf.image.resize_images( image_decoded, [new_h,new_w]);    
    """Randomly crop a ROI from the image"""
    if do_aug:
        img = tf.to_float( tf.random_crop( img, [num_pixels[0],num_pixels[1],4] ) );   
    else:
        img = tf.image.resize_image_with_crop_or_pad(img,num_pixels[0],num_pixels[1] );
        
    return img,label


def _tf_proc_image_hupa( img, label, do_aug, do_whiten, loc_map=None ):
                    
    """Performing the normalization / whitening"""
    if do_whiten:
        img = tf.image.per_image_standardization( img );        
    else:
        img = tf.scalar_mul( (1.0/256.0), img );
        
    """Randomly rotate original image by 90 deg"""
    coin = tf.less(tf.random_uniform((), 0., 1.), 0.5)
    img = tf.cond( coin, lambda: tf.image.rot90(img), lambda: img );
            
    """Randomly crop a ROI from the image"""
    if do_aug:
        img = tf.image.random_flip_up_down( img );
        img = tf.image.random_flip_left_right( img );
        img = img + tf.random_normal( tf.shape(img), 0.0,0.02, tf.float32 );  
        img = img*tf.random_uniform( (), 0.75, 1.25 );
        img = img + tf.random_normal( (), 0.0, 0.5 );
    
    if loc_map is not None:
        img = tf.concat([img,loc_map], axis=-1 );
    
    return img,label


def _tf_soft_image_hupa( img, label, do_aug, do_whiten, loc_map=None ):
                    
    """Performing the normalization / whitening"""
    if do_whiten:
        img = tf.image.per_image_standardization( img );        
    else:
        img = tf.scalar_mul( (1.0/256.0), img );
        
    """Randomly rotate original image by 90 deg"""
    coin = tf.less(tf.random_uniform((), 0., 1.), 0.5)
    img = tf.cond( coin, lambda: tf.image.rot90(img), lambda: img );
            
    """Randomly crop a ROI from the image"""
    if do_aug:
        img = tf.image.random_flip_up_down( img );
        img = tf.image.random_flip_left_right( img );
        img = img + tf.random_normal( tf.shape(img), 0.0,0.01, tf.float32 );  
        img = img*tf.random_uniform( (), 0.85, 1.15 );
        img = img + tf.random_normal( (), 0.0, 0.3 );
    
    if loc_map is not None:
        img = tf.concat([img,loc_map], axis=-1 );
    
    return img,label





