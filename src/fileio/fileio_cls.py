# -*- coding: utf-8 -*-
"""
Created on Wed May  9 11:16:40 2018

@author: mohacsii
"""
import pandas as pd
import numpy as np
import os,sys

csvfile = "/run/media/imohacsi/DataSSD/Kaggle/HumanProteinAtlas/train.csv"
class get_train_filelist:
    def __init__( self, path, style, min_counts=None ):
        self.path = path;
        if self.path[-1] != '/':
            self.path = self.path+'/';
        self.style = style;
        self.min_counts = min_counts;
    
    def list_directories( self, class_directories ):
        list_of_entries = [];
        
        for dd in class_directories:
            if self.style == 'imagenet':
                image_directory = self.path + dd;
            elif self.style == 'glm-challenge':
                image_directory = self.path + dd;
            elif self.style == 'tiny-imagenet':
                image_directory = self.path + dd + '/images';
            else:
                sys.exit('ERROR: unsupported image folder format')
                
            #List all supported image files in folder    
            filelist= os.listdir( image_directory );
            image_file_list = [ fil for fil in filelist if ( fil.endswith(".jpg") or fil.endswith(".JPEG") or fil.endswith(".png") or fil.endswith(".PNG") ) ];
            
            #Generate full entry and append to list
            for file in image_file_list:
                fullname = image_directory+'/'+file;
                hashname = os.path.splitext( file )[0];
                list_of_entries.append( list([fullname, hashname, dd]) );
                
        return list_of_entries
                
    def load_filelist( self ):
        print("Listing image files from: ")
        print( self.path )
        
        #Each subdirectory corresponds to a class
        class_directories = [dd for dd in os.listdir(self.path) if os.path.isdir(os.path.join(self.path, dd))] 
        #List image files in each directory    
        list_of_entries = self.list_directories( class_directories );

        """Adding number of samples to dataset (auto-augment for rare ocasions)"""
        fileDFrame = pd.DataFrame( list_of_entries, columns=['filename','hash','class_id']);
        fileDFrame['freq'] = fileDFrame.groupby('class_id')['class_id'].transform('count');
        
        """Filter classes with too few examples"""
        if self.min_counts is not None:
            fileDFrame = fileDFrame[ fileDFrame['freq']>=self.min_counts ];
            
        """Generating labels from Class IDs"""
        class_uniq = fileDFrame['class_id'].unique();   
        label = [];
        for lid in fileDFrame['class_id']:
            lb_pos = np.where(class_uniq==lid);
            label.append( lb_pos );
        label = np.squeeze( np.array(label) )
        fileDFrame['label'] = label;

        num_class = len( fileDFrame['class_id'].unique() );    
        print( 'Found %d unique classes' % ( num_class ) );
        return fileDFrame, num_class;


    
def get_test_filelist( path = '../test' ):
    testlist = [];
             
    imagedir = path;
    filelist= os.listdir( imagedir );   
    imagelist = [ fil for fil in filelist if (fil.endswith(".png") or fil.endswith(".jpg")) ];

    for file in imagelist:
        fullname = imagedir+'/'+file;
        hashname = os.path.splitext( os.path.basename(file) )[0];
        testlist.append( list([fullname, hashname]) );
            
    filelist_test = pd.DataFrame( testlist, columns=['filename','hash']);
 
    return filelist_test;



