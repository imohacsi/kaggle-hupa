# -*- coding: utf-8 -*-
"""
Created on Wed May  9 11:16:40 2018

@author: mohacsii
"""
import pandas as pd
import numpy as np
import os,sys


def get_train_filelist( path = '../data/train', style='camvid', min_counts=25 ):
#    path = "./data/train"
    imagedir = path+"/images";
    masksdir = path+"/labels";
    
    filelist= os.listdir( imagedir );
    imagelist = [ fil for fil in filelist if (fil.endswith(".png") or fil.endswith(".jpg")) ];
    
    testlist = [];
    for file in imagelist:
        fullname = imagedir+'/'+file;
        hashname = os.path.splitext( file )[0];
        if style in ['camvid','CamVid']:
            maskname = masksdir + "/" + hashname+ "_L.png" ;
        elif style in ['salt','tgs_salt','tgssalt']:
            maskname = masksdir + "/" + hashname+ ".png" ;
        else:
            sys.exit("ERROR: unknown filelist style...")
        testlist.append( list([hashname, fullname, maskname ]) );
            
    """Adding number of samples to dataset (auto-augment for rare ocasions)"""
    fileDFrame = pd.DataFrame( testlist, columns=['hash','filename','maskname']);
    return fileDFrame;


def get_test_filelist( path = '../test' ):
    testlist = [];
             
    imagedir = path;
    filelist= os.listdir( imagedir );   
    imagelist = [ fil for fil in filelist if (fil.endswith(".png") or fil.endswith(".jpg")) ];

    for file in imagelist:
        fullname = imagedir+'/'+file;
        hashname = os.path.splitext( os.path.basename(file) )[0];
        testlist.append( list([fullname, hashname]) );
            
    filelist_test = pd.DataFrame( testlist, columns=['filename','hash']);
 
    return filelist_test;



