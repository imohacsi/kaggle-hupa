# -*- coding: utf-8 -*-
"""
Created on Wed May  9 11:16:40 2018

@author: mohacsii
"""
import pandas as pd
import numpy as np
import os,sys


def encode_multilabel( tags, num_class ): 
    lab = np.zeros([num_class]);
    try:
        if len(tags)>0:
            tags = tags.split()    
            for entr in tags:
                lab[ int(entr) ] = 1;
    except:
        pass;
    return lab;

class filelist_hupa:
    def __init__( self, csvname, path, num_class=28, min_counts=None ):
        self.num_class = num_class;
        self.df = pd.read_csv( csvname );
        self.df['blue']   = self.df['Id'].apply(lambda x: path+"/"+x[0]+"/"+x+"_blue.png" );
        self.df['green']  = self.df['Id'].apply(lambda x: path+"/"+x[0]+"/"+x+"_green.png" );
        self.df['yellow'] = self.df['Id'].apply(lambda x: path+"/"+x[0]+"/"+x+"_yellow.png" );
        self.df['red']    = self.df['Id'].apply(lambda x: path+"/"+x[0]+"/"+x+"_red.png" );
        
        mlabel = self.df['Target'].apply(lambda x: encode_multilabel(x,num_class) );
        mlabel = np.array( list(mlabel) )
        self.df = pd.concat( [self.df, pd.DataFrame(mlabel)], axis=1)
        self.df = self.df.drop( ['Target'],axis=1 );
        
    def load_filelist( self, rseed=1234 ):       
        return self.df.sample(frac=1.0, random_state=rseed ), self.num_class

    @staticmethod
    def get_cls_stat( df_ext ):
        cls_stat =  df_ext.iloc[:,5:33].sum()
        freq = cls_stat / len( df_ext );
        return freq;

def class_rebalance( df, thold, n_iter=3 ):        
    for itr in range( n_iter ):
        cls_stat =  df.iloc[:,5:33].sum()
        n_tot = len( df );
        n_min = thold * n_tot;
        
        for idx,val in cls_stat.iteritems():
            if val < n_min:
                sel = df[ df[idx]==1 ];                
                for jj in range( int(n_min/val) ):
                    df = df.append( sel, ignore_index=True );      
    return df;

def get_test_filelist( path = '../test' ):
    testlist = [];
    imagelist= [];
    
    imagedir = path;
    subdirlist= os.listdir( imagedir );   
    
    for dd in subdirlist:
        dirname = imagedir+"/"+dd;
        if os.path.isdir( dirname):
            filelist= os.listdir( dirname );   
            imgindir = [ fil.split('_')[0] for fil in filelist if (fil.endswith(".png") or fil.endswith(".jpg")) ];
        imagelist = imagelist + imgindir;
    

    for file in imagelist:
        hashname = os.path.splitext( os.path.basename(file) )[0];
        testlist.append( hashname );
        
    df = pd.DataFrame( testlist, columns=['Id']);     
    df = df.drop_duplicates(['Id'])
    
    df['blue']   = df['Id'].apply(lambda x: path+"/"+x[0]+"/"+x+"_blue.png" );
    df['green']  = df['Id'].apply(lambda x: path+"/"+x[0]+"/"+x+"_green.png" );
    df['yellow'] = df['Id'].apply(lambda x: path+"/"+x[0]+"/"+x+"_yellow.png" );
    df['red']    = df['Id'].apply(lambda x: path+"/"+x[0]+"/"+x+"_red.png" );
        
 
    return df;



