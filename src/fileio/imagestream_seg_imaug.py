# -*- coding: utf-8 -*-
import tensorflow as tf
import numpy as np
from imgaug import augmenters as iaa 
FLAGS = tf.app.flags.FLAGS

from .palettes import palettes

seq_inplace = iaa.Sequential([
        iaa.AdditiveGaussianNoise(loc=0,scale=(0.0,6.0), per_channel=0.5),
        iaa.SomeOf( (0,2),
            iaa.OneOf([
                iaa.GaussianBlur((0.0,1.5)),
                iaa.AverageBlur(k=(1,3)),
                iaa.MedianBlur(k=(1,3)) ]),
            iaa.OneOf([
                iaa.Sharpen(alpha=(0, 0.7), lightness=(0.75, 1.25)),
                iaa.Emboss(alpha=(0, 0.7), strength=(0, 1.0)) ]),                   
            iaa.Sometimes( 0.3, iaa.Dropout( (0.01,0.05), per_channel=0.5 ) ) )                  
        ], random_order=True);  

def wrap_inplace( x_in ):
    return seq_inplace.augment_image( x_in );

seq_morpho = iaa.Sequential([
#        iaa.CoarseDropout((0.0, 0.05), size_percent=(0.05, 0.25))
#        iaa.Sometimes(iaa.Crop(percent=(0, 0.3))),
        iaa.Affine(
            scale={"x": (0.8, 1.2), "y": (0.8, 1.2)},
            translate_percent={"x": (-0.1, 0.1), "y": (-0.1, 0.1)},
            rotate=(-15, 15),
            shear=(-8, 8),
            order=0,
            cval=(0) )
            #mode=iaa.ALL 
#        iaa.Sometimes(0.5,iaa.PiecewiseAffine(scale=(0.01, 0.1)))
        ], random_order=True);    
           
def wrap_morpho( x_in, y_in ):
    seq_morpho_det = seq_morpho.to_deterministic();
    return seq_morpho_det.augment_image( x_in ),seq_morpho_det.augment_image( y_in );


class imagestream_seg():
    def __init__(self, data_shuffle, data_prefetch, pproc_form, is_test=False,
                 do_loopit=True, do_prefetch=True, iter_type='initializable' ):
        self.image_size = tf.constant( [FLAGS.image_crop_y,FLAGS.image_crop_x], dtype=tf.int32 );
        self.image_zoom = tf.constant( FLAGS.image_zoom, dtype=tf.int32 );
        self.batch_size = FLAGS.batch_size;
        self.num_threads= FLAGS.num_threads;
        self.data_shuffle = data_shuffle;
        self.data_prefetch= data_prefetch;
        self.is_test = is_test;
        self.do_loopit = do_loopit;
        self.do_prefetch = do_prefetch;
        self.iter_type = iter_type;
        self.pproc_form = pproc_form; 
        self.style='camvid'

        """Build the preprocessing pipeline"""
        self.set_up_pipeline();

                
    def set_up_pipeline( self ):
    
        self.plholder_fnames = tf.placeholder( tf.string, [None] );
        self.plholder_mnames = tf.placeholder( tf.string, [None] );
#        self.plholder_depth = tf.placeholder( tf.float32, [None] );

        self.data_set = tf.data.Dataset.from_tensor_slices( ( self.plholder_fnames, self.plholder_mnames ) );
        """Loop and repeat the filenames (not the images)"""
        if self.do_loopit:
            self.data_set = self.data_set.shuffle( buffer_size=self.data_shuffle );
            self.data_set = self.data_set.repeat( );       
        self.data_set = self.data_set.map( lambda fn,mn: _tf_read_image_all(fn,mn,self.image_size,self.image_zoom,self.pproc_form,self.style,self.is_test), num_parallel_calls=self.num_threads );
        self.data_set = self.data_set.batch( self.batch_size );        
       
        if self.do_prefetch:
            self.data_set = self.data_set.prefetch( self.data_prefetch );
    
        if self.iter_type=='initializable':
            self.dataset_iterator = self.data_set.make_initializable_iterator();
        elif self.iter_type=='reinitializable':
            self.dataset_iterator = self.data_set.make_reinitializable_iterator();
        else:
            assert False,'ERROR: unknown iterator type for dataset.';        
    
    def get_keypoints( self ):
        return self.plholder_fnames,self.plholder_mnames,self.data_set,self.dataset_iterator;


def _tf_read_image_all( filename, maskname, num_pixels, num_zoom, pproc, style, is_test=False ):
    """Load image"""
    image_bytecode = tf.read_file(filename);
    image_decoded = tf.image.decode_jpeg(image_bytecode, channels=3 );

    """Load mask or dummy mask"""
    if is_test:
        mask_decoded= tf.zeros( [tf.shape(image_decoded)[0],tf.shape(image_decoded)[1],1] );    
    else:
        mask_bytecode  = tf.read_file(maskname);
        mask_decoded  = tf.image.decode_png(mask_bytecode, dtype=tf.uint8, channels=1 );   
        
    """Ensuring that no image dimension will be smaller than num_zoom """
    oshape = tf.shape( image_decoded );
    is_landscape = tf.greater_equal( oshape[0], oshape[1] ); #Means x>=y
    nshort = num_zoom;
    new_h, new_w = tf.cond( is_landscape,
            lambda: [ tf.to_int32( (oshape[0]*nshort) / oshape[1] ), nshort ],                
            lambda: [ nshort, tf.to_int32( (oshape[1]*nshort) / oshape[0] ) ] );
                           
    """Finally resize the image to the final scale"""
    img  = tf.image.resize_images( image_decoded, [new_h,new_w]);          
    mask = tf.image.resize_images( mask_decoded,  [new_h,new_w], method=tf.image.ResizeMethod.NEAREST_NEIGHBOR );   
    
#    """Decode color coded mask"""
#    omshape = tf.shape( mask );
#    mask = tf.py_func( palettes.decode_cmap, [mask,style], tf.int64 );
#    mask = tf.reshape( mask, [omshape[0],omshape[1],1] );
        
#    img  = tf.image.resize_images(img,  [num_pixels[0],num_pixels[1]] );
#    mask = tf.image.resize_images(mask, [num_pixels[0],num_pixels[1]], method=tf.image.ResizeMethod.NEAREST_NEIGHBOR  );    
    img  = tf.image.resize_image_with_crop_or_pad(img,num_pixels[0],num_pixels[1] );
    mask = tf.image.resize_image_with_crop_or_pad(mask, num_pixels[0],num_pixels[1] );
    img  = tf.cast( img, tf.float32 );
    mask = tf.cast( (mask), tf.int64 );        

    """Save current shape"""    
    oishape = tf.shape( img );
    omshape = tf.shape( mask );
    
    #Doing the augmentation    
    if pproc.do_py_inplace:
        img         = tf.py_func( wrap_inplace, [img], tf.float32 );
    if pproc.do_py_ooplace:
        img, mask   = tf.py_func( wrap_morpho, [ img,mask ],  [tf.float32,tf.int64] );
    
    #Restoring shape
    if pproc.do_py_inplace or pproc.do_py_ooplace:
        img = tf.reshape( img, oishape );
        mask = tf.reshape( mask, omshape );

    """Applying morphological transformations"""
#    if pproc.do_rot90:
#        """Randomly rotate original image by 90 deg"""
#        coin = tf.less(tf.random_uniform((), 0., 1.), 0.5)
#        img,mask = tf.cond( coin, lambda: (tf.image.rot90(img),tf.image.rot90(mask)), lambda: (img,mask) );
    if pproc.do_udflip:
        """Randomly flip image up-down"""
        coin = tf.less(tf.random_uniform((), 0., 1.), 0.5)
        img,mask = tf.cond( coin, lambda: (tf.image.flip_up_down(img),tf.image.flip_up_down(mask)), lambda: (img,mask) );        
    if pproc.do_lrflip:
        """Randomly flip image left-right"""
        coin = tf.less(tf.random_uniform((), 0., 1.), 0.5)
        img,mask = tf.cond( coin, lambda: (tf.image.flip_left_right(img),tf.image.flip_left_right(mask)), lambda: (img,mask) );
    if pproc.do_negate:
        """Randomly take the negative image"""
        coin = tf.less(tf.random_uniform((), 0., 1.), 0.25)
        img,mask = tf.cond( coin, lambda: (tf.scalar_mul(-1.0,img),mask), lambda: (img,mask) );

    """Applying normalization and inplace transforms"""
    if pproc.do_whiten:
        img = tf.image.per_image_standardization( img ); 
    else:
        img = tf.scalar_mul( (1.0/256.0), img );
        
    if pproc.do_rndint:
        offset = tf.random_uniform((), 0., 1.)-0.5;
        scale = tf.random_uniform((), 0.5, 2.);
        img = tf.scalar_mul( scale, img );
        img = tf.add( img, offset );
    mask = tf.cast( (mask), tf.float32 );        
     
    return img,mask
