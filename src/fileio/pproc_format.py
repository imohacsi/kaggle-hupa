# -*- coding: utf-8 -*-
"""
Created on Fri Sep  7 11:56:57 2018

@author: mohacsii
"""

class pproc_format():
#    do_rot90=False;
#    do_lrflip=False;
#    do_udflip=False;
#    do_negate =True;
#    do_whiten = True;
#    do_gnoise = True;
#    do_rndint = True;
#    do_py_inplace = True;
#    do_py_ooplace = False;
    
    def __init__(self, do_rot90=False,do_lrflip=False,do_udflip=False,do_negate=True,do_whiten=True,
                 do_gnoise=True,do_rndint=True,do_py_inplace=True,do_py_ooplace=False ):
        self.do_rot90=do_rot90;
        self.do_lrflip=do_lrflip;
        self.do_udflip=do_udflip;
        self.do_negate =do_negate;
        self.do_whiten = do_whiten;
        self.do_gnoise = do_gnoise;
        self.do_rndint = do_rndint;
        self.do_py_inplace = do_py_inplace;
        self.do_py_ooplace = do_py_ooplace;