# -*- coding: utf-8 -*-
import tensorflow as tf
import numpy as np

FLAGS = tf.app.flags.FLAGS




class imagestream_cls():
    def __init__(self, data_shuffle, data_prefetch, is_test=False,
                 do_augment=False, do_loopit=True, do_whiten=False, do_prefetch=True, iter_type='initializable' ):
        self.image_size = tf.constant( [FLAGS.image_crop_x,FLAGS.image_crop_y], dtype=tf.int32 );
        self.image_zoom = tf.constant( FLAGS.image_zoom, dtype=tf.int32 );
        self.num_threads= FLAGS.num_threads;
        self.data_shuffle = data_shuffle;
        self.data_prefetch= data_prefetch;
        self.is_test = is_test;
        self.do_augment = do_augment;
        self.do_loopit = do_loopit;
        self.do_whiten = do_whiten;
        self.do_prefetch = do_prefetch;
        self.iter_type = iter_type;
        
#        self.create_location_map( FLAGS.image_crop_x, FLAGS.image_crop_y );
        
        self.set_up_pipeline_onehot();

    
    def create_location_map( self, Nx, Ny ):
        x_vec = np.arange( Nx )*1.0/float(Nx);
        y_vec = np.arange( Ny )*1.0/float(Ny);
        
        xy_map = np.zeros( [Nx,Ny,2] );
        for xx in range(Ny):
            xy_map[xx,:,0] = x_vec;
        for yy in range(Nx):
            xy_map[:,yy,1] = y_vec;
            
        self.location_map = tf.Variable( xy_map, trainable=False, dtype=tf.float32 );        

    def set_up_pipeline_onehot( self ):
    
        self.plholder_fnames = tf.placeholder( tf.string, [None] );
        self.plholder_labels = tf.placeholder( tf.int32, [None] );

        self.data_set = tf.data.Dataset.from_tensor_slices( ( self.plholder_fnames, self.plholder_labels) );
        """Loop and repeat the filenames (not the images)"""
        if self.do_loopit:
            self.data_set = self.data_set.shuffle( buffer_size=self.data_shuffle );
            self.data_set = self.data_set.repeat( );       
        self.data_set = self.data_set.map( lambda fn,lb: _tf_read_image_one(fn,lb,self.image_size,self.image_zoom,self.do_augment,self.do_whiten), num_parallel_calls=FLAGS.num_threads );
        self.data_set = self.data_set.batch( FLAGS.batch_size );        
       
        if self.do_prefetch:
            self.data_set = self.data_set.prefetch( self.data_prefetch );
    
        if self.iter_type=='initializable':
            self.dataset_iterator = self.data_set.make_initializable_iterator();
        elif self.iter_type=='reinitializable':
            self.dataset_iterator = self.data_set.make_reinitializable_iterator();
        else:
            assert False,'ERROR: unknown iterator type for dataset.';        
    

    def get_keypoints( self ):
        return self.plholder_fnames,self.plholder_labels,self.data_set,self.dataset_iterator;
    


def _tf_read_image_one( filename, label, num_pixels, num_zoom, do_aug, do_whiten, loc_map=None ):
    image_bytecode = tf.read_file(filename);
    image_decoded  = tf.image.decode_jpeg(image_bytecode, channels=3 );
#    image_decoded.set_shape([None,None,3]);
    
    """Ensuring that no image dimension will be smaller than num_zoom """
    oshape = tf.shape( image_decoded );
    is_landscape = tf.greater_equal( oshape[0], oshape[1] ); #Means x>=y
    nshort = num_zoom;
    new_h, new_w = tf.cond( is_landscape,
            lambda: [ tf.to_int32( (oshape[0]*nshort) / oshape[1] ), nshort ],                
            lambda: [ nshort, tf.to_int32( (oshape[1]*nshort) / oshape[0] ) ] );
                           
    """Finally resize the image to the final scale"""
    img = tf.image.resize_images( image_decoded, [new_h,new_w]);    
    
    """Randomly rotate original image by 90 deg"""
    coin = tf.less(tf.random_uniform((), 0., 1.), 0.5)
    img = tf.cond( coin, lambda: tf.image.rot90(img), lambda: img );
        
    """Randomly crop a ROI from the image"""
    if do_aug:
        img = tf.to_float( tf.random_crop( img, [num_pixels[0],num_pixels[1],3] ) );   
        img = tf.image.random_flip_up_down( img );
        img = tf.image.random_flip_left_right( img );
        img = img + tf.random_normal( tf.shape(img), 0.0,3.0, tf.float32 );  
    else:
        img = tf.image.resize_image_with_crop_or_pad(img,num_pixels[0],num_pixels[1] );
    
    if do_whiten:
        img = tf.image.per_image_standardization( img );        
    else:
        img = tf.scalar_mul( (1.0/256.0), img );
    
    if loc_map is not None:
        img = tf.concat([img,loc_map], axis=-1 );
    
    return img,label












