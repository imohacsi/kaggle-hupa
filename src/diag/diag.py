# -*- coding: utf-8 -*-

import tensorflow as tf
import numpy as np
import sys
sys.path.append("..")

from fileio.palettes import palettes

def print_num_trainpar( opath=None ):
    varlist = tf.trainable_variables();
    num_vars=0;
    
    for var in varlist:
        num_vars+= np.prod( np.array( var.get_shape().as_list() ) );
    print("Total number of trained variables in this session is: %0.3f Mpar" % (float(num_vars)/1000000) );
            
    sizelist = [ [var.name, np.prod( np.array( var.get_shape().as_list() ) )] for var in varlist ];
    logfile = open( opath+'/trainable_parameters.txt', 'w');
    for itm in sizelist:
        logfile.write( "%s\t%s\n" % (itm[1], itm[0] ) );
    logfile.close();

    return num_vars

"""Add a simple scalar to summary if it exists and matches format"""
def add_scalar( name, tensor ):
    try:
        tf.summary.scalar( name, tensor );
    except:
        print( "WARNING: Failed to add tensor to summary: ", name );
        pass;
    
def add_histogram( name, tensor ):
    try:
        tf.summary.histogram( name, tensor );
    except:
        print( "WARNING: Failed to add tensor to summary: ", name );
        pass;
        
        
def prepare_summary_writer_cls( cgraph, summaries_path, do_images=False ):
    with tf.name_scope("summary"):
        
        add_scalar( 'accuracy', cgraph.res_accur );
        add_scalar( 'loss_foc', cgraph.res_loss_foc );
        add_scalar( 'loss_f1', cgraph.res_loss_f1 );                      
        add_scalar( 'loss_xe',  cgraph.res_loss_xe );
        add_scalar( 'loss_sl',  cgraph.res_loss_sl );
        add_scalar( 'loss_l2',  cgraph.res_loss_l2 );
        add_scalar( 'loss',     cgraph.res_loss );
        add_scalar( 'learning_rate', cgraph.res_lrate );

        add_scalar( 'running_accuracy', cgraph.res_run_accur );
        add_histogram( 'running_classwise_accuracy', cgraph.res_run_accur_cw );


        """Add example input images to the summary"""
        if do_images:
            x_size= cgraph.input_images.get_shape().as_list();
            if x_size[-1]<=3:
                tf.summary.image( 'input', cgraph.input_images, max_outputs=2 );
            else:
                img_pnt = cgraph.input_images[:,:,:,0:3]; 
                tf.summary.image( 'input', img_pnt, max_outputs=2 );                

    merged_summary = tf.summary.merge_all()
    train_writer = tf.summary.FileWriter( summaries_path + '/train')
    valid_writer = tf.summary.FileWriter( summaries_path + '/test')
    return train_writer,valid_writer,merged_summary

def prepare_summary_writer_seg( cgraph, summaries_path, style ):
    with tf.name_scope("summary"):
        tf.summary.scalar( 'batch_miou', cgraph.batch_miou );
        tf.summary.scalar( 'loss_xe',  cgraph.res_loss_xe );
        tf.summary.scalar( 'loss_sl',  cgraph.res_loss_sl );
        tf.summary.scalar( 'loss_l2',  cgraph.res_loss_l2 );
        tf.summary.scalar( 'loss',     cgraph.res_loss );
        tf.summary.scalar( 'learning_rate', cgraph.res_lrate );
        try:
            in_list = tf.unstack(cgraph.input_images, axis=3)
            in_imag = tf.expand_dims( in_list[0], axis=-1 )
            tf.summary.image( 'input', in_imag, max_outputs=2 );
        except:
            pass;
#        try:
        msk_imag = palettes.encode_cmap(cgraph.input_labels, style);
        tf.summary.image( 'mask', msk_imag, max_outputs=2 );
#        except:
#            pass;
#        try:
        pred_imag = palettes.encode_cmap(cgraph.res_pred, style);
        tf.summary.image( 'pred', pred_imag, max_outputs=2 );            
#        except:
#            pass;
    merged_summary = tf.summary.merge_all()
    train_writer = tf.summary.FileWriter( summaries_path + '/train')
    valid_writer = tf.summary.FileWriter( summaries_path + '/test')
    return train_writer,valid_writer,merged_summary
            
