import numpy as np
import tensorflow as tf
from . import model_template_seg
from ..layers import pooling
from ..layers import layers

class psp_net( model_template_seg.model_template_seg ):

    def _residual_bottleneck( self, x_in, filters, filters_out=None, strides=[1,1], dilation=1, act_fn=tf.nn.elu ):
        x_size= x_in.get_shape().as_list();
		
		#Make sure we have the right output dimension
        if filters_out is None:
            filters_out = x_size[-1];

        #If increasing depth before pooling
        if filters_out != x_size[-1]:
            x_scut = self._conv( x_in, filters=filters_out, kernel_size=[1,1], strides=strides, padding="SAME" );
        else:
            x_scut = x_in;
		
        x = self._conv( x_in, filters, [1,1], strides=strides, act_fn=None );
        x = tf.layers.batch_normalization( x, axis=-1, training=self.is_training );
        x = act_fn( x );
        x = self._conv( x, filters, [3,3], dilation=dilation, act_fn=None );
        x = tf.layers.batch_normalization( x, axis=-1, training=self.is_training );
        x = act_fn( x );
        x = self._conv( x, x_size, [3,3], act_fn=None);
        x = tf.layers.batch_normalization( x, axis=-1, training=self.is_training );

        return x+x_scut;        

		
    def pyramid_pool(self, x_in, pool_size, depth):
        x_shape = tf.shape(x_in);
	
        x_pool = tf.nn.pool( x_in, pool_size, pool_size );
        x_conv = self._conv( x_pool, filters=depth, kernel_size=[3,3], padding="SAME" );
        x_out  = tf.image.resize_bilinear( x_conv, [x_shape[1],x_shape[2]] );
        return x_out;
		
		
		
    def _ppm_module( self, x_in, filters=None, scales=[1,2,3,6] ):
        x_size= x_in.get_shape().as_list();
        if filters is None:
            filters = int(x_size[-1]/len(scales));

        #Create the pooling pyramid
        x_pooled = [ ];
        for scale in scales:
            x_pooled.append( self.pyramid_pool( x_in, [scale,scale], filters ) );
		
        #Merge the pooling pyramid
        x_merg = tf.concat( [x_in]+x_pooled, axis=-1 );		

        #Assign output depth
        x_out =self._conv( x_merg, x_size[-1], [1,1], padding="SAME");
        return x_out; 

    def _build_model( self, x_in ):       
#        x_in = tf.image.resize_images( x_in, [101,101]);
        
        with tf.variable_scope('block1'):
			#First block is always standard convolution
            c1 = layers.conv2d( x_in, filters=int(self.model_width*64), kernel_size=[3,3], padding="SAME" );
            c1 = layers.conv2d( c1,    filters=int(self.model_width*64), kernel_size=[3,3], padding="SAME" );
            c1_o = tf.nn.pool( c1, [3,3], strides=[2,2], pooling_type='MAX', padding="SAME");
			
        with tf.variable_scope('block2'):
            c2_i = self._residual_bottleneck( c1_o, filters=int(self.model_width*64/4), filters_out=int(self.model_width*256)  );
            c2   = self._residual_bottleneck( c2_i,   filters=int(self.model_width*256/4) );
            c2   = self._residual_bottleneck( c2,   filters=int(self.model_width*256/4) );
            c2_o = c2;
			
        with tf.variable_scope('block3'):
            c3_i = self._residual_bottleneck( c2_o, filters=int(self.model_width*512/4), filters_out=int(self.model_width*512/4), strides=[2,2] );
            c3   = self._residual_bottleneck( c3_i,   filters=int(self.model_width*512/4) );
            c3   = self._residual_bottleneck( c3,   filters=int(self.model_width*512/4) );
            c3   = self._residual_bottleneck( c3,   filters=int(self.model_width*512/4) );
            c3_o = c3;
			
        with tf.variable_scope('block4'):
            c4_i = self._residual_bottleneck( c3_o, filters=int(self.model_width*1024/4), filters_out = int(self.model_width*1024/4), dilation=2 );
            c4   = self._residual_bottleneck( c4_i,   filters=int(self.model_width*1024/4), dilation=2 );
            c4   = self._residual_bottleneck( c4,   filters=int(self.model_width*1024/4), dilation=2 );
            c4   = self._residual_bottleneck( c4,   filters=int(self.model_width*1024/4), dilation=2 );
            c4   = self._residual_bottleneck( c4,   filters=int(self.model_width*1024/4), dilation=2 );
            c4   = self._residual_bottleneck( c4,   filters=int(self.model_width*1024/4), dilation=2 );
            c4_o = c4;
			
        with tf.variable_scope('ppm1'):
            ppm1 = self._ppm_module( c4_o );
            ppm1 = self._ppm_module( ppm1 );
            ppm1 = self._ppm_module( ppm1 );

        with tf.variable_scope('deconv2'):
            dc2 = tf.image.resize_images( ppm1, size=[ tf.shape(c2)[1], tf.shape(c2)[2] ] );
            dc2 = self._conv( dc2, filters=int(self.model_width*512), kernel_size=[3,3], padding="SAME" );
            dc2 = tf.concat( [c2,dc2], axis=-1 );
            dc2 = self._conv( dc2, filters=int(self.model_width*256), kernel_size=[3,3], padding="SAME" );
            dc2 = self._conv( dc2, filters=int(self.model_width*256), kernel_size=[3,3], padding="SAME" );
        
        with tf.variable_scope('deconv1'):
            dc1 = tf.image.resize_images( dc2, size=[ tf.shape(c1)[1], tf.shape(c1)[2] ] );
            dc1 = self._conv( dc1, filters=int(self.model_width*64), kernel_size=[3,3], padding="SAME" );
            dc1 = tf.concat( [c1,dc1], axis=-1 );
            dc1 = self._conv( dc1, filters=int(self.model_width*64), kernel_size=[3,3], padding="SAME" );
            dc1 = self._conv( dc1, filters=int(self.model_width*64), kernel_size=[3,3], padding="SAME" );

        """This was the last deconvolved layer"""
        x_out = dc1;
		            
        """Agregate sparial features for postprocessing"""
        with tf.variable_scope("features",reuse=True):
            x_out = tf.identity( x_out, name='feature_map');
        print('Feature map dimensions: ', x_out.get_shape().as_list() );
        return x_out;

            
            
            
