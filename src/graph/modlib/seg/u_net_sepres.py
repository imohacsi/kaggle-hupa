import numpy as np
import tensorflow as tf
from . import model_template_seg
from ..layers import pooling
from ..layers import attention
from ..layers import layers

class u_net_sepres( model_template_seg.model_template_seg ):

    def _residual_block( self, x ):
        x_size = x.get_shape().as_list()[-1];
        b_neck = int(x_size/2);
        
        shortcut = x;
        x = tf.layers.batch_normalization( x, axis=-1, training=self.is_training, trainable=True );
        x = tf.nn.elu( x );        
        x = self._conv( x, filters=b_neck, kernel_size=[3,3], padding="SAME", act_fn=None, cstyle='sepconv' );
        x = tf.layers.batch_normalization( x, axis=-1, training=self.is_training, trainable=True );
        x = tf.nn.elu( x );
        x = self._conv( x, filters=x_size, kernel_size=[3,3], padding="SAME", act_fn=None, cstyle='sepconv' );
        return x+shortcut;          
      
    def _build_model( self, x_in ):       
#        x_in = tf.image.resize_images( x_in, [101,101]);    

        with tf.variable_scope('conv1'):
            c1 = self._conv( x_in, filters=int(self.model_width*64), kernel_size=[3,3], padding="SAME" );
            c1 = self._conv( c1,    filters=int(self.model_width*64), kernel_size=[3,3], padding="SAME" );
        
        with tf.variable_scope('conv2'):
            c2 = pooling.pool_dual( c1, pool_out=int(self.model_width*128) );
            c2   = tf.nn.dropout( c2, keep_prob=0.85 );
            c2 = self._conv( c2, filters=int(self.model_width*128), kernel_size=[3,3], padding="SAME" );
            c2 = self._residual_block( c2 );
            c2 = self._residual_block( c2 );
            c2 = tf.nn.elu( c2 )

        with tf.variable_scope('conv3'):
            c3 = pooling.pool_dual( c2, pool_out=int(self.model_width*256) );
            c3   = tf.nn.dropout( c3, keep_prob=0.7 );
            c3 = self._conv( c3, filters=int(self.model_width*256), kernel_size=[3,3], padding="SAME" );
            c3 = self._residual_block( c3 );
            c3 = self._residual_block( c3 );
            c3 = tf.nn.elu( c3 )

        with tf.variable_scope('conv4'):
            c4 = pooling.pool_dual( c3, pool_out=int(self.model_width*512) );
            c4 = tf.nn.dropout( c4, keep_prob=0.7 );
            c4 = self._conv( c4, filters=int(self.model_width*512), kernel_size=[3,3], padding="SAME" );
            c4 = self._residual_block( c4 );
            c4 = self._residual_block( c4 );
            c4 = tf.nn.elu( c4 )           
            
            
        with tf.variable_scope('bridge'):
            br   = pooling.pool_dual( c4, pool_out=int(self.model_width*1024) );
            br   = tf.nn.dropout( br, keep_prob=0.7 );
            br   = self._residual_block( br );
#            br   = attention.attention_recurrent_global( br );
            br   = self._residual_block( br );	
            br_o = tf.nn.elu( br );
            
        with tf.variable_scope('deconv4'):
            dc4_i = tf.image.resize_images( br_o, size=[tf.shape(c4)[1], tf.shape(c4)[2]] );
            dc4_i = self._conv( dc4_i, filters=int(self.model_width*512), kernel_size=[3,3], padding="SAME" );
            dc4   = tf.concat( [c4,dc4_i], axis=-1 );
            dc4 = tf.nn.dropout( dc4, keep_prob=0.7 );
            dc4 = self._conv( dc4, filters=int(self.model_width*512), kernel_size=[3,3], padding="SAME" );
            dc4 = self._residual_block( dc4 );
            dc4 = self._residual_block( dc4 );
            dc4_o = tf.nn.elu( dc4 );
            
        with tf.variable_scope('deconv3'):
            dc3_i = tf.image.resize_images( dc4_o, size=[tf.shape(c3)[1], tf.shape(c3)[2]] );
            dc3_i = self._conv( dc3_i, filters=int(self.model_width*256), kernel_size=[3,3], padding="SAME" );
            dc3   = tf.concat( [c3,dc3_i], axis=-1 );
            dc3   = tf.nn.dropout( dc3, keep_prob=0.7 );
            dc3   = self._conv( dc3, filters=int(self.model_width*256), kernel_size=[3,3], padding="SAME" );
            dc3   = self._residual_block( dc3 );
            dc3   = self._residual_block( dc3 );
            dc3_o = tf.nn.elu( dc3 );
#
        with tf.variable_scope('deconv2'):
            dc2_i = tf.image.resize_images( dc3_o, size=[tf.shape(c2)[1], tf.shape(c2)[2]] );
            dc2_i = self._conv( dc2_i, filters=int(self.model_width*128), kernel_size=[3,3], padding="SAME" );
            dc2   = tf.concat( [c2,dc2_i], axis=-1 );
            dc2   = tf.nn.dropout( dc2, keep_prob=0.7 );
            dc2   = self._conv( dc2, filters=int(self.model_width*128), kernel_size=[3,3], padding="SAME" );
            dc2   = self._residual_block( dc2 );
            dc2   = self._residual_block( dc2 );
            dc2_o = tf.nn.elu( dc2 );
            
        with tf.variable_scope('deconv1'):
            dc1_i = tf.image.resize_images( dc2_o, size=[tf.shape(c1)[1], tf.shape(c1)[2]] );
            dc1_i = self._conv( dc1_i, filters=int(self.model_width*64), kernel_size=[3,3], padding="SAME" );
            dc1   = tf.concat( [c1,dc1_i], axis=-1 );           
            dc1 = tf.nn.dropout( dc1, keep_prob=0.85  );
            dc1 = self._conv( dc1, filters=int(self.model_width*64), kernel_size=[3,3], padding="SAME" );
            dc1 = self._residual_block( dc1 );
            dc1 = self._residual_block( dc1 );	
            dc1_o = tf.nn.elu( dc1 );

        """This was the last deconvolved layer"""
        x = dc1_o;       
            
        """Agregate sparial features for postprocessing"""
        with tf.variable_scope("features",reuse=True):
            x = tf.identity( x, name='feature_map');
        print('Feature map dimensions: ', x.get_shape().as_list() );
        return x;

            
            
            