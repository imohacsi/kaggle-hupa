import numpy as np
import tensorflow as tf
from . import model_template_seg
from ..layers import pooling
from ..layers import attention
from ..layers import layers

class u_net( model_template_seg.model_template_seg ):
    def conv_block(self, x_in, filters, act_fn=tf.nn.elu ):
        c1 = self._conv( x_in, filters=int(filters), kernel_size=[3,3], padding="SAME", act_fn=None );
        c1 = tf.layers.batch_normalization( c1, axis=-1, training=self.is_training, trainable=True );
        c1 = act_fn( c1 );
        c2 = self._conv( c1,   filters=int(filters), kernel_size=[3,3], padding="SAME", act_fn=None );			
        c2 = tf.layers.batch_normalization( c2, axis=-1, training=self.is_training, trainable=True );
        c2 = act_fn( c2 );        
        return c2;
    
    def _build_model( self, x_in ):          
#        x_in = tf.image.resize_images( x_in, [101,101]);    

        with tf.variable_scope('conv1'):
            c1 = layers.conv2d( x_in, filters=int(self.model_width*64), kernel_size=[3,3], padding="SAME" );
            c1 = layers.conv2d( c1,    filters=int(self.model_width*64), kernel_size=[3,3], padding="SAME" );
        
        with tf.variable_scope('conv2'):
            c2 = pooling.pool_dual( c1, pool_out=int(self.model_width*128) );
            c2 = self.conv_block( c2, int(self.model_width*128) );

        with tf.variable_scope('conv3'):
            c3 = pooling.pool_dual( c2, pool_out=int(self.model_width*256) );
            c3 = self.conv_block( c3, int(self.model_width*256) );

        with tf.variable_scope('conv4'):
            c4 = pooling.pool_dual( c3, pool_out=int(self.model_width*512) );
            c4 = self.conv_block( c4, int(self.model_width*512) );
            
        with tf.variable_scope('bridge'):
            br   = pooling.pool_dual( c4, pool_out=int(self.model_width*1024) );
            br_o = self.conv_block( br, int(self.model_width*1024) );

        with tf.variable_scope('deconv4'):
            dc4_i = tf.image.resize_images( br_o, size=[tf.shape(c4)[1], tf.shape(c4)[2]] );
            dc4_i = self._conv( dc4_i, filters=int(self.model_width*512), kernel_size=[3,3], padding="SAME" );
            dc4   = tf.concat( [c4,dc4_i], axis=-1 );
            dc4   = tf.nn.dropout( dc4, keep_prob=0.7 );
            dc4_o = self.conv_block( dc4, int(self.model_width*512) );
        
        with tf.variable_scope('deconv3'):
            dc3_i = tf.image.resize_images( dc4_o, size=[tf.shape(c3)[1], tf.shape(c3)[2]] );
            dc3_i = self._conv( dc3_i, filters=int(self.model_width*256), kernel_size=[3,3], padding="SAME" );
            dc3   = tf.concat( [c3,dc3_i], axis=-1 );
            dc3   = tf.nn.dropout( dc3, keep_prob=0.7 );
            dc3_o = self.conv_block( dc3, int(self.model_width*256) );

        with tf.variable_scope('deconv2'):
            dc2_i = tf.image.resize_images( dc3_o, size=[tf.shape(c2)[1], tf.shape(c2)[2]] );
            dc2_i = self._conv( dc2_i, filters=int(self.model_width*128), kernel_size=[3,3], padding="SAME" );
            dc2   = tf.concat( [c2,dc2_i], axis=-1 );
            dc2   = tf.nn.dropout( dc2, keep_prob=0.7 );
            dc2_o = self.conv_block( dc2, int(self.model_width*128) );
	
        with tf.variable_scope('deconv1'):
            dc1_i = tf.image.resize_images( dc2_o, size=[tf.shape(c1)[1], tf.shape(c1)[2]] );
            dc1_i = self._conv( dc1_i, filters=int(self.model_width*64), kernel_size=[3,3], padding="SAME" );
            dc1   = tf.concat( [c1,dc1_i], axis=-1 );
            dc1   = tf.nn.dropout( dc1, keep_prob=0.7 );
            dc1_o = self.conv_block( dc1, int(self.model_width*64) );

                	
        """This was the last deconvolved layer"""
        x = dc1_o;       
		           
        """Agregate sparial features for postprocessing"""
        with tf.variable_scope("features",reuse=True):
            x = tf.identity( x, name='feature_map');
        print('Feature map dimensions: ', x.get_shape().as_list() );
        return x;

            
            
            
