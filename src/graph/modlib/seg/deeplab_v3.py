import numpy as np
import tensorflow as tf
from . import model_template_seg
from ..layers import pooling
from ..layers import layers

class deeplab_v3( model_template_seg.model_template_seg ):
       
    def _residual_bottleneck( self, x_in, filters, filters_out=None, strides=[1,1], dilation=1, act_fn=tf.nn.elu ):
        x_size= x_in.get_shape().as_list();
		
		#Make sure we have the right output dimension
        if filters_out is None:
            filters_out = x_size[-1];

        #If increasing depth before pooling
        if filters_out != x_size[-1]:
            x_scut = self._conv( x_in, filters=filters_out, kernel_size=[1,1], strides=strides, padding="SAME" );
        else:
            x_scut = x_in;
		
        x = self._conv( x_in, filters, [1,1], strides=strides, act_fn=None );
        x = tf.layers.batch_normalization( x, axis=-1, training=self.is_training, trainable=True );
        x = act_fn( x );
        x = self._conv( x, filters, [3,3], dilation=dilation, act_fn=None );
        x = tf.layers.batch_normalization( x, axis=-1, training=self.is_training, trainable=True );
        x = act_fn( x );
        x = self._conv( x, filters_out, [3,3], act_fn=None);
        x = tf.layers.batch_normalization( x, axis=-1, training=self.is_training, trainable=True );

        return x+x_scut;    
		
		
    def _asp_pool_block( self, x_in, filters=None, rate=[2,3,6] ):
        x_size= x_in.get_shape().as_list();
        if filters is None:
            filters = x_size[-1];

        #Create the spatial pooling pyramid
        sp_list = [ self._conv( x_in, x_size[-1], [1,1], padding="SAME") ];
        for scale in rate:
            sp_list.append( self._conv( x_in, x_size[-1], [3,3], padding="SAME", dilation=scale ) );
			
		#Calculate image level features
        img_lev = tf.reduce_mean( x_in, [1,2], keepdims=True )
        img_lev = self._conv( x_in, x_size[-1], [1,1], padding="SAME");
        img_lev = tf.image.resize_images( img_lev, size=[ tf.shape(x_in)[1], tf.shape(x_in)[2] ] );
		
        #Merge the spatial pooling pyramid
        x_merg = tf.concat( [img_lev]+sp_list, axis=-1 );
		
        #Assign output depth
        x_out =self._conv( x_merg, filters, [1,1], padding="SAME");
        return x_out; 

    def _build_model( self, x_in ):       
#        x_in = tf.image.resize_images( x_in, [101,101]);
        
        with tf.variable_scope('block0'):
			#First block is always standard convolution
            c0_i = layers.conv2d( x_in,   filters=int(self.model_width*64), kernel_size=[3,3], padding="SAME" );
            c0   = layers.conv2d( c0_i,   filters=int(self.model_width*64), kernel_size=[3,3], padding="SAME" );
            c0_o = tf.nn.pool( c0, [3,3], strides=[2,2], pooling_type='MAX', padding="SAME");
			
        with tf.variable_scope('block1'):
            c1_i = self._residual_bottleneck( c0_o, filters=int(self.model_width*128/4), filters_out=int(self.model_width*128)  );
            c1   = self._residual_bottleneck( c1_i,   filters=int(self.model_width*128/4) );
            c1   = self._residual_bottleneck( c1,   filters=int(self.model_width*128/4) );
            c1_o = c1;
			
        with tf.variable_scope('block2'):
            c2_i = self._residual_bottleneck( c1_o, filters=int(self.model_width*256/4), filters_out=int(self.model_width*256), strides=[2,2] );
            c2   = self._residual_bottleneck( c2_i, filters=int(self.model_width*256/4) );
            c2   = self._residual_bottleneck( c2,   filters=int(self.model_width*256/4) );
            c2   = self._residual_bottleneck( c2,   filters=int(self.model_width*256/4) );
            c2_o = c2;
			
        with tf.variable_scope('block3'):
            c3_i = self._residual_bottleneck( c2_o, filters=int(self.model_width*256/4), filters_out = int(self.model_width*256), dilation=2 );
            c3   = self._residual_bottleneck( c3_i, filters=int(self.model_width*256/4), dilation=2 );
            c3   = self._residual_bottleneck( c3,   filters=int(self.model_width*256/4), dilation=2 );
            c3   = self._residual_bottleneck( c3,   filters=int(self.model_width*256/4), dilation=2 );
            c3   = self._residual_bottleneck( c3,   filters=int(self.model_width*256/4), dilation=2 );
            c3   = self._residual_bottleneck( c3,   filters=int(self.model_width*256/4), dilation=2 );
            c3_o = c3;
			
        with tf.variable_scope('block4'):
            c4_i = self._residual_bottleneck( c3_o, filters=int(self.model_width*512/4), dilation=4 );
            c4   = self._residual_bottleneck( c4_i, filters=int(self.model_width*512/4), dilation=4 );
            c4   = self._residual_bottleneck( c4,   filters=int(self.model_width*512/4), dilation=4 );
            c4_o = c4;			
			
        with tf.variable_scope('assp1'):
            assp1_i = self._asp_pool_block( c4_o, int(self.model_width*512), rate=[3,6,9] );
            assp1_o = assp1_i;

        with tf.variable_scope('deconv1'):
            dc1_i1 = tf.image.resize_images( assp1_o, size=[ tf.shape(c1_o)[1], tf.shape(c1_o)[2] ] );
            dc1_i2 = self._conv( c1_o, filters=int(self.model_width*64), kernel_size=[1,1], padding="SAME" );
            dc1_i = tf.concat( [dc1_i1,dc1_i2], axis=-1 );
            dc1   = self._conv( dc1_i, filters=int(self.model_width*256), kernel_size=[3,3], padding="SAME" );
            dc1   = self._conv( dc1,   filters=int(self.model_width*256), kernel_size=[3,3], padding="SAME" );
            dc1_o = tf.image.resize_images( dc1 , size=[ tf.shape(c0)[1], tf.shape(c0)[2] ] );

        """This was the last deconvolved layer"""
        x_out = dc1_o;
		            
        """Agregate sparial features for postprocessing"""
        with tf.variable_scope("features",reuse=True):
            x_out = tf.identity( x_out, name='feature_map');
        print('Feature map dimensions: ', x_out.get_shape().as_list() );
        return x_out;

            
            
            
