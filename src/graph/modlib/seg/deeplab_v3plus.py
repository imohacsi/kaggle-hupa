import numpy as np
import tensorflow as tf
from . import model_template_seg
from ..layers import pooling
from ..layers import layers

class deeplab_v3plus( model_template_seg.model_template_seg ):
       
    
    def entry_block(self, x_in, filters, act_fn=tf.nn.elu ):
        x_scut = self._conv( x_in, filters, [1,1], strides=[2,2], act_fn=tf.nn.elu );    
        x = self._conv( x_in, filters, [3,3], act_fn=None, style="sepconv" );
        x = tf.layers.batch_normalization( x, axis=-1, training=self.is_training, trainable=True );
        x = act_fn( x );
        x = self._conv( x,    filters, [3,3], act_fn=None, style="sepconv" );
        x = tf.layers.batch_normalization( x, axis=-1, training=self.is_training, trainable=True );
        x = act_fn( x );
        x = self._conv( x,    filters, [3,3], act_fn=None, style="sepconv", strides=[2,2] ); 
        x = tf.layers.batch_normalization( x, axis=-1, training=self.is_training, trainable=True );
        x = act_fn( x );
        return x+x_scut;
    
    def middle_block(self, x_in, filters, act_fn=tf.nn.elu ):   
        x_scut = x_in;
        x = self._conv( x_in, filters, [3,3], act_fn=None, style="sepconv" );
        x = tf.layers.batch_normalization( x, axis=-1, training=self.is_training, trainable=True );
        x = act_fn( x );
        x = self._conv( x,    filters, [3,3], act_fn=None, style="sepconv" );
        x = tf.layers.batch_normalization( x, axis=-1, training=self.is_training, trainable=True );
        x = act_fn( x );
        x = self._conv( x,    filters, [3,3], act_fn=None, style="sepconv" );
        x = tf.layers.batch_normalization( x, axis=-1, training=self.is_training, trainable=True );
        x = act_fn( x );
        return x+x_scut;    
    
    def exit_block(self, x_in, filters, act_fn=tf.nn.elu ):
        x_scut = self._conv( x_in, filters, [1,1], strides=[2,2], act_fn=tf.nn.elu );    
        
        x = self._conv( x_in, filters, [3,3], act_fn=None, style="sepconv" );
        x = tf.layers.batch_normalization( x, axis=-1, training=self.is_training, trainable=True );
        x = act_fn( x );
        x = self._conv( x,    filters, [3,3], act_fn=None, style="sepconv" );
        x = tf.layers.batch_normalization( x, axis=-1, training=self.is_training, trainable=True );
        x = act_fn( x );
        x = self._conv( x,    filters, [3,3], act_fn=None, style="sepconv", strides=[2,2] ); 
        x = tf.layers.batch_normalization( x, axis=-1, training=self.is_training, trainable=True );
        x = act_fn( x );
        
        x_merg = x+x_scut
        x = self._conv( x_merg, filters, [3,3], act_fn=tf.nn.elu, style="sepconv" );
        x = tf.layers.batch_normalization( x, axis=-1, training=self.is_training, trainable=True );
        x = act_fn( x );
        x = self._conv( x,    int(1.5*filters), [3,3], act_fn=tf.nn.elu, style="sepconv" );
        x = tf.layers.batch_normalization( x, axis=-1, training=self.is_training, trainable=True );
        x = act_fn( x );
        x = self._conv( x,    int(2.0*filters), [3,3], act_fn=tf.nn.elu, style="sepconv" );    
        x = tf.layers.batch_normalization( x, axis=-1, training=self.is_training, trainable=True );
        x = act_fn( x );
                
        return x;


    def _asp_pool_block( self, x_in, filters=None, rate=[2,3,6] ):
        x_size= x_in.get_shape().as_list();
        if filters is None:
            filters = x_size[-1];

        #Create the spatial pooling pyramid
        sp_list = [ self._conv( x_in, x_size[-1], [1,1], padding="SAME") ];
        for scale in rate:
            sp_list.append( self._conv( x_in, x_size[-1], [3,3], padding="SAME", dilation=scale ) );
			
		#Calculate image level features
        img_lev = tf.reduce_mean( x_in, [1,2], keepdims=True )
        img_lev = self._conv( x_in, x_size[-1], [1,1], padding="SAME");
        img_lev = tf.image.resize_images( img_lev, size=[ tf.shape(x_in)[1], tf.shape(x_in)[2] ] );
		
        #Merge the spatial pooling pyramid
        x_merg = tf.concat( [img_lev]+sp_list, axis=-1 );
		
        #Assign output depth
        x_out =self._conv( x_merg, filters, [1,1], padding="SAME");
        return x_out; 

    def _build_model( self, x_in ):       
#        x_in = tf.image.resize_images( x_in, [101,101]);
        
        with tf.variable_scope('block0'):
            #First block is always standard convolution
            c0 = layers.conv2d( x_in, filters=int(self.model_width*64), kernel_size=[3,3], padding="SAME" );
            c0 = layers.conv2d( c0,   filters=int(self.model_width*64), kernel_size=[3,3], padding="SAME" );
			
        with tf.variable_scope('entry'):
            entr1 = self.entry_block( c0,    filters=int(self.model_width*128) );
            entr2 = self.entry_block( entr1, filters=int(self.model_width*256) );
#            entr3 = self.entry_block( entr2, filters=int(self.model_width*512) );
            entr_o = entr2;
            
        with tf.variable_scope('middle'):
            midd1 = self.entry_block( entr_o, filters=int(self.model_width*256) );
            midd2 = self.entry_block( midd1,  filters=int(self.model_width*256) );
            midd3 = self.entry_block( midd2,  filters=int(self.model_width*256) );
            midd4 = self.entry_block( midd3,  filters=int(self.model_width*256) );
            midd5 = self.entry_block( midd4,  filters=int(self.model_width*256) );
            midd6 = self.entry_block( midd5,  filters=int(self.model_width*256) );
#            midd7 = self.entry_block( midd6,  filters=int(self.model_width*512) );
#            midd8 = self.entry_block( midd7,  filters=int(self.model_width*512) );
#            midd9 = self.entry_block( midd8,  filters=int(self.model_width*512) );
#            midd10 = self.entry_block( midd9,  filters=int(self.model_width*512) );
#            midd11= self.entry_block( midd10,  filters=int(self.model_width*512) );
#            midd12 = self.entry_block( midd11,  filters=int(self.model_width*512) );
#            midd13 = self.entry_block( midd12,  filters=int(self.model_width*512) );
#            midd14 = self.entry_block( midd13,  filters=int(self.model_width*512) );
#            midd15 = self.entry_block( midd14,  filters=int(self.model_width*512) );
#            midd16 = self.entry_block( midd15,  filters=int(self.model_width*512) );
            midd_o = midd6;

        with tf.variable_scope('exit'):
            exit1 = self.entry_block( midd_o,  filters=int(self.model_width*512) );
            exit_o = exit1; 

        print("\nSize before ASSP: ", exit_o.get_shape().as_list() );			
        with tf.variable_scope('assp1'):
            assp1_i = self._asp_pool_block( exit_o, int(self.model_width*512), rate=[3,6,9] );
            assp1_o = assp1_i;
		
#        with tf.variable_scope('deconv2'):
#            dc2_i1 = tf.image.resize_images( assp1_o, size=[ tf.shape(entr_o)[1], tf.shape(entr_o)[2] ] );
#            dc2_i2 = self._conv( entr_o, filters=int(self.model_width*256), kernel_size=[1,1], padding="SAME" );
#            dc2_i = tf.concat( [dc2_i1,dc2_i2], axis=-1 );
#            dc2   = self._conv( dc2_i, filters=int(self.model_width*256), kernel_size=[3,3], padding="SAME" );
#            dc2_o = self._conv( dc2,   filters=int(self.model_width*256), kernel_size=[3,3], padding="SAME" );

        with tf.variable_scope('deconv1'):
            dc1_i1 = tf.image.resize_images( assp1_o, size=[ tf.shape(entr1)[1], tf.shape(entr1)[2] ] );
            dc1_i2 = self._conv( entr1, filters=int(self.model_width*64), kernel_size=[1,1], padding="SAME" );
            dc1_i = tf.concat( [dc1_i1,dc1_i2], axis=-1 );
            dc1   = self._conv( dc1_i, filters=int(self.model_width*256), kernel_size=[3,3], padding="SAME" );
            dc1   = self._conv( dc1,   filters=int(self.model_width*256), kernel_size=[3,3], padding="SAME" );
            dc1_o = tf.image.resize_images( dc1 , size=[ tf.shape(c0)[1], tf.shape(c0)[2] ] );

        """This was the last deconvolved layer"""
        x_out = dc1_o;
		            
        """Agregate sparial features for postprocessing"""
        with tf.variable_scope("features",reuse=True):
            x_out = tf.identity( x_out, name='feature_map');
        print('Feature map dimensions: ', x_out.get_shape().as_list() );
        return x_out;

            
            
            
