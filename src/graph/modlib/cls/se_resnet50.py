
import numpy as np
import tensorflow as tf
from . import model_template
from ..layers import layers
from ..layers import pooling

class se_resnet50( model_template.model_template ):
	
    #Residual bottleneck module with batch-normalization and pooling
    @layers.SENet_wrapper
    def _bottleneck( self, x_in, filters, filters_out=None, strides=[1,1], dilation=1, act_fn=tf.nn.elu ):
        x_size= x_in.get_shape().as_list();
		
        #Make sure we have the right output dimension
        if filters_out is None:
            filters_out = x_size[-1];
            
        x = self._conv( x_in, filters, [1,1], strides=strides, act_fn=None, use_bias=False );
        x = tf.layers.batch_normalization( x, axis=-1, training=self.is_training, trainable=True );
        x = act_fn( x );
        x = self._conv( x, filters, [3,3], dilation=dilation, act_fn=None, use_bias=False );
        x = tf.layers.batch_normalization( x, axis=-1, training=self.is_training, trainable=True );
        x = act_fn( x );
        x = self._conv( x, filters_out, [1,1], act_fn=None, use_bias=False);
        x = tf.layers.batch_normalization( x, axis=-1, training=self.is_training, trainable=True );

        return x;        


    def _residual_bottleneck( self, x_in, filters, filters_out=None, strides=[1,1], dilation=1, act_fn=tf.nn.elu ):
        x_size= x_in.get_shape().as_list();
		
		#Make sure we have the right output dimension
        if filters_out is None:
            filters_out = x_size[-1];

        #If increasing depth before pooling
        if filters_out != x_size[-1]:
            x_scut = self._conv( x_in, filters=filters_out, kernel_size=[1,1], strides=strides, padding="SAME" );
        else:
            x_scut = x_in;
		
        x_bot = self._bottleneck( x_in, filters, filters_out, strides, dilation, act_fn );
        return x_bot+x_scut;        
                 
    
    def _build_model( self, x_in ):         
        with tf.variable_scope('block1'):
			#First block is always standard convolution
            c1 = layers.conv2d( x_in, filters=int(self.model_width*64), kernel_size=[3,3], padding="SAME" );
            c1 = layers.conv2d( c1,    filters=int(self.model_width*64), kernel_size=[3,3], padding="SAME" );
            c1_o = tf.nn.pool( c1, [3,3], strides=[2,2], pooling_type='MAX', padding="SAME");
                
        with tf.variable_scope('block2'):
            c2_i = self._residual_bottleneck( c1_o, filters=int(self.model_width*256/4), filters_out=int(self.model_width*256)  );
            c2   = self._residual_bottleneck( c2_i,   filters=int(self.model_width*256/4) );
            c2   = self._residual_bottleneck( c2,   filters=int(self.model_width*256/4) );
            c2_o = c2;
			
        with tf.variable_scope('block3'):
            c3_i = self._residual_bottleneck( c2_o, filters=int(self.model_width*512/4), filters_out=int(self.model_width*512), strides=[2,2] );
            c3   = self._residual_bottleneck( c3_i,   filters=int(self.model_width*512/4) );
            c3   = self._residual_bottleneck( c3,   filters=int(self.model_width*512/4) );
            c3   = self._residual_bottleneck( c3,   filters=int(self.model_width*512/4) );
            c3_o = c3;
			
        with tf.variable_scope('block4'):
            c4_i = self._residual_bottleneck( c3_o, filters=int(self.model_width*1024/4), filters_out = int(self.model_width*1024), strides=[2,2] );
            c4   = self._residual_bottleneck( c4_i,   filters=int(self.model_width*1024/4) );
            c4   = self._residual_bottleneck( c4,   filters=int(self.model_width*1024/4) );
            c4   = self._residual_bottleneck( c4,   filters=int(self.model_width*1024/4) );
            c4   = self._residual_bottleneck( c4,   filters=int(self.model_width*1024/4) );
            c4   = self._residual_bottleneck( c4,   filters=int(self.model_width*1024/4) );
            c4_o = c4;
			
        with tf.variable_scope('block5'):
            c5_i = self._residual_bottleneck( c4_o, filters=int(self.model_width*2048/4), filters_out = int(self.model_width*2048), strides=[2,2] );
            c5   = self._residual_bottleneck( c5_i,   filters=int(self.model_width*2048/4) );
            c5   = self._residual_bottleneck( c5,   filters=int(self.model_width*2048/4) );
            c5   = self._residual_bottleneck( c5,   filters=int(self.model_width*2048/4) );
            c5_o = c5;


#        x = self._conv( c5_o, self.model_width*1024, [1,1], act_fn=tf.nn.elu );
#        x = self._conv( x, 28, [1,1], act_fn=None );                  
        print("Last conv layer:\n", c5_o)    
        
        """Agregate sparial features for postprocessing"""
        with tf.variable_scope("global_pool",reuse=True):
            x1 = tf.reduce_mean(c5_o, [1, 2], name='globalpool_mean');
            x2 = tf.reduce_max( c5_o, [1, 2], name='globalpool_max');
            x = tf.identity( tf.concat( [x1,x2], axis=-1 ), name="globalpool" )

        """Adding dense layers, dropout and feature rebalancing"""
        #Dense 1
        x = tf.layers.batch_normalization( x, axis=-1, training=self.is_training, trainable=True );
        x = tf.nn.dropout( x, self.p_keep );
        x = tf.layers.dense( x, self.model_width*2048, activation=tf.nn.elu );
        #Dense 2
        x = tf.layers.batch_normalization( x, axis=-1, training=self.is_training, trainable=True );
        x = tf.nn.dropout( x, keep_prob=self.p_keep );
        x = tf.layers.dense( x, self.model_width*1024, activation=tf.nn.elu );
        #Final features
        x = tf.layers.batch_normalization( x, axis=-1, training=self.is_training, trainable=True );
        x = tf.nn.dropout( x, keep_prob=self.p_keep );
        
        
        with tf.variable_scope("features",reuse=True):
            x = tf.identity( x, name='feature_vector');
        print('Feature vector dimensions: ', x.get_shape().as_list() );
        return x;

            
            
            
