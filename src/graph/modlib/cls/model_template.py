

import tensorflow as tf
from ..layers import final_ops
from ..layers import losses
from ..layers import layers


FLAGS = tf.app.flags.FLAGS

"""Add a simple scalar to summary if it exists and matches format"""
def get_tensor( tensor_name, substitute_name=None ):
    graph = tf.get_default_graph();
    try:
        return graph.get_tensor_by_name( tensor_name );
    except:
        print( "WARNING: Could not retrieve tensor: ", tensor_name );
        if substitute_name is not None:
            try:
                return graph.get_tensor_by_name( substitute_name );
            except:
                pass;
        pass;



class model_template:   
    def __init__( self, num_classes, is_multi=False, class_freq=None ):
        self.model_width = 1.0;
        self.num_classes = num_classes;
        self.is_multi = is_multi;
        self.class_freq = class_freq;
        self.class_weights = None;
        
        if self.class_freq is not None and FLAGS.loss_auto_class_balance:
            if FLAGS.loss_auto_class_balance==1:
                #Focal loss rebalancing: positive example fraction
                self.class_weights = 1.0-self.class_freq;
            elif FLAGS.loss_auto_class_balance==2:
                self.class_weights = 1.0-(self.class_freq**0.5);
            else:
                print("WARNING: unsupported auto_class_balance_value")
                self.class_weights = None;
        
        
    def create_new_model(self, d_in=3 ):
        with tf.variable_scope("model",reuse=False):
            self.is_training = tf.placeholder_with_default( False, shape=(), name="is_training" );
            self.p_keep = tf.placeholder_with_default( 1.0, shape=(), name="p_keep" );

            if not self.is_multi:
                x_in,labels =   self._build_input_string_handle( d_in );
            else:
                x_in,labels =   self._build_input_string_handle_multi( d_in );

            evidence =        self._build_model( x_in );
            if not self.is_multi:
                loss =          self._build_final_op( evidence, labels );
            else:
                loss =          self._build_final_op_multi( evidence, labels );

            self.train_op = self._build_training_op( loss );
    
    def load_old_graph( self, path, name ):
        metafile_name = '%s/%s.meta' % ( path, name)        
        loader = tf.train.import_meta_graph( metafile_name );   
        return loader;
    
    def __str__( self ):
        return "This is a general TF model template class."


    """Learn a colormap transformation based on local features"""
    def _learn_cmap( self, x_in, d_out=3, filters=8 ):      
        c1x1 = layers.conv2d( x_in, filters, [1,1], padding='SAME', act_fn=tf.nn.elu );
        c7x7 = layers.conv2d( x_in, filters, [7,7], padding='SAME', act_fn=tf.nn.elu );
        merg  = tf.concat( [c1x1,c7x7], axis=-1 );
        
        x_cm = layers.conv2d( merg, d_out, [1,1], padding='SAME', act_fn=tf.nn.tanh );
        return x_cm;
        

    def _build_input_string_handle( self, d_in ):
        with tf.variable_scope("input",reuse=None):
            """Merging placeholders with string handle"""
            print("\nInput handle dimension is: ",d_in);
            with tf.device('/cpu:0'):            
                handle = tf.placeholder(tf.string, shape=[], name='input_handle');
                iterator =  tf.data.Iterator.from_string_handle( handle, (tf.float32,tf.int32), (tf.TensorShape([None,None,None,d_in]), tf.TensorShape([None])) );

            x_raw, y_raw = iterator.get_next();
            x_in = tf.identity( x_raw, name='examples');
            self.x_in=x_in;
#            x_in = tf.reshape(x_in, [-1, config.image_size[0],config.image_size[1],3])
            y_in = tf.identity( y_raw, name='labels' );
            self.y_in=y_in
            labels = tf.one_hot( y_in, depth=self.num_classes );  
            
            return x_in,labels

    def _build_input_string_handle_multi( self, d_in ):
        with tf.variable_scope("input",reuse=None):
            """Merging placeholders with string handle"""
            print("\nInput handle dimension is: ",d_in, " ", self.num_classes);
            with tf.device('/cpu:0'):            
                handle = tf.placeholder(tf.string, shape=[], name='input_handle');
                iterator =  tf.data.Iterator.from_string_handle( handle, (tf.float32,tf.float32), (tf.TensorShape([None,None,None,d_in]), tf.TensorShape([None,28])) );

            x_raw, y_raw = iterator.get_next();
            x_raw = tf.image.resize_images(x_raw, [FLAGS.image_crop_x,FLAGS.image_crop_y]);
            if FLAGS.learn_cmap:
                with tf.variable_scope("learn_cmap",reuse=False):
                    x_raw = self._learn_cmap( x_raw, d_out=FLAGS.learn_cmap_dims );
                    
            x_in = tf.identity( x_raw, name='examples');
            y_in = tf.identity( y_raw, name='labels' );
            labels = y_in; 
            self.x_in=x_in;
            self.y_in=y_in

            return x_in,labels

    def _build_model( self, x_in ):
        print( "Dummy model from template!")
        
    def _build_final_op( self, x_in, labels ):
        with tf.variable_scope("finals",reuse=False):
            print(x_in)
            logits   = final_ops.logits_dense ( x_in, self.num_classes );
            loss_xen = losses.loss_xentropy_softmax( logits, labels );
            loss_ent = losses.loss_entropy_softmax( logits, self.class_weights, tempr=0.75, bsrate=FLAGS.loss_reg_entropy );            
            loss_l2  = losses.loss_kernel_l2( FLAGS.loss_reg_l2 );
            loss     = tf.identity( loss_xen +loss_l2+loss_ent, name='loss');   
            pred     = losses.pred_softmax( logits );   
            final_ops.metrics_accuracy( pred, labels );
            return loss;

    def _build_final_op_multi( self, x_in, labels, in_logits=True ):
        with tf.variable_scope("finals",reuse=False):
            print("INFO: Using multi-label targeting!\n",x_in)
            
            if in_logits:
#                logits = final_ops.logits_dense ( x_in, self.num_classes, act_fn=None );
                logits   = final_ops.logits_ident( x_in ); 
                pred     = losses.pred_sigmoid( logits );             
            else:
                logits   = final_ops.logits_invsig( x_in );             
                pred     = losses.pred_ident( x_in ); 
                
            if in_logits:
                loss_foc = losses.loss_xentropy_focal_logits( logits, labels, FLAGS.loss_focal_gamma, FLAGS.loss_focal_alpha, class_weights=self.class_weights );
                loss_xen = losses.loss_xentropy_sigmoid_logits( logits, labels, class_weights=self.class_weights  );
                loss_ent = losses.loss_entropy_sigmoid_logits( logits, self.class_weights, tempr=1.0,bsrate=FLAGS.loss_reg_entropy );
                losses.loss_f1_macro_logits( logits, labels );
            else:                
                loss_foc = losses.loss_xentropy_focal_prob( pred, labels, FLAGS.loss_focal_gamma, FLAGS.loss_focal_alpha, class_weights=self.class_weights );
                loss_xen = losses.loss_xentropy_sigmoid_prob( pred, labels, class_weights=self.class_weights  );
                loss_ent = losses.loss_entropy_sigmoid_prob( pred, self.class_weights, tempr=1.0,bsrate=FLAGS.loss_reg_entropy );
                losses.loss_f1_macro_logits( logits, labels );
            #Independent losses and total loss
            loss_l2  = losses.loss_kernel_l2( FLAGS.loss_reg_l2 );
            loss     = tf.identity( loss_foc + loss_l2+loss_ent, name='loss');    

            final_ops.metrics_f1_macro( pred, labels );
            final_ops.metrics_f1_macro_running( pred, labels, self.num_classes );
            return loss;

    def _build_training_op( self, loss ):
        with tf.variable_scope("train",reuse=False):
            train_op,lrate = self.set_optimizer( loss=loss, lrate_init=FLAGS.lrate_init, opt_par=FLAGS.optimizer_type, decay_type=FLAGS.lrate_decay_type, decay_per=FLAGS.lrate_decay_per, num_iter=FLAGS.last_step );
        return train_op;
        print( "Dummy training op from template!")
        

    def set_optimizer( self, loss, lrate_init, opt_par='mom', decay_type='exp', decay_per=2000, num_iter=None ):
        global_step = tf.train.get_or_create_global_step();
        ramp_up = FLAGS.lrate_ramp_up;
        
        with tf.variable_scope("optimizer",reuse=None):  
            if decay_type=='lincos':
                num_periods = int( num_iter/decay_per );
                learning_rate = tf.train.linear_cosine_decay( lrate_init, global_step, decay_steps=num_iter, num_periods=num_periods, alpha=1e-8, beta=1e-8 );
            elif decay_type=='lineargrowth':
                learning_rate = lrate_init*( tf.to_float(global_step) /num_iter );            
            elif decay_type=='expcos':     
                print( "Decay period ",decay_per )
                num_periods = int( 1000*num_iter/decay_per );
                cos_rate = tf.train.linear_cosine_decay( 1.0, (global_step-ramp_up), decay_steps=1000*num_iter, num_periods=num_periods, alpha=1e-8, beta=0.5 );
                exp_rate = tf.train.exponential_decay( lrate_init, (global_step-ramp_up), decay_per/2, 0.90 );
                dlearning_rate = exp_rate*cos_rate;
                learning_rate = tf.cond( global_step<ramp_up, lambda: 1.5*lrate_init*( tf.to_float(global_step)/ramp_up ),
                                                          lambda: dlearning_rate );                
            else:
                learning_rate = tf.train.exponential_decay( lrate_init, global_step, decay_per, 0.90 );
            learning_rate = tf.identity( learning_rate, name='decayed_learning_rate');
    

            if opt_par=='mom':
                optimizer =  tf.train.MomentumOptimizer(learning_rate,0.90);
            elif opt_par=='adam':
                optimizer =  tf.train.AdamOptimizer(learning_rate);
            else:
                optimizer =  tf.train.GradientDescentOptimizer(learning_rate);

            update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
            with tf.control_dependencies(update_ops):
                train = optimizer.minimize( loss, global_step=global_step, name='minimize' );
        return train,learning_rate;



    def load_graph_checkpoints( self ):
        self.input_handle = get_tensor("model/input/input_handle:0");  
        self.input_images = get_tensor("model/input/examples:0");    
        self.input_labels = get_tensor("model/input/labels:0");      
        self.res_loss_sl = get_tensor("model/finals/loss_entropy/loss_entropy:0");
        self.res_loss_xe = get_tensor("model/finals/loss_xentr/loss_xentropy:0");
        self.res_loss_l2 = get_tensor("model/finals/loss_l2:0");
        self.res_loss_f1 = get_tensor("model/finals/loss_f1/loss_f1:0");
        self.res_loss_foc = get_tensor("model/finals/loss_focal/loss_focal:0");
        self.res_loss  = get_tensor("model/finals/loss:0");
        self.is_training = get_tensor("model/is_training:0");
        self.p_keep = get_tensor("model/p_keep:0");
        self.vec_feat = get_tensor("model/features/feature_vector:0");
        self.res_logit = get_tensor("model/finals/logits/logits:0");
        self.res_accur = get_tensor("model/finals/accuracy/accuracy:0");
        self.res_run_accur_cw = get_tensor("model/finals/running_accuracy/classwise_accuracy:0" );
        self.res_run_accur = get_tensor("model/finals/running_accuracy/accuracy:0" );

        self.res_pred  = get_tensor("model/finals/predictions/predictions:0");
        self.res_gstep = get_tensor("model/train/global_step:0");
        self.res_lrate = get_tensor("model/train/optimizer/decayed_learning_rate:0");
        self.train_op  = get_tensor("model/train/optimizer/minimize:0");
        self.last_feature_map = get_tensor("model/conv_last:0"); 

            
    def set_model_width( self, val ):
        self.model_width = val;

    def get_basic_nodes( self ):
        return self.train_op,self.res_loss,self.res_accur,self.input_handle,self.is_training,self.p_keep;
		
		
