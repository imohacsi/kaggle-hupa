
import numpy as np
import tensorflow as tf
from . import model_template
from ..layers import layers
from ..layers import pooling

class xception( model_template.model_template ):     
                 
    def _input_block( self, x_in, filters, act_fn=tf.nn.elu ):    
        x_scut = layers.conv2d( x_in, filters=int(self.model_width*filters), kernel_size=[1,1], strides=[2,2], padding="SAME", act_fn=act_fn );
        
        x = layers.conv2d( x_in, filters=int(self.model_width*filters), kernel_size=[3,3], padding="SAME", act_fn=None, style="sepconv", use_bias=False );	
        x = tf.layers.batch_normalization( x, axis=-1, training=self.is_training, trainable=True );
        x = act_fn( x );
        x = layers.conv2d( x, filters=int(self.model_width*filters), kernel_size=[3,3], padding="SAME", act_fn=None, style="sepconv", use_bias=False );	
        x = tf.layers.batch_normalization( x, axis=-1, training=self.is_training, trainable=True );
        x = act_fn( x );
        x = tf.nn.pool( x, [3,3], strides=[2,2], pooling_type='MAX', padding="SAME");
        return x+x_scut;

    def _middle_block( self, x_in, filters, act_fn=tf.nn.elu ):    
        x = layers.conv2d( x_in, filters=int(self.model_width*filters), kernel_size=[3,3], padding="SAME", act_fn=None, style="sepconv", use_bias=False );	
        x = tf.layers.batch_normalization( x, axis=-1, training=self.is_training, trainable=True );
        x = act_fn( x );
        x = layers.conv2d( x, filters=int(self.model_width*filters), kernel_size=[3,3], padding="SAME", act_fn=None, style="sepconv", use_bias=False );	
        x = tf.layers.batch_normalization( x, axis=-1, training=self.is_training, trainable=True );
        x = act_fn( x );
        x = layers.conv2d( x, filters=int(self.model_width*filters), kernel_size=[3,3], padding="SAME", act_fn=None, style="sepconv", use_bias=False );	
        x = tf.layers.batch_normalization( x, axis=-1, training=self.is_training, trainable=True );
        x = act_fn( x );
        return x+x_in;

    def _exit_block( self, x_in, filters, act_fn=tf.nn.elu ):    
        x_shrt = layers.conv2d( x_in, filters=int(self.model_width*filters), kernel_size=[1,1], strides=[2,2], padding="SAME" );
        
        x = layers.conv2d( x_in, filters=int(self.model_width*filters), kernel_size=[3,3], padding="SAME", act_fn=None, style="sepconv", use_bias=False );	
        x = tf.layers.batch_normalization( x, axis=-1, training=self.is_training, trainable=True );
        x = act_fn( x );
        x = layers.conv2d( x, filters=int(self.model_width*filters), kernel_size=[3,3], padding="SAME", style="sepconv" );	
        x = tf.nn.pool( x, [3,3], strides=[2,2], pooling_type='MAX', padding="SAME");
        
        x = x+x_shrt;       
        x = tf.layers.batch_normalization( x, axis=-1, training=self.is_training, trainable=True );
        x = act_fn( x );
        x = layers.conv2d( x, filters=int(self.model_width*1.5*filters), kernel_size=[3,3], padding="SAME", act_fn=None, style="sepconv", use_bias=False );	
        x = tf.layers.batch_normalization( x, axis=-1, training=self.is_training, trainable=True );
        x = act_fn( x );
        x = layers.conv2d( x, filters=int(self.model_width*2.0*filters), kernel_size=[3,3], padding="SAME", act_fn=None, style="sepconv", use_bias=False );	
        x = tf.layers.batch_normalization( x, axis=-1, training=self.is_training, trainable=True );
        x = act_fn( x );
        return x;



    def _build_model( self, x_in ):         
        with tf.variable_scope('block1'):
            c1 = layers.conv2d( x_in, filters=int(self.model_width*32), kernel_size=[3,3], padding="SAME" );
            c1 = layers.conv2d( c1,    filters=int(self.model_width*64), kernel_size=[3,3], padding="SAME" );
            c1_o = tf.nn.pool( c1, [3,3], strides=[2,2], pooling_type='MAX', padding="SAME");
                
        with tf.variable_scope('block2'):
            c2_o = self._input_block( c1_o, 128 );

        with tf.variable_scope('block3'):
            c3_o = self._input_block( c2_o, 256 );

        with tf.variable_scope('block4'):
            c4_o = self._input_block( c3_o, 728 );

        with tf.variable_scope('block5'):
            c5 = self._middle_block( c4_o, 728 );
            c5 = self._middle_block( c5, 728 );
            c5 = self._middle_block( c5, 728 );
            c5 = self._middle_block( c5, 728 );
            c5 = self._middle_block( c5, 728 );
            c5 = self._middle_block( c5, 728 );
            c5 = self._middle_block( c5, 728 );
            c5_o = self._middle_block( c5, 728 );

        with tf.variable_scope('block6'):
            c6_o = self._exit_block( c5_o, 728 );

                    
        """Agregate sparial features for postprocessing"""
        with tf.variable_scope("global_pool",reuse=True):
            x1 = tf.reduce_mean(c6_o, [1, 2], name='globalpool_mean');
            x2 = tf.reduce_max( c6_o, [1, 2], name='globalpool_max');
            x = tf.identity( tf.concat( [x1,x2], axis=-1 ), name="globalpool" )

        """Adding dense layers, dropout and feature rebalancing"""
        #Dense 1
        x = tf.layers.batch_normalization( x, axis=-1, training=self.is_training, trainable=True );
        x = tf.nn.dropout( x, self.p_keep );
        x = tf.layers.dense( x, self.model_width*1024, activation=tf.nn.elu );
        #Dense 2
#        x = tf.layers.batch_normalization( x, axis=-1, training=self.is_training, trainable=True );
#        x = tf.nn.dropout( x, keep_prob=self.p_keep );
#        x = tf.layers.dense( x, self.model_width*512, activation=tf.nn.elu );
        #Final features
        x = tf.layers.batch_normalization( x, axis=-1, training=self.is_training, trainable=True );
        x = tf.nn.dropout( x, keep_prob=self.p_keep );

            
        with tf.variable_scope("features",reuse=True):
            x = tf.identity( x, name='feature_vector');
        print('Feature vector dimensions: ', x.get_shape().as_list() );
        return x;

            
            
            
