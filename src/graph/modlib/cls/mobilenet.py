#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 20 18:03:55 2018

@author: imohacsi
"""


import numpy as np
import tensorflow as tf
from . import model_template
from ..layers import layers
from ..layers import pooling

class mobilenet( model_template.model_template ):
	
	#Residual bottleneck module with batch-normalization and pooling
    def _mnet_block( self, x_in, filters, strides=1, act_fn=tf.nn.elu ):
        x = layers.conv2d( x_in, filters=int(self.model_width*filters), kernel_size=[3,3], strides=strides, padding="SAME", act_fn=None, style="sepconv", use_bias=False );
        x = tf.layers.batch_normalization( x, axis=-1, training=self.is_training, trainable=True );
        x = act_fn( x );
        return x;        
    
    def _build_model( self, x_in ):         
        with tf.variable_scope('block1'):
			#First block is always standard convolution
            c1 = layers.conv2d( x_in, filters=int(self.model_width*32), kernel_size=[3,3], padding="SAME" );
            c1 = layers.conv2d( c1,   filters=int(self.model_width*32), kernel_size=[3,3], padding="SAME", style="sepconv" );
            c1 = self._mnet_block( c1, 64, 2 );            
            c1_o = c1
            
        with tf.variable_scope('block2'):
            c2 = self._mnet_block( c1_o, 128, 2 );
            c2 = self._mnet_block( c2, 128 );
            c2_o = c2

        with tf.variable_scope('block3'):
            c3 = self._mnet_block( c2_o, 256, 2 );
            c3 = self._mnet_block( c3, 256 );
            c3_o = c3
            
        with tf.variable_scope('block4'):
            c4 = self._mnet_block( c3_o, 512, 2 );
            c4 = self._mnet_block( c4, 512 );
            c4 = self._mnet_block( c4, 512 );
            c4 = self._mnet_block( c4, 512 );
            c4 = self._mnet_block( c4, 512 );
            c4 = self._mnet_block( c4, 512 );
            c4_o = c4
            
        with tf.variable_scope('block5'):
            c5 = self._mnet_block( c4_o, 1024, 2 );
            c5 = self._mnet_block( c5, 1024 );
            c5_o = c5;
                    
        """Agregate sparial features for postprocessing"""
        with tf.variable_scope("global_pool",reuse=True):
            x = tf.reduce_mean(c5_o, [1, 2], name='globalpool');            
            
        with tf.variable_scope("features",reuse=True):
            x = tf.identity( x, name='feature_vector');
        print('Feature vector dimensions: ', x.get_shape().as_list() );
        return x;

            
            
            
