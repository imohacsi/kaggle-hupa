# -*- coding: utf-8 -*-
"""
Created on Sun Mar 11 02:48:59 2018

@author: mistvan
"""

import tensorflow as tf
import numpy as np
from . import layers

def logits_ident( x_in, num_class=None, act_fn=None ):
    with tf.variable_scope("logits",reuse=None):    
        logits = tf.identity( x_in, name='logits' );
    return logits;

def logits_invsig( y_in, num_class=None, act_fn=None ):
    with tf.variable_scope("logits",reuse=None):    
        logits = tf.identity( tf.log( y_in/(1.0-y_in+1e-15) +1e-15 ), name='logits' );
    return logits;


def logits_dense( x_in, num_class, act_fn=None ):
    with tf.variable_scope("logits",reuse=None):    
        logits = tf.layers.dense( inputs=x_in, units=num_class, activation=act_fn );
        logits = tf.identity( logits, name='logits' );
    return logits;

def logits_conv( x_in, num_class, act_fn=None ):
    with tf.variable_scope("logits",reuse=None):    
        logits = layers.conv2d( x_in, num_class, [1,1], act_fn=act_fn );
        logits = tf.identity( logits, name='logit_map' );
    return logits;
  

def metrics_accuracy(predictions,labels):
    """Here we define the correctly classified fraction of the database """
    ndim = len(predictions.get_shape().as_list() )  
    with tf.variable_scope("accuracy",reuse=None):    
#        is_correct = tf.logical_and( tf.cast(cls_real,tf.int32)
        
        
        correct_prediction = tf.equal( tf.argmax(predictions, ndim-1), tf.argmax( labels , ndim-1) );
        accuracy= tf.reduce_mean( tf.cast( correct_prediction, tf.float32 ) )  
        accuracy = tf.identity( accuracy, name='accuracy' );
    return accuracy;

def metrics_iou(predictions,labels):
    x_size = predictions.get_shape().as_list();

    """Here we calculate the IOU metric for segmentation """
    with tf.variable_scope("biou",reuse=None):    
        cls_pred = tf.argmax(predictions, axis=-1);
        cls_real = tf.argmax(labels,axis=-1);
        
        cls_real = tf.one_hot( tf.cast(cls_real,tf.int32), depth=x_size[-1], dtype=tf.int32 );   
        cls_pred = tf.one_hot( tf.cast(cls_pred,tf.int32), depth=x_size[-1], dtype=tf.int32 );   

        cls_reals = tf.cast( tf.unstack( cls_real, axis=-1 ), tf.bool);
        cls_preds = tf.cast( tf.unstack( cls_pred, axis=-1 ), tf.bool);
        
        cl_iou = [];
        for cc in range(x_size[-1]):
            tp =       tf.reduce_sum( tf.cast( tf.logical_and( cls_reals[cc], cls_preds[cc]), tf.int32 ) );
            tp_fp_fn = tf.reduce_sum( tf.cast( tf.logical_or(  cls_reals[cc], cls_preds[cc]), tf.int32 ) );
            cl_iou.append( tf.cast(tp,tf.float32) / tf.cast(tp_fp_fn,tf.float32) );
        
        #Hehe
        miau = tf.add_n( cl_iou )/float(x_size[-1]);
        iou_mean = tf.identity( cl_iou[-1], name='batch_iou_mean' );

    return cl_iou,iou_mean;

def metrics_hamming(predictions,labels):
    """Here we define the incorrectly classified fraction of the database """
    with tf.variable_scope("hamming",reuse=None):    
        predictions = tf.round( predictions );
        wrong_prediction = tf.logical_xor( tf.cast(predictions,tf.int32), tf.cast(labels,tf.int32) );
        hamming= tf.reduce_mean( tf.cast( wrong_prediction, tf.float32 ) )  
        hamming = tf.identity( hamming, name='accuracy' );
    return hamming;

def metrics_f1_micro( pred, labels ):
    with tf.variable_scope("accuracy",reuse=None):    
        pred = tf.round( pred );
        tp = tf.cast( pred*labels, tf.float32 )
    #    tn = tf.cast( (1.0-pred)*(1.0-labels), tf.float32 )
        fp = tf.cast( pred*(1.0-labels), tf.float32 )
        fn = tf.cast( (1.0-pred)*labels, tf.float32 )
    
        prec = tf.reduce_sum( tp, axis=-1 ) / ( tf.reduce_sum( tp+fp, axis=-1 )+1e-15);
        recl = tf.reduce_sum( tp, axis=-1 ) / ( tf.reduce_sum( tp+fn, axis=-1 )+1e-15);
    
        f1 = 2.0*prec*recl / (prec+recl+1e-15);
        f1 = tf.where(tf.is_nan(f1), tf.zeros_like(f1), f1)
        f1 = tf.reduce_mean( f1, name='accuracy' );
    return f1;

def metrics_f1_macro( pred, labels ):
    with tf.variable_scope("accuracy",reuse=None):    
        pred = tf.round( pred );
        tp = tf.reduce_sum( tf.cast( pred*labels, tf.float32 ), axis=0 );
    #    tn = tf.reduce_sum( tf.cast( (1.0-pred)*(1.0-labels), tf.float32 ), axis=0 );
        fp = tf.reduce_sum( tf.cast( pred*(1.0-labels), tf.float32 ), axis=0 );
        fn = tf.reduce_sum( tf.cast( (1.0-pred)*labels, tf.float32 ), axis=0 );
    
        prec = tp / ( tp + fp + tf.constant(1e-7) );
        recl = tp / ( tp + fn + tf.constant(1e-7) );
    
        f1 = 2.0*prec*recl / (prec+recl);
        f1 = tf.boolean_mask( f1, tf.is_finite(f1) );
#        f1 = tf.where(tf.is_nan(f1), tf.zeros_like(f1), f1)
        f1 = tf.reduce_mean( f1, name='accuracy' );
    return f1;


def metrics_f1_macro_running( pred, labels, num_classes ):
    with tf.variable_scope("running_accuracy",reuse=None):    
        H = tf.get_variable( "classwise_accuracy", [num_classes], dtype=tf.float32, trainable=False, initializer=tf.zeros_initializer() );
        
        pred = tf.round( pred );
        tp = tf.reduce_sum( tf.cast( pred*labels, tf.float32 ), axis=0 );
    #    tn = tf.reduce_sum( tf.cast( (1.0-pred)*(1.0-labels), tf.float32 ), axis=0 );
        fp = tf.reduce_sum( tf.cast( pred*(1.0-labels), tf.float32 ), axis=0 );
        fn = tf.reduce_sum( tf.cast( (1.0-pred)*labels, tf.float32 ), axis=0 );
    
        prec = tp / ( tp + fp + tf.constant(1e-7) );
        recl = tp / ( tp + fn + tf.constant(1e-7) );
    
        f1_curr = 2.0*prec*recl / (prec+recl);
        
        tf.add_to_collection( name=tf.GraphKeys.UPDATE_OPS, value=H.assign( tf.where( tf.is_finite(f1_curr), 0.9*H+0.1*f1_curr, H ) ) );
        
        f1 = tf.boolean_mask( H, tf.is_finite(H) );
        f1 = tf.reduce_mean( f1, name='accuracy' );
    return f1;





#
#
#
#
#def batch_ml_accuracy(predictions,labels):
#    """Here we define the correctly classified fraction of the database """
#    with tf.variable_scope("accuracy",reuse=None):    
#        predictions = tf.round( predictions );
#        correct_prediction = tf.logical_and( tf.cast(predictions,tf.bool), tf.cast(labels,tf.bool) );
##        correct_prediction = tf.equal( tf.cast(predictions,tf.int32), tf.cast(labels,tf.int32) );
#        accuracy= tf.reduce_mean( tf.cast( correct_prediction, tf.float32 ) )  / tf.reduce_mean( labels ); 
#        accuracy = tf.identity( accuracy, name='accuracy' );
#    return accuracy;
#
#
#
#
