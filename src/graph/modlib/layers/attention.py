# -*- coding: utf-8 -*-
"""
Created on Mon Aug 27 11:53:03 2018

@author: mohacsii
"""
import tensorflow as tf
try:
    from . import pooling    
    from . import layers
except:
    import pooling
    import layers
    pass;
    


def attention_average_global( x_in ):
    x_size = x_in.get_shape().as_list();
    gpooled = tf.reduce_mean(x_in,axis=[1,2]);
    
    x_out = layers.conv2d( x_in, x_size[-1], [1,1], act_fn=tf.nn.elu, bias_vec=gpooled );
    return x_out
    
    


def attention_recurrent_global( x_in ):
    x_size = x_in.get_shape().as_list();

    v_pool = pooling.pool_recurrent( x_in );
    v_pool = tf.layers.dense(v_pool, x_size[-1] )
    
    x_map = layers.conv2d( x_in, x_size[-1],[1,1], bias_vec=v_pool )
    return x_map;


def attention_feature_global( x_in ):
    x_size = x_in.get_shape().as_list();

    a1_map  = layers.conv2d( x_in, x_size[-1]/4,[1,1], padding='SAME', act_fn=tf.nn.softplus )
    att_map  = layers.conv2d( a1_map,1,[1,1], padding='SAME', act_fn=tf.nn.softplus )
#    att_map = tf.expand_dims(xx_pos,axis=0)
    norm = tf.expand_dims( tf.expand_dims( tf.reduce_sum( att_map, axis=[1,2] ), axis=1 ), axis=1 );
    att_map =att_map / norm;
    
    x_attended = x_in * att_map;
    return x_attended;

def attention_class_global( x_in, num_classes ):
    x_size = x_in.get_shape().as_list();

    a1_map  = layers.conv2d( x_in, x_size[-1],[3,3], padding='SAME', act_fn=tf.nn.softplus )
    a2_1_map  = layers.conv2d( a1_map, num_classes,[1,1], padding='SAME', act_fn=None )
    a2_2_map  = layers.conv2d( a1_map, num_classes,[1,1], padding='SAME', act_fn=None )
    att_map = tf.nn.softmax( a2_1_map ) - tf.nn.sigmoid( a2_2_map );
    
    norm = tf.reduce_sum( att_map, axis=[1,2] );
    att_map =att_map / norm;
    
    x_attended = x_in * att_map;
    return x_attended;
