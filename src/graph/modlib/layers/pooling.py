# -*- coding: utf-8 -*-
"""
Created on Sat Feb  3 14:34:31 2018

@author: mistvan
"""

import tensorflow as tf
from . import layers
from . import attention

"""Global pooling with recurrent network"""
def pool_recurrent(x_in, pool_out=None, cell_size=None, cell_numb=1):
    x_size = x_in.get_shape().as_list();
    x_shape = tf.shape( x_in );
    with tf.variable_scope('rec_pool', reuse=tf.AUTO_REUSE ):
        #Generating exact location map
        x_pos=tf.linspace(0.0,1.0,tf.shape(x_in)[1]);
        y_pos=tf.linspace(0.0,1.0,tf.shape(x_in)[2]);
        xx_pos,yy_pos = tf.meshgrid(x_pos, y_pos);
        xx_pos = tf.expand_dims( tf.expand_dims(xx_pos,axis=0), axis=-1 );
        xx_pos = tf.tile( xx_pos, [x_shape[0],1,1,1] )
        yy_pos = tf.expand_dims( tf.expand_dims(yy_pos,axis=0), axis=-1 );
        yy_pos = tf.tile( yy_pos, [x_shape[0],1,1,1] )
        #Concatenating exact location map
        x_in = tf.concat([x_in,xx_pos,yy_pos],axis=-1);
        
        #Checking input
        if pool_out is None:
            pool_out = x_size[-1];
        if cell_size is None:
            cell_size = x_size[-1];
            
        """Initial state based on global pooling"""
    #    H_init = tf.zeros([x_shape[0], cell_numb*cell_size],tf.float32)
        x_gpooled = tf.reduce_mean(x_in,axis=[1,2]);
        H_init = tf.layers.dense( x_gpooled, units=(cell_numb*cell_size) );
          
        """Generate and apply the actual RNN cell"""
        x = tf.reshape(x_in, shape=[x_shape[0],x_shape[1]*x_shape[2],2+x_size[-1]] )
        rnn_cells  = [ tf.nn.rnn_cell.GRUCell( cell_size, activation=tf.nn.softsign, reuse=tf.AUTO_REUSE ) for _ in range(cell_numb) ];
        multicell = tf.nn.rnn_cell.MultiRNNCell(rnn_cells, state_is_tuple=False)
        Yr, state = tf.nn.dynamic_rnn( multicell, x, dtype=tf.float32, initial_state=H_init )
        
    return state;



""""Dimensionality preserving pooling without additional parameters"""
def pool_max( x_in, pool_out=None ):   
    p_max = tf.nn.pool( x_in, [3,3], strides=[2,2], pooling_type='MAX', padding="VALID");
    return p_max;

""""Dimensionality preserving pooling without additional parameters"""
def pool_avg( x_in, pool_out=None ):
    p_avg = tf.nn.pool( x_in, [3,3], strides=[2,2], pooling_type='AVG', padding="VALID");
    return p_avg;




""""Dimensionality preserving pooling without additional parameters"""
def pool_avgmax( x_in, pool_out=None ):
    p_max = tf.nn.pool( x_in, [3,3], strides=[2,2], pooling_type='MAX', padding="VALID");
    p_avg = tf.nn.pool( x_in, [3,3], strides=[2,2], pooling_type='AVG', padding="VALID");
    return tf.concat( [p_avg, p_max], axis=-1 );

""""Dimensionality preserving pooling with additional parameters"""
def pool_dual( x_in, pool_out=None ):
    x_size = x_in.get_shape().as_list()[-1];
    #Checking input
    if pool_out is None:
        pool_out = x_size;
    else:
        assert pool_out>=x_size,"ERROR: Expected output from dual pooling layer is too shallow.";
        pool_out = pool_out-x_size;
    p_max = tf.nn.pool( x_in, [3,3], strides=[2,2], pooling_type='MAX', padding="VALID");
    if pool_out>0:        
        p_con = layers.sepconv2d( x_in, filters=pool_out, kernel_size=[3,3], strides=[2,2], padding='VALID' );
        merged = tf.concat( [p_con, p_max], axis=-1 );
    else:
        merged =p_max;
#    p_con = layers.conv2d( x_in, filters=pool_out, kernel_size=[3,3], strides=[2,2], padding="valid", act_fn=tf.nn.elu );
    return merged;

"""Deconvolution layer"""
def depool( x_in, pool_out=None, output_shape=None, padding="VALID" ):
    x_size = x_in.get_shape().as_list();
    x_shape= tf.shape(x_in);
    #Checking input
    if pool_out is None:
        pool_out = x_size[-1]/2;
    if output_shape is None:
        output_shape = tf.stack([x_shape[0], 2*x_shape[1], 2*x_shape[2], pool_out ]);       
       
    strides = [1,2,2,1];
    weights = layers.add_weight([3,3,pool_out,x_size[-1]]);
    bias    = layers.add_bias(pool_out);

    ACT = tf.nn.conv2d_transpose( x_in, weights, output_shape, strides, padding=padding );
    FIN = tf.nn.elu( tf.add( ACT, bias ) );

    return FIN;
    
    
    

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""


def globalpool_WAVG( logits ):

    p_wavg = tf.reduce_mean( attention.attention_feature_global( logits ), [1, 2] );    
    p_wavg = tf.identity( p_wavg, name='globalpool' );
    return p_wavg;

def signed_pow( x, p ):
	return tf.sign(x)*tf.pow( tf.abs(x), p );

def globalpool_GM( inp_val, r_par=2.0 ):

    p_gm = signed_pow( tf.reduce_mean( signed_pow(inp_val,r_par), [1, 2] ), 1.0/r_par );    
    p_gm = tf.identity( p_gm, name='globalpool' );
    return p_gm;

def globalpool_LSE( inp_val, r_par=2.0 ):
        
    p_lse = tf.log( tf.reduce_mean( tf.exp( r_par*inp_val), [1, 2] ) );    
    p_lse = tf.identity( (1.0/r_par)*p_lse, name='globalpool' );
    return p_lse;

def globalpool_nAND( inp_val, a_par=5.0 ):
    
    x_size = inp_val.get_shape().as_list();
#    a_par = tf.Variable( tf.ones([1,x_size[-1]], tf.float32) );
    b_par = tf.Variable( tf.zeros([1,x_size[-1]], tf.float32) );

    p_avg = tf.reduce_mean( inp_val, [1, 2] );
    p_nand=  ( tf.nn.sigmoid( a_par*(p_avg-b_par)) - tf.nn.sigmoid( -a_par*b_par) ) / ( tf.nn.sigmoid( a_par*(1.0-b_par)) - tf.nn.sigmoid( -a_par*b_par)+1e-7 );
    p_nand = tf.identity( p_nand, name='globalpool' );
    return p_nand;


