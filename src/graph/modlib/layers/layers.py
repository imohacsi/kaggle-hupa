# -*- coding: utf-8 -*-
"""
Created on Mon May 28 13:12:54 2018

@author: mohacsii
"""

import tensorflow as tf


"""Basic procedure of adding weights that are registered in the weights list"""
def add_weight( shape, initializer=tf.glorot_uniform_initializer() ):       
    """Allocate the variable"""
    if shape is not None:                       
        weight = tf.Variable( initializer(shape), dtype=tf.float32, name='kernel' )    
    else:
        assert False,"ERROR: Not enough parameters for bias initialization"
    return weight;    
    
"""Basic procedure of adding bias that are registered in the bias list"""
def add_bias( shape=None ):
    """Change from initialization methods"""
    if shape is not None:                       #Initialize it to zero
        bias = tf.Variable( tf.zeros(shape), dtype=tf.float32, name='bias' );
    else:
        assert False,"ERROR: Not enough parameters for bias initialization"       
    return bias;  

"""Basic procedure of adding an external feature vector as bias"""
def add_vector_bias( x_in, bias_vec ):
    vec_size = bias_vec.get_shape().as_list()[-1];
    x_size = x_in.get_shape().as_list()[-1];


#    if tf.rank( x_in )==4:
    mat_proj = add_weight( [vec_size,x_size] );
    dyn_bias = tf.matmul( bias_vec, mat_proj);            
    x_out = x_in + tf.expand_dims( tf.expand_dims(dyn_bias,1),1 );        
#    elif tf.rank( x_in )==2:
#        mat_proj = add_weight( [vec_size,x_size] );
#        dyn_bias = tf.matmul( bias_vec, mat_proj);            
#        x_out = x_in + dyn_bias;        
    return x_out;


def SENet_wrapper( func ):
	def func_wrapper( *args, **kwargs ):
		x = func(*args, **kwargs );
		x_size = x.get_shape().as_list();
		att = tf.reduce_mean( x, axis=[1,2] );
		att = tf.layers.dense( att, x_size[-1]//4, activation=tf.nn.elu );
		att = tf.layers.dense( att, x_size[-1], activation=tf.nn.sigmoid );
		att = tf.expand_dims( tf.expand_dims(att,1),1 );  
		x_out = att*x; 
		return x_out
	return func_wrapper;



def conv2d( x_in, filters, kernel_size, dilation=[1,1], strides=[1,1], padding='SAME', act_fn=tf.nn.elu, bias_vec=None, num_groups=1, style="conv", use_bias=True ):
    """Make sure that input is correct"""
    filters = int( filters );

    """Also accept simple numerical values"""
    if not isinstance(dilation, list):
        dilation = [dilation,dilation];
    if not isinstance(strides, list):
        strides = [strides,strides];

    if style in ['conv','full','normal']:
        return full_conv2D( x_in, filters, kernel_size, dilation, strides, padding, act_fn, bias_vec, use_bias );
    elif style in ['sep','sepconv','separable']:
        return separable_conv2D( x_in, filters, kernel_size, dilation, strides, padding, act_fn, bias_vec, use_bias );
    elif style in ['grouped']:
        return grouped_conv2D( x_in, filters, kernel_size, dilation, strides, padding, act_fn, bias_vec, num_groups, use_bias );



def conv2D( x_in, filters, kernel_size, dilation=[1,1], strides=[1,1], padding='SAME', act_fn=tf.nn.elu, bias_vec=None, num_groups=1, style="conv", use_bias=True ):
    return conv2d( x_in, filters, kernel_size, dilation, strides, padding, act_fn, bias_vec, num_groups, style, use_bias );



def full_conv2D( x_in, filters, kernel_size, dilation=[1,1], strides=[1,1], padding='SAME', act_fn=tf.nn.elu, bias_vec=None, use_bias=True ):
    """Specifying number of input channels"""
    x_filt = x_in.get_shape().as_list()[-1];
    w_size = [ kernel_size[0], kernel_size[1], x_filt,filters];
        
    with tf.variable_scope("conv",reuse=None):
        W = add_weight( w_size );   
        if use_bias:         
            B = add_bias( [filters] );                     
        stride_rate = [1, strides[0], strides[1],1];
        dilate_rate = [1,dilation[0],dilation[1],1];
        
        """Performing the actual convolution and adding postprocessing steps"""
        try:
            conv = tf.nn.conv2d(x_in, W, strides=stride_rate, padding=padding.upper(), dilations=dilate_rate );
        except:
            print("Ooops, something wrong with conv2d, trying legacy version...");
            conv = tf.nn.conv2d(x_in, W, strides=stride_rate, padding=padding.upper() );
        
        """Applying both biases"""
        if use_bias:
            conv = tf.nn.bias_add( conv, B);        
        if bias_vec is not None:          
            conv = add_vector_bias( conv, bias_vec );
                    
        """Just activate at the end"""
        if act_fn is not None:
            conv = act_fn( conv );
    return conv;


def separable_conv2D( x_in, filters, kernel_size, dilation=[1,1], strides=[1,1], padding='SAME', act_fn=tf.nn.elu, bias_vec=None, use_bias=True ):
    """1x1 separable convolutions are useless"""
    if kernel_size[0]==1 and kernel_size[1]==1:
        return conv2d( x_in, filters, kernel_size, padding=padding, act_fn=act_fn );

    """Specifying input and output channel numbers"""
    x_filt = x_in.get_shape().as_list()[-1];
    
    w_d_size = [ kernel_size[0], kernel_size[1], x_filt, 1 ];
    w_p_size = [ 1, 1, x_filt, filters ];
    b_p_size = [ filters ]; 
    
    with tf.variable_scope("sepconv",reuse=None):
        W_d = add_weight( w_d_size );            
        W_p = add_weight( w_p_size ); 
        if use_bias:
            B_p = add_bias(   b_p_size );  
        
        padding = padding.upper();
        stride = [1, strides[0], strides[1],1];
        #Performing the actual convolution and adding postprocessing steps
        sconv = tf.nn.separable_conv2d( x_in, W_d, W_p, strides=stride, padding=padding, rate=dilation );

        """If we are using external bias"""
        """Applying both biases"""
        if use_bias:
            sconv = tf.nn.bias_add( sconv, B_p);        
        if bias_vec is not None:          
            sconv = add_vector_bias( sconv, bias_vec );
            
            """Just activate at the end"""
        if act_fn is not None:
            sconv = act_fn( sconv );
    return sconv;
    



def grouped_conv2D( x_in, filters, kernel_size, dilation=[1,1], strides=[1,1],  padding='SAME', act_fn=tf.nn.elu, bias_vec=None, num_groups=4, use_bias=True ):    
    x_size = x_in.get_shape().as_list();
    group_size_in  = x_size[-1] // num_groups;
    group_size_out = filters // num_groups;

    stride = [1, strides[0], strides[1],1];
    dilation = [1, dilation[0], dilation[1],1];
    #There is a bug here if filters != x_size[-1]
    w_size = [ kernel_size[0], kernel_size[1], x_size[-1], group_size_out];
    b_size = filters;
    
    with tf.variable_scope("grouped_conv",reuse=None):
        W = add_weight( w_size );   
        if use_bias:     
            B = add_bias(   b_size );  

        x = tf.nn.depthwise_conv2d_native(x_in, W, strides=stride, dilations=dilation, padding=padding );
        dw_shape = tf.shape( x );
        dw_list  = x.get_shape().as_list(); 
        im_shape = [dw_shape[0],dw_list[1],dw_list[2], int(num_groups), int(group_size_in), int(group_size_out)];

        x = tf.reshape(x, shape=im_shape );
        x = tf.reduce_sum(x, axis=4)
        fi_shape = [dw_shape[0],dw_list[1],dw_list[2], int(filters) ];
        x = tf.reshape(x, shape=fi_shape )
        """Adding bias"""
        if use_bias:
            sconv = tf.nn.bias_add( x, B);        
    
        if act_fn is not None:
            sconv = act_fn( sconv );

    return sconv

