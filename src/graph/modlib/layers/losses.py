#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  8 18:36:19 2018

@author: imohacsi
"""

import tensorflow as tf
import numpy as np





def pred_softmax( logits ):
    with tf.variable_scope("predictions",reuse=None):    
        predictions = tf.nn.softmax( logits=logits, name='predictions' );
    return predictions;

def pred_sigmoid( logits ):
    with tf.variable_scope("predictions",reuse=None):    
        predictions = tf.nn.sigmoid( logits, name='predictions' );
    return predictions;

def pred_ident( pred ):
    with tf.variable_scope("predictions",reuse=None):    
        predictions = tf.identity( pred, name='predictions' );
    return predictions;


"""The diverse density probability from MIL ( probability based on KNN distance)"""
def pred_ddensity( features, num_classes ):
    
    x_size = features.get_shape().as_list();
    with tf.variable_scope("predictions",reuse=tf.AUTO_REUSE):    
        W = tf.get_variable( "dd_center", [1, num_classes,x_size[-1]], dtype=tf.float32, initializer=tf.zeros_initializer );
        S = tf.get_variable( "dd_sigmas", [1, num_classes,x_size[-1]], dtype=tf.float32, initializer=tf.constant_initializer(0.01) );
    
        feat_T = tf.tile( tf.expand_dims( features, axis=1 ), [1,num_classes,1] );
        logits = tf.reduce_mean( S*tf.square( feat_T - W), axis=[-1], name='norm' );
        prob = tf.exp( -logits, name='predictions' )
        
    with tf.variable_scope("logits",reuse=None):    
        logits = tf.identity( logits, name='logits' );
    return prob;    
    
def pred_knn_ddensity( features, num_classes, k=4 ):
    
    x_size = features.get_shape().as_list();
    with tf.variable_scope("predictions",reuse=tf.AUTO_REUSE):    
        W = tf.get_variable( "dd_center", [1, num_classes,k,x_size[-1]], dtype=tf.float32, initializer=tf.glorot_uniform_initializer() );
        S = tf.get_variable( "dd_sigmas", [1, num_classes,k,x_size[-1]], dtype=tf.float32, initializer=tf.constant_initializer(0.01) );
    
        feat_T = tf.tile( tf.expand_dims( tf.expand_dims( features, axis=1 ), axis=1 ), [1,num_classes,k,1] );
        #Multiply by 10 to reduce L2 penalty
        logits = tf.reduce_min( tf.reduce_mean( S*tf.square( feat_T - W ), axis=[-1] ), axis=[-1], name='norm' );
        prob = tf.exp( -logits, name='predictions' )
        
    with tf.variable_scope("logits",reuse=None):    
        logits = tf.identity( logits, name='logits' );
    return prob;    
    
def get_class_weights( alpha, class_weights, t_out ):
    #First calculate the class rebalancing factors
    if class_weights is not None:
        class_weights = np.array( class_weights );
        print("INFO: Initializing loss with class weights:\n",class_weights)
        weight_t = tf.constant( class_weights, dtype=tf.float32 );
        weight_t = tf.expand_dims( weight_t, axis=0, name='class_weights' );
        alpha_t = weight_t
    else:
        alpha_t = tf.constant(alpha, dtype=tf.float32); 
    print( alpha_t )
    return alpha_t;
    
    

def loss_xentropy_softmax_logits( logits, labels, alpha=0.5, class_weights=None):
    #Logits: Raw evidence coming trom the neural network
    #The actual training labels
    with tf.variable_scope("loss_xentr",reuse=None):  
        alpha = get_class_weights( alpha, class_weights, logits );  
        alpha_t = alpha*tf.ones_like( logits, dtype=tf.float32 );
        alpha_t = tf.where( tf.equal(labels,1.0), alpha_t, 1.0-alpha_t );
         
        raw_xentropy = tf.nn.softmax_cross_entropy_with_logits_v2( labels=labels, logits=logits );
        loss_xentropy =  tf.reduce_sum( alpha_t * raw_xentropy, axis=-1 );
        loss = tf.reduce_mean( loss_xentropy, name='loss_xentropy');   
    return loss;


def loss_xentropy_sigmoid_logits( logits, labels, alpha=0.5, class_weights=None ):
    with tf.variable_scope("loss_xentr",reuse=None):  
        #First calculate the class rebalancing factors
        alpha = get_class_weights( alpha, class_weights, logits );
        alpha_t = alpha*tf.ones_like( logits, dtype=tf.float32 );
        alpha_t = tf.where( tf.equal(labels,1.0), alpha_t, 1.0-alpha_t );

        #Then calculate the cross entropy
        raw_xentropy  = tf.nn.sigmoid_cross_entropy_with_logits( labels=labels, logits=logits );
        loss_xentropy =  tf.reduce_sum( alpha_t * raw_xentropy, axis=-1 );
        loss = tf.reduce_mean( loss_xentropy, name='loss_xentropy');         
    return loss;


def loss_xentropy_sigmoid_prob( prob, labels, alpha=0.5, class_weights=None ):
    with tf.variable_scope("loss_xentr",reuse=None):  
        #First calculate the class rebalancing factors
        alpha = get_class_weights( alpha, class_weights, prob );           
        alpha_t = alpha*tf.ones_like( prob, dtype=tf.float32 );
        alpha_t = tf.where( tf.equal(labels,1.0), alpha_t, 1.0-alpha_t );

        #Then calculate the cross entropy
        p = prob;
        p_t= tf.where( tf.equal(labels,1.0), p, 1.0-p)
        raw_xentropy = -tf.log( p_t+1e-10 );
        loss_xentropy =  tf.reduce_sum( alpha_t * raw_xentropy, axis=-1 );
        loss = tf.reduce_mean( loss_xentropy, name='loss_xentropy');         
    return loss;


def loss_xentropy_focal_logits( logits, labels, gamma=2.0, alpha=0.5, class_weights=None ):   
    
    with tf.variable_scope("loss_focal",reuse=None):         
        p = tf.nn.sigmoid( logits );
        p_t= tf.where( tf.equal(labels,1.0), p, 1.0-p);
        xentropy = tf.nn.sigmoid_cross_entropy_with_logits( labels=labels, logits=logits );
#        xentropy = -tf.log( p_t+1e-15 );
        
        alpha = get_class_weights( alpha, class_weights, logits );
        alpha_t = alpha*tf.ones_like( logits, dtype=tf.float32 );
        alpha_t = tf.where( tf.equal(labels,1.0), alpha_t, 1.0-alpha_t );
        
        focal_loss =  alpha_t * tf.pow( (1.0-p_t), gamma ) * xentropy;
        focal_loss = tf.identity( tf.reduce_mean( tf.reduce_sum(focal_loss, axis=1) ), name='loss_focal');      
    return focal_loss;


def loss_xentropy_focal_prob( prob, labels, gamma=2.0, alpha=0.5, class_weights=None ):   
    
    with tf.variable_scope("loss_focal",reuse=None):         
        p = prob;
        p_t= tf.where( tf.equal(labels,1.0), p, 1.0-p);
#        xentropy = tf.nn.sigmoid_cross_entropy_with_logits( labels=labels, logits=logits );
        xentropy = -tf.log( p_t+1e-15 );

        alpha = get_class_weights( alpha, class_weights, prob );
        alpha_t = alpha*tf.ones_like( prob, dtype=tf.float32 );
        alpha_t = tf.where( tf.equal(labels,1.0), alpha_t, 1.0-alpha_t );
        
        focal_loss =  alpha_t * tf.pow( (1.0-p_t), gamma ) * xentropy;
        focal_loss = tf.identity( tf.reduce_mean( tf.reduce_sum(focal_loss, axis=1) ), name='loss_focal');      
    return focal_loss;



""""""""""""""""""""""""""""""""""""""""""""""""""
"""      Regularization losses section         """
""""""""""""""""""""""""""""""""""""""""""""""""""



def loss_kernel_l2( w_decay ):
    regval_l2= tf.constant( w_decay, name='l2_regularizer' )
    kernel_var = [tvar for tvar in tf.trainable_variables() if 'kernel' in tvar.name];
#    kernel_var = [tvar for tvar in tf.trainable_variables() ];

    kernel_l2  = [tf.nn.l2_loss(kvar) for kvar in kernel_var];
    added_loss = tf.add_n(kernel_l2)  
    l2_loss = tf.identity( tf.scalar_mul( regval_l2, added_loss), name='loss_l2');         
    return l2_loss;




def loss_entropy_softmax_logits( logits, weights=None, tempr=1.0, bsrate=0.0):
    #Logits: Raw evidence coming trom the neural network
    #The actual training labels
    with tf.variable_scope("loss_entropy",reuse=None):         
        bootstr_tempr = tf.constant(tempr,name='bootstrapping_temperature', dtype=tf.float32);
        bootstr_ratio = tf.constant(bsrate,name='bootstrapping_ratio', dtype=tf.float32);
        #Generating smoothened labels        
        softlabels = tf.nn.softmax( logits=(bootstr_tempr*logits) );             
        entropy = tf.nn.softmax_cross_entropy_with_logits_v2( labels=softlabels, logits=logits );
        loss = tf.reduce_mean( entropy, name='loss_entropy');         
    return bootstr_ratio*loss;

def loss_entropy_sigmoid_logits( logits, weights=None, tempr=1.0, bsrate=0.0 ):
    #Logits: Raw evidence coming trom the neural network
    #The actual training labels
    with tf.variable_scope("loss_entropy",reuse=None):         
        bootstr_tempr = tf.constant(tempr,name='bootstrapping_temperature', dtype=tf.float32);
        bootstr_ratio = tf.constant(bsrate,name='bootstrapping_ratio', dtype=tf.float32);
        softlabels = tf.nn.sigmoid(bootstr_tempr*logits)
        entropy =  tf.nn.sigmoid_cross_entropy_with_logits( labels=softlabels, logits=logits );
        loss = tf.identity( tf.reduce_mean( tf.reduce_sum(entropy, axis=1) ), name='loss_entropy');         
    return bootstr_ratio*loss;










def loss_mse( prediction, labels):
    with tf.variable_scope("loss_mse",reuse=None):  
        loss = tf.losses.mean_squared_error( labels, prediction );
        """Establishing the common diagnostics"""
        total_loss = tf.reduce_mean(loss, name='loss_mse')
    return total_loss;













def loss_f1_micro( logits, labels ):
    with tf.variable_scope("loss_f1",reuse=None):    
        pred = tf.nn.sigmoid( logits );
        tp = tf.cast( pred*labels, tf.float32 )
    #    tn = tf.cast( (1.0-pred)*(1.0-labels), tf.float32 )
        fp = tf.cast( pred*(1.0-labels), tf.float32 )
        fn = tf.cast( (1.0-pred)*labels, tf.float32 )
    
        prec = tf.reduce_sum( tp, axis=-1 ) / ( tf.reduce_sum( tp+fp, axis=-1 )+1e-15);
        recl = tf.reduce_sum( tp, axis=-1 ) / ( tf.reduce_sum( tp+fn, axis=-1 )+1e-15);
    
        f1 = 2.0*prec*recl / (prec+recl+1e-15);
        f1 = tf.where(tf.is_nan(f1), tf.zeros_like(f1), f1)
        f1 = 1.0-tf.reduce_mean( f1, name='loss_f1' );
    return f1;

def loss_f1_macro_logits( logits, labels, beta=1.0 ):
    print("Using macro F1 loss (This should automatically balance classes)")
    with tf.variable_scope("loss_f1",reuse=None):    
        pred = tf.nn.sigmoid( logits );
        tp = tf.reduce_sum( tf.cast( pred*labels, tf.float32 ), axis=0 );
    #    tn = tf.reduce_sum( tf.cast( (1.0-pred)*(1.0-labels), tf.float32 ), axis=0 );
        fp = tf.reduce_sum( tf.cast( pred*(1.0-labels), tf.float32 ), axis=0 );
        fn = tf.reduce_sum( tf.cast( (1.0-pred)*labels, tf.float32 ), axis=0 );

        prec = tp / ( tp + fp + tf.constant(1e-7) );
        recl = tp / ( tp + fn + tf.constant(1e-7) );
    
        f1 = 2.0*prec*recl / (prec+recl);
        if1 = 1.0 - f1;
        if1 = tf.boolean_mask( if1, tf.is_finite(if1) );
        if1 = tf.reduce_mean( if1, name='loss_f1' );
    return if1;



""""""""""""""""""""""""""""""""""""""""""""""""""
"""            Experimental section            """
""""""""""""""""""""""""""""""""""""""""""""""""""


def loss_xentropy_ddensity( features, labels, weights=None ):
    num_classes = labels.get_shape().as_list()[-1];
    #Logits: Raw evidence coming trom the neural network
    #The actual training labels
    with tf.variable_scope("loss_ddxentr",reuse=None):  
        p = pred_ddensity( features, num_classes );
        p_t= tf.where( tf.equal(labels,1.0), p, 1.0-p)
        
        raw_xentropy = -tf.log( p_t+1e-15 );
        
        if weights is not None:
            class_weights = tf.constant(weights,name='class_weights', dtype=tf.float32);
            example_weights = tf.reduce_sum( class_weights*labels, axis=1 )
            print( "Weights shape: ", example_weights.get_shape().as_list() )
            loss = tf.reduce_mean( tf.reduce_sum(raw_xentropy*example_weights,axis=1), name='loss_ddxentr' );
        else:
            loss = tf.reduce_mean( tf.reduce_sum(raw_xentropy, axis=1), name='loss_ddxentr');         
    return loss,p;


def loss_xentropy_knn_ddensity( features, labels, k=4, weights=None ):
    num_classes = labels.get_shape().as_list()[-1];
    #Logits: Raw evidence coming trom the neural network
    #The actual training labels
    with tf.variable_scope("loss_ddxentr",reuse=None):  
        p = pred_knn_ddensity( features, num_classes, k=k );
        p_t= tf.where( tf.equal(labels,1.0), p, 1.0-p)
        
        raw_xentropy = -tf.log( p_t+1e-15 );
        
        if weights is not None:
            class_weights = tf.constant(weights,name='class_weights', dtype=tf.float32);
            example_weights = tf.reduce_sum( class_weights*labels, axis=1 )
            print( "Weights shape: ", example_weights.get_shape().as_list() )
            loss = tf.reduce_mean( tf.reduce_sum(raw_xentropy*example_weights,axis=1), name='loss_ddxentr' );
        else:
            loss = tf.reduce_mean( tf.reduce_sum(raw_xentropy, axis=1), name='loss_ddxentr');         
    return loss,p;

def loss_xentropy_focal_ddensity( features, labels, gamma=2.0, alpha=0.5, class_weights=None ):   
    num_classes = labels.get_shape().as_list()[-1];
    if class_weights is not None:
        class_weights = np.array( class_weights );
        print("INFO: Initializing focal loss with weights:\n",class_weights)
        class_weights = tf.constant(alpha, dtype=tf.float32);
        class_weights = tf.expand_dims( class_weights, axis=0, name='class_weights' );
        alpha = class_weights
    
    with tf.variable_scope("loss_focal_dd",reuse=None):         
        p = pred_ddensity( features, num_classes );
        p_t= tf.where( tf.equal(labels,1.0), p, 1.0-p)
        
        xentropy = -tf.log( p_t+1e-15 );

        alpha_t = alpha*tf.ones_like( features, dtype=tf.float32 );
        alpha_t = tf.where( tf.equal(labels,1.0), alpha_t, 1.0-alpha_t );
        
        focal_loss =  alpha_t * tf.pow( (1.0-p_t), gamma ) * xentropy;
        
        focal_loss = tf.identity( tf.reduce_mean( tf.reduce_sum(focal_loss, axis=1) ), name='loss_focal_dd');      
    return focal_loss;

def loss_xentropy_focal_knn_ddensity( features, labels, gamma=2.0, alpha=0.5, k=4, class_weights=None ):   
    num_classes = labels.get_shape().as_list()[-1];
    if class_weights is not None:
        class_weights = np.array( class_weights );
        print("INFO: Initializing focal loss with weights:\n",class_weights)
        class_weights = tf.constant(alpha, dtype=tf.float32);
        class_weights = tf.expand_dims( class_weights, axis=0, name='class_weights' );
        alpha = class_weights
    
    with tf.variable_scope("loss_focal_dd",reuse=None):         
        p = pred_knn_ddensity( features, num_classes, k=k );
        p_t= tf.where( tf.equal(labels,1.0), p, 1.0-p)
        
        xentropy = -tf.log( p_t+1e-15 );

        alpha_t = alpha*tf.ones_like( features, dtype=tf.float32 );
        alpha_t = tf.where( tf.equal(labels,1.0), alpha_t, 1.0-alpha_t );
        
        focal_loss =  alpha_t * tf.pow( (1.0-p_t), gamma ) * xentropy;
        
        focal_loss = tf.identity( tf.reduce_mean( tf.reduce_sum(focal_loss, axis=1) ), name='loss_focal_dd');      
    return focal_loss;







