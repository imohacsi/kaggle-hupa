# -*- coding: utf-8 -*-
"""
Created on Wed Mar  7 14:57:00 2018

@author: mohacsii
"""
import tensorflow as tf
import sys

from .modlib.cls import resnet50
from .modlib.cls import se_resnet50
from .modlib.cls import resnet24
from .modlib.cls import se_resnet24

from .modlib.cls import resnet18
from .modlib.cls import mobilenet
from .modlib.cls import xception
from .modlib.cls import xception24

from .modlib.cls import se_xception
from .modlib.cls import resnext50
from .modlib.cls import resnext18

from .modlib.seg import u_net
from .modlib.seg import u_net_res
from .modlib.seg import u_net_sepres
from .modlib.seg import psp_net
from .modlib.seg import deeplab_v1
from .modlib.seg import deeplab_v3
from .modlib.seg import deeplab_v3plus


def load_graph( path, name, mtype='cls' ):
    if mtype in ['cls','classification']:
        cgraph = resnet50.resnet50.load_old_graph( path, name );
    elif mtype in ['hupa']:
        cgraph = resnet50.resnet50.load_old_graph( path, name, is_multi=True, );
    elif mtype in ['seg','segmentation']:
        cgraph = u_net.u_net.load_old_graph( path, name );
    else:
        sys.exit( "ERROR: Unknown classification model type: " + mtype );
    return cgraph;


def get_model_from_library( model_name, num_classes, mtype='cls', class_freq=None ):
    if mtype in ['cls','classification']:
        if model_name == 'resnet18':
            return resnet18.resnet18( num_classes, class_freq=class_freq );
        elif model_name == 'resnet50':
            return resnet50.resnet50( num_classes, class_freq=class_freq );  
        elif model_name == 'se_resnet50':
            return se_resnet50.se_resnet50( num_classes, class_freq=class_freq ); 
        elif model_name == 'mobilenet':
            return mobilenet.mobilenet( num_classes, class_freq=class_freq );   
        elif model_name == 'xception':
            return xception.xception( num_classes, class_freq=class_freq );   
        elif model_name == 'xception24':
            return xception24.xception24( num_classes, class_freq=class_freq );   
        elif model_name == 'se_xception':
            return se_xception.se_xception( num_classes, class_freq=class_freq );   
        elif model_name == 'resnext50':
            return resnext50.resnext50( num_classes, class_freq=class_freq );  
        elif model_name == 'resnext18':
            return resnext18.resnext18( num_classes, class_freq=class_freq );  
        else:
            sys.exit( "ERROR: Unknown classification model with name: " + model_name );
    if mtype in ['hupa']:
        if model_name == 'resnet18':
            return resnet18.resnet18( num_classes, is_multi=True, class_freq=class_freq );
        elif model_name == 'resnet24':
            return resnet24.resnet24( num_classes, is_multi=True, class_freq=class_freq ); 
        elif model_name == 'se_resnet24':
            return se_resnet24.se_resnet24( num_classes, is_multi=True, class_freq=class_freq ); 
        elif model_name == 'resnet50':
            return resnet50.resnet50( num_classes,  is_multi=True, class_freq=class_freq ); 
        elif model_name == 'se_resnet50':
            return se_resnet50.se_resnet50( num_classes, is_multi=True, class_freq=class_freq ); 
        elif model_name == 'mobilenet':
            return mobilenet.mobilenet( num_classes, is_multi=True, class_freq=class_freq );   
        elif model_name == 'xception':
            return xception.xception( num_classes, is_multi=True, class_freq=class_freq );
        elif model_name == 'xception24':
            return xception24.xception24( num_classes, is_multi=True, class_freq=class_freq );
        elif model_name == 'se_xception':
            return se_xception.se_xception( num_classes, is_multi=True, class_freq=class_freq );
        elif model_name == 'resnext50':
            return resnext50.resnext50( num_classes, is_multi=True, class_freq=class_freq );  
        elif model_name == 'resnext18':
            return resnext18.resnext18( num_classes, is_multi=True, class_freq=class_freq );  
        else:
            sys.exit( "ERROR: Unknown classification model with name: " + model_name );
    elif mtype in ['seg','segmentation']:
        if model_name == 'u_net':
            return u_net.u_net( num_classes, class_freq=class_freq );
        elif model_name == 'u_net_res':
            return u_net_res.u_net_res( num_classes, class_freq=class_freq ); 
        elif model_name == 'u_net_sepres':
            return u_net_sepres.u_net_sepres( num_classes, class_freq=class_freq ); 
        elif model_name == 'deeplab_v1':
            return deeplab_v1.deeplab_v1( num_classes, class_freq=class_freq ); 
        elif model_name == 'deeplab_v3':
            return deeplab_v3.deeplab_v3( num_classes, class_freq=class_freq ); 
        elif model_name == 'deeplab_v3+':
            return deeplab_v3plus.deeplab_v3plus( num_classes, class_freq=class_freq ); 
        elif model_name == 'psp_net':
            return psp_net.psp_net( num_classes, class_freq=class_freq );  
        else:
            sys.exit( "ERROR: Unknown segmentation model with name: " + model_name );
    else:
        sys.exit( "ERROR: Unknown model type with name: " + mtype );
    
